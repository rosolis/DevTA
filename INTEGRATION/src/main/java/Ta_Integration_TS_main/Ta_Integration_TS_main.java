package Ta_Integration_TS_main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import SubmitAndCreateDEsubfilesDE_001.SubmitAndCreateDEsubfilesDE_001;
import SubmitAndSaveFileInDmsHml_015.SubmitAndSaveFileInDmsHml_015;
import UploadAndConfirmationEmail.UploadAndConfirmationEmail;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.ExcelRead;
import Utils.FileAndLog;
import CommonWeb.PageLoginPortal;
import CommonWeb.PageLogoutPortal;

public class Ta_Integration_TS_main {

    Properties SYS_CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_URL="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME="SubmitBordereau";
    private String FILE_PARAM="01_ParamPORTAL/04_SubmitBordereau_PARAM.txt";
    private int NUM_LIN_TEST=0;
    private boolean logsTrue = true;
    private FileAndLog sFlog = null;



    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String DIR_TS_PARAM=DIR_RUN;
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String GLOBAL_URL="02_LOG/";
    private String BROWSER_FROM_SYS="NO";
    private String BROWSER_FOR_ALL_TEST="";

    private String EXCEL_FILE="TS01_PORTAL_REPORT_V01.xlsx";
    private String EXCEL_FEUILLE  ="PORTAL_TESTS";
    private ExcelRead EXC = new ExcelRead();

    private List<List<Object>> TIME_RAPPORT= new ArrayList<>();
    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private String STATUS_TXT="NOT_OK";
    private String STATUS_NOTES="_";
    private List<String> LOG_TEST   =new ArrayList<>();

    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;



    //********************************************************
    //********************************************************


    public Ta_Integration_TS_main() throws IOException {
        String os = System.getProperty("os.name").toLowerCase();

        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        System.out.println("user home =");
        if (isOperatingSystemWindows) {

            DIR_ROOT_TS="C:"+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=dire_home+DIR_ROOT_TS;
        }
    }
    public Ta_Integration_TS_main(String nomTest, String fileParam) throws Exception {
        String dire_lancement = System.getProperty("user.dir");
        System.out.println("dire_lancement:" + dire_lancement);

        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        int deb = dire_lancement.lastIndexOf("0A_TS1_Portal");
        String sp=dire_lancement.substring(0,deb-1);
        if (isOperatingSystemWindows) {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        }
        System.out.println("Start= "+DIR_ROOT_TS);
        if (fileParam.length()<1){
            System.out.println("FILE PARAMETER ERROR");
            System.exit(0);
        }else{
            TEST_NAME=nomTest;
            FILE_PARAM=fileParam;
        }
        TEST_NAME=nomTest;
        FILE_PARAM=fileParam;
        sFlog = new FileAndLog();
        sFlog.setLogs(logsTrue);
        sUtil= new SeleniumUtil();

        InputStream inputRoot = new FileInputStream(DIR_ROOT_TS+"00_SYS_CONFIG.txt");

        SYS_CONFIG.load(inputRoot);
        DIR_REPORT = SYS_CONFIG.getProperty("DIR_REPORTS");
        EXCEL_FILE = SYS_CONFIG.getProperty("EXCEL_REPORT");
        DIR_JDD=SYS_CONFIG.getProperty("DIR_JDD");
        DIR_LOGS=SYS_CONFIG.getProperty("DIR_LOGS");
        DIR_DRIVER=SYS_CONFIG.getProperty("DIR_DRIVER");
        DIR_TS_PARAM=SYS_CONFIG.getProperty("DIR_TS_INTEGRATION");
        GLOBAL_URL=SYS_CONFIG.getProperty("SYS_CNX_LOGIN_PORTAL");
        BROWSER_FROM_SYS=SYS_CONFIG.getProperty("BROWSER_ALL");
        BROWSER_FOR_ALL_TEST=SYS_CONFIG.getProperty("SYS_CNX_BROWSER");
        DIR_RUN=DIR_ROOT_TS+DIR_RUN;

        String sF[]=FILE_PARAM.split("\\.");
        String sF2=sF[0].replace("\\","-");
        sF2=sF2.replace("/","-");

        DIR_LOGS=DIR_ROOT_TS+DIR_LOGS+sF2+ "_"+sFlog.logDate(4)+".log";
        sHelp = new SeleniumHelp(SYS_CONFIG,LOG_TEST);
        sHelp.setDIR_ROOT_TS(DIR_ROOT_TS);
        DIR_TS_PARAM=DIR_ROOT_TS+DIR_TS_PARAM;
        EXCEL_FILE=DIR_RUN+EXCEL_FILE;


    }

    //********************************************************

    //********************************************************
    public boolean setUp(String testName, String testParam) throws Exception {
        String dire_lancement = System.getProperty("user.dir");
        sFlog.Add_String("START TEST  = " +TEST_NAME,DIR_LOGS);


        InputStream input = new FileInputStream(DIR_TS_PARAM+FILE_PARAM);

        TEST_CONFIG.load(input);
        String temp = TEST_CONFIG.getProperty("CNX_BROWSER");
        if (BROWSER_FROM_SYS.contentEquals("YES")){
            temp=BROWSER_FOR_ALL_TEST;
        }
        if (temp.length()>0)
            CONF_BROWSER=temp.trim();

        MILLI_DEP = System.currentTimeMillis();
        MAP_TIME_LIGNE.put("START",sFlog.logDate(4));
        LOG_TEST.add("START TEST  = " +TEST_NAME);
        LOG_TEST.add("Dir lancement ="+dire_lancement);
        driver = sHelp.openWebdriver(driver, CONF_BROWSER); // FIREFOX  CHROME IEXPLORER PHANTOMJS

        sFlog.Add_String("apres INIT =" + CONF_BROWSER, DIR_LOGS);
        LOG_TEST.add("INIT OK =" + CONF_URL );
        try {

            WAIT = new WebDriverWait(driver, 10);
            Dimension dime = new Dimension(1400, 800);
            driver.manage().window().setSize(dime);
            driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
            if (BROWSER_FROM_SYS.contains("YES")){
                CONF_URL =GLOBAL_URL ;
            }else
                CONF_URL =TEST_CONFIG.getProperty("CNX_LOGIN_PORTAL");
            LOG_TEST.add("TRY TO OPEN URL =" + CONF_URL );

            String s_login = TEST_CONFIG.getProperty("CNX_TA_USER").trim();
            String s_pass = TEST_CONFIG.getProperty("CNX_TA_PASSWORD").trim();
            driver.get(CONF_URL);

            PageLoginPortal loginPage = new PageLoginPortal(driver, sUtil,sHelp, WAIT,LOG_TEST);
            sUtil.tempo(2);

            LOG_TEST.add("TRY TO LOGIN WITH USER AND PSW "+"Login user = "+s_login+ " ");

            loginPage.loginPortal(s_login, s_pass);
            sUtil.tempo(3);
            //WAIT.until(ExpectedConditions.urlContains("microsoftonline.com"));
            driver = loginPage.GetDriver();

            MILLI_FIN = System.currentTimeMillis();
            MILLI_DIFF = MILLI_FIN-MILLI_DEP;

            MAP_TIME_LIGNE.put("LOGIN",MILLI_DIFF);
            MILLI_TOT=MILLI_DIFF;
        } catch (Exception e) {
            e.printStackTrace();
            LOG_TEST.add("CRASH ");
            sHelp.trace_screen(driver);
        }

        System.out.println(MILLI_DIFF);

        return logGetIfOK();
    }

    public void runOneTest(String testName) throws IOException {

        if (FILE_PARAM.contains("__")){
            String a[]=FILE_PARAM.split("__");
            String num=a[0];
            if (num.length()>2){
                String os = System.getProperty("os.name").toLowerCase();
                int deb=0;
                if (num.contains("\\")) {
                    deb = num.lastIndexOf("\\");
                    num=num.substring(deb+1);
                }
                if (num.contains("/")){
                    deb = num.lastIndexOf("/");
                    num=num.substring(deb+1);
                    }
            }
            NUM_LIN_TEST=Integer.parseInt(num);
        }

        System.out.println("\n--->CALL TEST = "+TEST_NAME);
        try {
            switch  (TEST_NAME) {
                case "01_UPLOAD_AND_CONFIRMATION_EMAIL":
                    UploadAndConfirmationEmail uace =new UploadAndConfirmationEmail(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );

                    MILLI_DIFF=uace.TestUploadAndConfirmationEmail();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;

                    break;


                case "02_SUBMIT_AND_SAVE_FILE_IN_DMS_HML_015":
                    SubmitAndSaveFileInDmsHml_015 sasfim =new SubmitAndSaveFileInDmsHml_015(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    MILLI_DIFF=sasfim.TestSubmitAndSaveFileInDms();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;
                case "03_SUBMIT_AND_SAVE_ONE_FILE_IN_DE_001":
                    SubmitAndCreateDEsubfilesDE_001 sacDEs =new SubmitAndCreateDEsubfilesDE_001(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    MILLI_DIFF=sacDEs.TestSubmitAndCreateDEsubfilesDE_001();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;

                case "04_SUBMIT_MORE_THAT_ONE_FILE_IN_DE_001":
                    SubmitAndCreateDEsubfilesDE_001 smtofid =new SubmitAndCreateDEsubfilesDE_001(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    MILLI_DIFF=smtofid.TestSubmitAndCreateDEsubfilesDE_001();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;

                case "05_DE_SUBFILES_ARRIVED_IN_LA_001":
                    SubmitAndCreateDEsubfilesDE_001 DSAL =new SubmitAndCreateDEsubfilesDE_001(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    MILLI_DIFF=DSAL.TestSubmitAndCreateDEsubfilesDE_001();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;

            }
            LOG_TEST.add("FIN TEST  _-------------");
        } catch (Exception e) {
            e.printStackTrace();
            int i=LOG_TEST.size();
            String s=LOG_TEST.get(i-1);
            s="Fail test when "+s;
            LOG_TEST.set(i-1,s);
            LOG_TEST.add("CRASH");
            System.out.println("-------- CHRASH ----------");
        }finally {

            System.out.println("---> FIN TEST "+TEST_NAME+"\n");
        }


    }
    //***************************************************
    public void tearDown() throws IOException {

        MILLI_DEP = System.currentTimeMillis();
        PageLogoutPortal logoutPage = new PageLogoutPortal(driver, WAIT, DIR_LOGS);
        try {
            if (driver!=null) {
                if (logGetIfOK()){
                    logoutPage.clickClickProfile();
                    logoutPage.clickClickConfirm();
                    STATUS_TXT="OK";
                }else{
                    STATUS_NOTES=LOG_TEST.get(LOG_TEST.size()-2);
                }
            }
        } catch (Exception e) {
            if (logGetIfOK()){
                LOG_TEST.add("Test OK but log out KO");
                LOG_TEST.add("CRASH");
                STATUS_TXT="OK";
                STATUS_NOTES="Test OK but log out KO";
            }
            e.printStackTrace();
        } finally {
            System.out.println("------------------");

        }
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN-MILLI_DEP;
        MAP_TIME_LIGNE.put("LOGOUT",MILLI_DIFF);
        MILLI_TOT=MILLI_TOT+MILLI_DIFF;
        MAP_TIME_LIGNE.put("TOTAL",MILLI_TOT);
        sFlog.writeStatistic(DIR_ROOT_TS+DIR_REPORT+"STAT_"+TEST_NAME+".csv",MAP_TIME_LIGNE);
        sFlog.Add_Lines(LOG_TEST, DIR_LOGS);
        if (NUM_LIN_TEST>4)
            setExcelfile();
        if (driver!=null) driver.quit();

    }

    //***************************************************
    public void setExcelfile() throws IOException {
        EXC.set_File_Feuille(EXCEL_FILE,EXCEL_FEUILLE);
        System.out.println("EXCEL_FILE ="+EXCEL_FILE+" EXCEL_FEUILLE="+EXCEL_FEUILLE);
        EXC.openExcelFile();
        EXC.openFeuille(EXCEL_FEUILLE);
        EXC.ecrit_Lign_StringCell(NUM_LIN_TEST,5,STATUS_TXT);
        EXC.ecrit_Lign_StringCell(NUM_LIN_TEST,6,STATUS_NOTES);
        EXC.saveXLSX_file();
    }

    public boolean logGetIfOK(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return false;
        else
            return true;
    }
    public String logGetLastAction(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return LOG_TEST.get(i-2);
        else
            return LOG_TEST.get(i-1);
    }

    /************************************************************************************
     * ********************   MAIN
     ************************************************************************************/

    public  static void main(String[] args) throws Exception{



        HashMap<String,String> map_test=new HashMap<>();
        map_test.put("UACE" ,"01_UPLOAD_AND_CONFIRMATION_EMAIL");


        String  nomTest="";
        //nomTest="01_UPLOAD_AND_CONFIRMATION_EMAIL";
        //nomTest="02_SUBMIT_AND_SAVE_FILE_IN_DMS_HML_015";
        //nomTest="03_SUBMIT_AND_SAVE_ONE_FILE_IN_DE_001";
        //nomTest="04_SUBMIT_MORE_THAT_ONE_FILE_IN_DE_001";
        nomTest="05_DE_SUBFILES_ARRIVED_IN_LA_001";

        String fileParam="";

        //fileParam="101_UPLOAD_AND_CONFIRMATION_EMAIL_PARAM.txt";
        //fileParam="102_SUBMIT_AND_SAVE_FILE_IN_DMS_HML_015_PARAM.txt";
        //fileParam="103_SUBMIT_AND_SAVE_ONE_FILE_IN_DE_001_PARAM.txt";
        //fileParam="104_SUBMIT_MORE_THAT_ONE_FILE_IN_DE_001_PARAM.txt";
        fileParam="105_DE_SUBFILES_ARRIVED_IN_LA_001_PARAM_Excel.txt";


        String  nomTestK="";
        if (args.length ==2) {
            System.out.println(args[0] +"+ "+args[1] );
            nomTestK=args[0] ;
            nomTest=map_test.get(nomTestK);
            fileParam=args[1] ;
            System.out.println(nomTest+"+ "+fileParam);
            if (nomTest==null)
                System.exit(0);
            if (fileParam==null)
                System.exit(0);
        }


        /*********  Construit le test ***********************
         *
         */
        Ta_Integration_TS_main ta = new Ta_Integration_TS_main(nomTest,fileParam);
        boolean status=ta.setUp(nomTest,fileParam);

        if (status==true) ta.runOneTest(nomTest);
        ta.tearDown();
        System.exit(0);
    }

}
