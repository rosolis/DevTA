package SubmitAndCreateDEsubfilesDE_001;

        import AccesToHeliosMonitoring.AccesToHeliosMonitoring;
        import Helper.SeleniumHelp;
        import Helper.SeleniumUtil;
        import SubmitBordereau.AccesAndSubmitBordereau;

        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.support.ui.WebDriverWait;

        import java.io.IOException;
        import java.util.*;

public class SubmitAndCreateDEsubfilesDE_001 {


    Properties CONFIG = new Properties();
    Properties TEST_CONFIG = new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR = "";
    private String CONF_BROWSER = "FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME = "";
    private String TEST_PARAM = "";

    private List<String> LOG_TEST = null;

    private String DIR_PARAM = "";
    private String FIC_LOGS = DIR_PARAM + "";

    private LinkedHashMap<String, Object> MAP_TIME_LIGNE = new LinkedHashMap<>();
    private LinkedHashMap<String, Object> STATUS_TEST = new LinkedHashMap<>();

    private long MILLI_DEP = 0;
    private long MILLI_FIN = 0;
    private long MILLI_DIFF = 0;
    private long MILLI_TOT = 0;


    private List<String> FILES_UPLOADED = new ArrayList<>();

    //*****************************************************************************************
    public SubmitAndCreateDEsubfilesDE_001() throws IOException {
        ;

    }

    public SubmitAndCreateDEsubfilesDE_001(
            WebDriver dr, Properties testConf, WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String, Object> t_li, List<String> st) throws IOException {
        driver = dr;


        TEST_CONFIG = testConf;
        WAIT = wt;
        sUtil = sut;
        sHelp = sIni;
        MAP_TIME_LIGNE = t_li;
        LOG_TEST = st;
    }

    //*****************************************************************************************
    public void setUp() throws Exception {
        ;

    }

    public List<String> getFILES_UPLOADED(){
        return FILES_UPLOADED;
    }
    //********************************************************

    public Long TestSubmitAndCreateDEsubfilesDE_001() throws Exception {
        // d_upload =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE GR_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
        Map<String, Object> d_upload = new HashMap<>();
        AccesAndSubmitBordereau asbneg= new AccesAndSubmitBordereau(
                driver, TEST_CONFIG,WAIT,sUtil,
                sHelp,MAP_TIME_LIGNE,LOG_TEST );
        MILLI_DEP = System.currentTimeMillis();

        d_upload=asbneg.TestAccesAndSubmitBordereau();
        MILLI_FIN=(Long)d_upload.get("MILLI_TOT");

        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        //
        MAP_TIME_LIGNE.put("UPLOAD FILE", MILLI_DIFF);
        System.out.println("UPLOAD FILE" + MILLI_DIFF);

        AccesToHeliosMonitoring ta = new AccesToHeliosMonitoring(LOG_TEST);

        ta.setUp();
        LOG_TEST.add("CALL WaitPOR_009:WaitHML_015:WaitDE_001");
        sUtil.tempo(6);
        ta.trtMonitoringActions(d_upload,"FindTransID:WaitPOR_009:WaitHML_015:WaitDE_001:CLOSE");
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF=MILLI_FIN-MILLI_TOT;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        d_upload.replace("MILLI_TOT",MILLI_TOT);
        return MILLI_TOT;
    }


    public String getFromLogTest(String txt){
        String retS="";
        for (String sl:LOG_TEST){
            if (sl.contains(txt)) {
                String spl[] = sl.split("=");
                retS=spl[1].trim();
            }
        }
        return retS;
    }
    //********************************************************

    public void tearDown() {

        driver.quit();
    }

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        SubmitAndCreateDEsubfilesDE_001 logP = new SubmitAndCreateDEsubfilesDE_001();
        logP.setUp();
        logP.TestSubmitAndCreateDEsubfilesDE_001();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }




}