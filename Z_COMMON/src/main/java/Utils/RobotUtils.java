package Utils;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import static java.awt.event.KeyEvent.VK_BACK_SLASH;


/**
 * A Java Robot example class.
 *
 * Caution: Using the Java Robot class improperly can cause
 * a lot of system problems. I had to reboot my Mac ~10
 * times yesterday while trying to debug this code.
 *
 * I created this class to demonstrate the Java Robot
 * class on a Mac OS X system, though it should run on Linux
 * or Windows as well.
 *
 * On a Mac system, I place the TextEdit text editor in the
 * upper-left corner of the screen, and put a bunch of blank lines
 * in the editor. Then I run this Java Robot example from
 * Eclipse or the Unix command line.
 *
 * It types the three strings shown in the code below into
 * the text editor.
 *
 * Many thanks to the people on the Mac Java-dev mailing list
 * for your help.
 *
 * @author Alvin Alexander, http://devdaily.com
 *
 */
public class RobotUtils
{
    Robot robot = new Robot();



    public RobotUtils() throws AWTException
    {
        robot.setAutoDelay(40);
        robot.setAutoWaitForIdle(true);

//        robot.delay(4000);
//        robot.mouseMove(40, 130);
//        robot.delay(500);
//
//        leftClick();
//        robot.delay(500);
//        leftClick();
//
//        robot.delay(500);
//        type("Hello, world");
//
//        robot.mouseMove(40, 160);
//        robot.delay(500);
//
//        leftClick();
//        robot.delay(500);
//        leftClick();
//
//        robot.delay(500);
//        type("This is a test of the Java Robot class");
//
//        robot.delay(50);
//        type(KeyEvent.VK_DOWN);
//
//        robot.delay(250);
//        type("Four score and seven years ago, our fathers ...");
//
//        robot.delay(1000);
//        System.exit(0);
    }

    public void leftClick()
    {
        int mask = InputEvent.BUTTON1_DOWN_MASK;
        int mask2 = InputEvent.BUTTON1_MASK;
        robot.mousePress(mask2);
        robot.delay(300);
        robot.mouseRelease(mask2);
        robot.delay(300);
    }
    public void rightClick()
    {
        int mask = InputEvent.BUTTON3_DOWN_MASK;
        int mask2 = InputEvent.BUTTON3_MASK;
        robot.mousePress(mask2);
        robot.delay(300);
        robot.mouseRelease(mask2);
        robot.delay(300);
    }
    public void leftClickPAram(int x, int y) {
        robot.delay(1000);
        robot.mouseMove(x, y);
        robot.delay(600);
        leftClick();
        robot.delay(600);
    }
    public void rightClickPAram(int x, int y) {
        robot.delay(1000);
        robot.mouseMove(x, y);
        robot.delay(600);
        rightClick();
        robot.delay(600);
    }
    public void type(int i)
    {
        robot.delay(40);
        robot.keyPress(i);
        robot.keyRelease(i);
    }
    public void typeKeyPress(String scode)
    {
        robot.delay(90);
        switch (scode){
            case "ENTER":
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
                break;
            case "TAB":
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_TAB);
                break;
            case "DELETE":
                robot.keyPress(KeyEvent.VK_DELETE);
                robot.keyRelease(KeyEvent.VK_DELETE);
                break;
            case "ESCAPE":
                robot.keyPress(KeyEvent.VK_ESCAPE);
                robot.keyRelease(KeyEvent.VK_ESCAPE);
                break;
            case "VK_CANCEL":
                robot.keyPress(KeyEvent.VK_CANCEL);
                robot.keyRelease(KeyEvent.VK_CANCEL);
                break;
            case "BACK_SPACE":
                    robot.keyPress(KeyEvent.VK_BACK_SPACE);
                robot.keyRelease(KeyEvent.VK_BACK_SPACE);
                break;
            case "ARROW_DOWN":
                robot.keyPress(KeyEvent.VK_DOWN);
                robot.keyRelease(KeyEvent.VK_DOWN);
                break;

            case "ARROW_UP":
                robot.keyPress(KeyEvent.VK_UP);
                robot.keyRelease(KeyEvent.VK_UP);
                break;
            case "PAGE_DOWN":
                robot.keyPress(KeyEvent.VK_PAGE_DOWN);
                robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
                break;

            case "PAGE_UP":
                robot.keyPress(KeyEvent.VK_PAGE_UP);
                robot.keyRelease(KeyEvent.VK_PAGE_UP);
                break;
            case "CTRL_V":
                robot.delay(40);
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_CONTROL);
                break;



        };
    }

    public void typeAlpha(String s) {
        System.out.println(s);
        byte[] bytes = s.getBytes();
        for (byte b : bytes) {
            System.out.println(b);
            int code = b;
            boolean shif = false;
            if (code > 47 && code < 58) {
                shif = true;
            } else if (code > 65 && code < 91) {
                shif = true;
            }
            int keyCode = KeyEvent.getExtendedKeyCodeForChar(code);
            if (KeyEvent.CHAR_UNDEFINED == keyCode) {
                throw new RuntimeException(
                        "Key code not found for character '" + code + "'");
            }
            if (shif) {
                robot.keyPress(KeyEvent.VK_SHIFT);
                robot.keyPress(keyCode);
                robot.keyRelease(keyCode);
                robot.keyRelease((KeyEvent.VK_SHIFT));
            } else if(code==92) {
                keyCode='8';
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_ALT);
                robot.keyPress(keyCode);
                robot.keyRelease(keyCode);
                robot.keyRelease(KeyEvent.VK_ALT);
                robot.keyRelease(KeyEvent.VK_CONTROL);
            } else if(code==95) {
                keyCode='8';
               //robot.keyPress(KeyEvent.VK_SHIFT);
               robot.keyPress(keyCode);
               robot.keyRelease(keyCode);
               //robot.keyRelease(KeyEvent.VK_SHIFT);
        } else if(code==46) {
            keyCode=';';
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(keyCode);
            robot.keyRelease(keyCode);
            robot.keyRelease(KeyEvent.VK_SHIFT);
        }
            else
                robot.keyPress(keyCode);
                robot.keyRelease(keyCode);
            }
            //System.out.println(keyCode);

    }
    
    
    
    public static void main(String[] args) throws AWTException
    {
        new RobotUtils();
    }
}