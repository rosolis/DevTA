package Utils;


import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class Run_UploadData {

    private String TEST_NAME="";
    private String FILE_PARAM="";
    private String EXCEL_FILE="TS01_PORTAL_REPORT_V02.xlsx";
    private String EXCEL_FEUILLE  ="TEST_DataSet";

    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String GLOBAL_URL="02_LOG/";
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String DIR_TS_BAT="09_DATASET/";
    private String DIR_TS_PARAM="09_DATASET_PARAM/";

    private String LAUNCH_ALL_NAME="00_LAUNCH_ALL";
    private ExcelRead EXC = new ExcelRead();
    private String      DIRE_PARAM="";

    private String      DIRE_LAUNCH="";


    private HashMap<String,String> MAP_USER=new HashMap<>();
    private HashMap<String,String> MAP_PSW=new HashMap<>();
    private FileAndLog fLog = null;

    //********************************************************
    //********************************************************


    public Run_UploadData() throws IOException {
        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");

        if (isOperatingSystemWindows) {
            DIR_ROOT_TS="C:"+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=dire_home+DIR_ROOT_TS;
        }

        DIR_TS_PARAM=DIR_RUN+DIR_TS_PARAM;

        EXCEL_FILE=DIR_ROOT_TS+EXCEL_FILE;

        DIRE_PARAM=DIR_TS_PARAM;
        DIRE_LAUNCH=DIR_RUN+DIR_TS_BAT;

        fLog = new FileAndLog();
        ExcelRead EXC = new ExcelRead();
        MAP_USER.put("CS-1","TaCedentSuper-1@outlook.com");
        MAP_USER.put("CS-2","TaCedentSuper-2@outlook.com");
        MAP_USER.put("CS-3","TaCedentSuper-3@outlook.com");
        MAP_USER.put("CS-4","TaCedentSuper-4@outlook.com");
        MAP_USER.put("CB-1","TaCedentBasic-1@outlook.com");
        MAP_USER.put("CB-2","TaCedentBasic-2@outlook.com");
        MAP_USER.put("CB-3","TaCedentBasic-3@outlook.com");
        MAP_USER.put("CB-4","TaCedentBasic-4@outlook.com");
        MAP_USER.put("EP-1","TaExternalPartner-1@outlook.com");

        MAP_PSW.put("CS-1","1CedentSuper-1");
        MAP_PSW.put("CS-2","1CedentSuper-2");
        MAP_PSW.put("CS-3","1CedentSuper-3");
        MAP_PSW.put("CS-4","1CedentSuper-4");
        MAP_PSW.put("CB-1","1CedentBasic-1");
        MAP_PSW.put("CB-2","1CedentBasic-2");
        MAP_PSW.put("CB-3","1CedentBasic-3");
        MAP_PSW.put("CB-4","1CedentBasic-4");
        MAP_PSW.put("EP-1","1ExternalPartner-1");
    }
    public void trtAllLines() throws IOException {

        EXC.set_File_Feuille(EXCEL_FILE,EXCEL_FEUILLE);
        EXC.openExcelFile();
        EXC.readAllExcel();
        int nb=EXC.get_nbLign();
        int lecLign=11;
        String os = System.getProperty("os.name").toLowerCase();

        if(os.contains("win")){
            LAUNCH_ALL_NAME=LAUNCH_ALL_NAME+".bat";
            creerLanceToutWin();
        }else{
            LAUNCH_ALL_NAME=LAUNCH_ALL_NAME+".sh";
            creerLanceToutLin();
        }

        while(EXC.get_valCol(lecLign,0).length()>1 && lecLign<nb) {
            String type="";
            List<String> fileT=new ArrayList<>();
            String NomTest=EXC.get_valCol(lecLign,0);
            NomTest=NomTest.trim();
            String NomTestBat=EXC.get_valCol(lecLign,1).trim();
            NomTestBat=NomTestBat.replace(" ","");
            NomTestBat=NomTestBat.replace(")","");
            NomTestBat=NomTestBat.replace("(","");
            NomTestBat=(lecLign+1)+"_"+NomTestBat;
            NomTest=(lecLign+1)+"_"+NomTest;
            String param[]=EXC.get_valCol(lecLign,2).split("\\n");

            fileT.add("CNX_LOGIN_PORTAL   = https://aeenintas-helios-portal-r1.azurewebsites.net");
            fileT.add("CNX_BROWSER      = FIREFOX");
            String tmp1[]=NomTest.split("_");
            String taUser=MAP_USER.get(tmp1[2]);
            String taPsw =MAP_PSW.get(tmp1[2]);
            fileT.add("CNX_TA_USER      = "+taUser);
            fileT.add("CNX_TA_PASSWORD  = "+taPsw);
            fileT.add("SENDING_TYPE     = FIXE");

            String typEdat[]=param[0].split("-");
            if (tmp1[1].contains("-SOR")){
                fileT.add("DIR_FILE_TO_SEND=  01_OTHER_REQUESTS/");
                type=" SOR ";
            }else{
                type=" SB ";
                String tmp2[]=typEdat[0].split("=");
                String taType=tmp2[1].trim();
                fileT.add("TYPE_BORDEREAU = "+taType);

                tmp2=typEdat[1].split("=");
                String taDates=tmp2[1].trim()+"-"+typEdat[2].trim();
                fileT.add("DATE_BORDEREAU  = "+taDates);
                fileT.add("DIR_FILE_TO_SEND=  00_BORDEREAUX/");
            }

            String liF="FILE_TO_SEND=";
            String sep=" ";
            // Crea upload file name
            for (int x=2;x<param.length-1;x++){
                tmp1=param[x].split("=");
                int nombreF=Integer.parseInt(tmp1[0].trim());
                String nomFile[]=tmp1[1].split("\\.");
                String ty=nomFile[1];
                String name[]=nomFile[0].split("_");
                int numF=Integer.parseInt(name[2]);
                for (int z=numF;z<numF+nombreF;z++){
                    String sNumF=String.format("%03d",z);
                    liF+=sep+name[0]+"_"+name[1]+"_"+sNumF+"."+ty;
                    sep=";";
                }
            }
            fileT.add(liF);

            System.out.println(DIR_ROOT_TS+DIRE_PARAM+NomTest);
            fLog.Add_Lines(fileT,DIR_ROOT_TS+DIRE_PARAM+NomTest+".txt");
            //Creation Bat files
           // String os = System.getProperty("os.name").toLowerCase();
            if(os.contains("win")){
                CreaFileBatDataSet(DIR_ROOT_TS+DIRE_LAUNCH+NomTestBat+".bat",NomTest+".txt",type);
                System.out.println("file BAT = "+NomTestBat+".bat");
                ajoutLanceToutWin(NomTest+".txt","SB");
            }else{
                creerShellLinux(DIR_ROOT_TS+DIRE_LAUNCH+NomTestBat+".sh",NomTest+".txt",type);
                System.out.println("file Sh = "+NomTestBat+".sh");
                ajoutLanceToutLin(NomTest+".txt",type);
            }


            lecLign++;
        }
    }
    public List<String> extractLigne(){
        List<String> lRet=new ArrayList<>();


        return lRet;
    }
    public void CreaFileBatDataSet(String fileBat, String fileParam,String ty) throws IOException {
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);



        fileT.add("ECHO OFF");
        //System.out.println(" Ouvre ="+fichierParam);
        fileT.add("Title \"Test "+fileBat+"\"");
        fileT.add("REM #Launch test Submit borderau for DataSet");
        fileT.add("REM ");
        fileT.add("REM  SB = submit bordereau ");
        fileT.add("REM ");
        fileT.add("java -jar ..\\Ta_Portal_TS1.jar "+ty+ "09_DATASET_PARAM\\"+fileParam );

        fLog.Add_Lines(fileT,fileBat);
    }



    public void creerShellLinux(String fileBat, String fileParam,String ty) throws IOException {
        // vérifier si la dir existe
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);

        fileT.add("#!/bin/bash");
        fileT.add("# Launch test Submit borderau for DataSet");
        fileT.add("# ");
        fileT.add("#   SB = submit bordereau ");
        fileT.add("# ");
        fileT.add("java -jar ../Ta_Portal_TS1.jar "+ty+ "09_DATASET_PARAM/"+fileParam );
        write_Ls_Linux(fileT,fileBat);


//        file.setReadable(true, false);
//        file.setExecutable(true, false);
//        file.setWritable(true, false);
    }
    public void write_Ls_Linux(List<String> fileT,String fileBat) throws IOException {
        File fg = new File(fileBat);
        if (fg.exists()) {
            fg.delete();
        }
        try (
                final BufferedWriter writer = Files.newBufferedWriter(fg.toPath(),
                        StandardCharsets.UTF_8, StandardOpenOption.CREATE);
        ) {
            for (final String line: fileT) {
                writer.write(line+"\n");

            }
            // Not compulsory, but...
            writer.flush();
        }
    }
    //**************************************
    public void creerLanceToutWin() throws IOException{
        // vérifier si la dir existe

        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH+LAUNCH_ALL_NAME;
        System.out.println("bat="+fileBat);
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);
        fileT.add("ECHO OFF");
        fLog.Add_Lines(fileT,fileBat);
    }

    public void ajoutLanceToutWin( String fileParam, String ty) throws IOException {


        String fileBat = DIR_ROOT_TS + DIRE_LAUNCH+LAUNCH_ALL_NAME ;
        System.out.println(" AJOUT =" + ty);
        List<String> fileT = new ArrayList<>();
        fileT.add("echo =======");

        fileT.add("java -jar ../Ta_Portal_TS1.jar "+ty+" 09_DATASET/"+fileParam );
        fileT.add("\ntimeout 5 > NUL");

        fileT.add("echo***********************************************************************");
        fLog.Add_Lines(fileT,fileBat);
    }
    public void creerLanceToutLin() throws IOException{
        // vérifier si la dir existe
        

        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH+LAUNCH_ALL_NAME;
        System.out.println("sh="+fileBat);
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);
        fileT.add("#!/bin/bash");
        fileT.add("#");
        fileT.add("#");
        fLog.Add_Lines(fileT,fileBat);
    }

    public void ajoutLanceToutLin( String fileParam, String ty) throws IOException {


        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH + LAUNCH_ALL_NAME ;
        System.out.println(" AJOUT =" + ty);
        List<String> fileT = new ArrayList<>();
        fileT.add("echo =======");

        fileT.add("java -jar ../Ta_Portal_TS1.jar "+ty+" 09_DATASET/"+fileParam );
        fileT.add("echo sleep 10");

        fileT.add("echo***********************************************************************");
        fLog.Add_Lines(fileT,fileBat);
    }


    /********************************************************************************************************
     * MAIN
     * @throws IOException
     *
     ********************************************************************************************************/
    public static void main(String[] args) throws IOException {
        // Instance
        Run_UploadData re =new Run_UploadData();
        re.trtAllLines();
    }
}
