package Utils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.util.*;

public class CsvRead {

	private List<List<String>> L_LIGNES_CSV = new ArrayList<List<String>>();
    private List<Object> L_OBJ = new ArrayList<Object>();


	private List<String> L_TITRES_CSV = new ArrayList<String>();
	private List<String> L_TITRES_TYP = new ArrayList<String>();
	private List<String> ERREURS = new ArrayList<String>();

	private String CODAGE = "";
	private String SEPARE = ";";
	private String SEPARE_Split = "";
	private String NOM_SEP = "";
	private boolean Check_csv = true;

	// ************ Fichier Csv ************
	private String FICHIER_CSV = "";
	private String DIRE = "";

	private boolean Enleve_Guillemets = false;

	private int PAGE_CUR = 0;
	private int PAGE_MAX = 1;
	private int PAGE_NBli = 10000;
	private int LIGN_CUR = 0;
	private int MAX_COL = 0;
	private int MAX_NB_LIGN = 0;
	private BufferedReader READER_F = null;

	/********************************************************************************************************
	 * CONSTRUCTEUR Author Roberto SOLISConstructeur du test
	 * 
	 * @throws IOException
	 * @throws Exception
	 ******************************************************************************************************/

	public CsvRead() throws IOException {
		String dire_lancement = System.getProperty("user.dir");
		//System.out.println("fichier param rapport de test:" + dire_lancement);
		// f_Dire_ParamTa = dire_lancement+"/ParamUpDocuMongo.txt";
		// f_fichier_result = dire_lancement+"/RESULT_CmpMongo.txt";
		L_LIGNES_CSV = new ArrayList<List<String>>();
	}

	/*
	 * ****************************************************************
	 */
	public int compteColFromCsv() throws IOException {
		Path path = Paths.get(FICHIER_CSV);
		BufferedReader reader = null;
		if (CODAGE.contains("8859"))
			reader = Files.newBufferedReader(path, StandardCharsets.ISO_8859_1);
		else
			reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);

		String lin = reader.readLine();
		String[] partsL = lin.split(SEPARE_Split);
		MAX_COL = partsL.length;
		return MAX_COL;
	}

	public int compteLignesFile() throws IOException {
		// ouvre file et compte lines

		// System.out.println("Param " + n_fichier +" par2 "+pattern);
		BufferedReader reader = crea_Reader();
		int nb_docu = 0;
		if (reader == null)
			System.out.println("Erreur de codage du fichier ");
		else {
			for (String lin = reader.readLine(); (lin != null)&&(lin.length()>2); lin = reader.readLine()) {
				// lin=lin.trim();
				lin += " ";
				nb_docu++;
//				if(nb_docu<65)
//					System.out.println(nb_docu);
//				 System.out.println(lin);
			}
			reader.close();
		}
		return nb_docu;
	}

	// **************************************************************
	public int compte_nb_lignes_Csv() throws IOException {
		// compte lines CSV avec controle nb colonnes
		// READER doit etre ouvert
		int nb = 0;
		ERREURS = new ArrayList<String>();
		for (String lin = READER_F.readLine(); lin != null; lin = READER_F.readLine()) {
			lin = lin.trim();
			lin += " ";
			List<String> l = trt_LineFcsv(lin, nb);
			nb++;
		}
		return nb;
	}
	// **************************************************************
		public int compte_All_lignes(String sF) throws IOException {
			// compte lines CSV avec controle nb colonnes
			// READER doit etre ouvert
			set_DIR_et_Nom_FICHIER(sF);
			READER_F=crea_Reader();
			int nb = 0;
			ERREURS = new ArrayList<String>();
			for (String lin = READER_F.readLine(); lin != null; lin = READER_F.readLine()) {
				nb++;
			}
			close_reader();
			return nb;
		}
	// ********************************************************************

	public int chargePageFromCsv(int page) throws IOException {
		L_LIGNES_CSV = new ArrayList<List<String>>();
		L_TITRES_CSV = new ArrayList<String>();

		Path path = Paths.get(FICHIER_CSV);
		// System.out.println("Param " + n_fichier +" par2 "+pattern);
		BufferedReader reader = crea_Reader();

		int nb_li = 0;
		boolean titres = true;
		String separe1 = ";";
		if (SEPARE.length() > 0)
			separe1 = SEPARE;

		for (String l_lu = reader.readLine(); l_lu != null; l_lu = reader.readLine()) {
			// lin=lin.trim();
			// pointer la page : page = ligne MOD 1000 +1
			int nl = (((nb_li + 1) / PAGE_NBli));
			if (nl == PAGE_CUR) {
				l_lu += " ";
				if (l_lu.length() > 2) {
					String[] partsL = null;
					if (separe1.contains("|"))
						separe1 = "\\|";
					partsL = l_lu.split(separe1);
					if (titres) {
						for (int i = 0; i < partsL.length; i++) {
							String d1 = partsL[i];
							if (d1.contains("\""))
								d1 = enleveGui(d1, nb_li);
							L_TITRES_CSV.add(d1);
						}
						MAX_COL = partsL.length;
						titres = false;
					} else {
						List<String> list_st = new ArrayList<String>();
						for (int i = 0; i < partsL.length; i++) {
							String d1 = partsL[i];
							if (d1.contains("\""))
								d1 = enleveGui(d1, nb_li);
							list_st.add(d1);
						}
						nb_li++;
						if (MAX_COL != partsL.length) {
							String s = "ERREUR FORMAT FILE CSV LIGNE= " + (nb_li + 1) + " nb col attendu = " + MAX_COL + " nb col trouvés " + partsL.length;
							System.out.println(s);
							ERREURS.add(s);
							System.out.println(s);
						}
						if ((nb_li + 1) % 1000 == 0)
							System.out.println("Ligne= " + (nb_li + 1) + "\n" + list_st);
						L_LIGNES_CSV.add(list_st);
					}
				}

			}

		}
		reader.close();
		MAX_NB_LIGN = L_LIGNES_CSV.size();
		return nb_li;
	}
	// ********************************************************************

	public int chargeDataFromCsv() throws IOException {
		L_LIGNES_CSV = new ArrayList<List<String>>();
		L_TITRES_CSV = new ArrayList<String>();

		open_reader();

		int nb_docu = 0;
		L_TITRES_CSV = read_Titres_CSV();

		String separe1 = ";";
		if (SEPARE.length() > 0)
			separe1 = SEPARE;
		LIGN_CUR++;

		List<String> list_st = read_Line_CSV(LIGN_CUR);

		while (list_st != null) {
			nb_docu++;
			if ((nb_docu + 1) % 1000 == 0)
				System.out.println("Ligne= " + (nb_docu + 1) + "\n" + list_st);
			L_LIGNES_CSV.add(list_st);
			LIGN_CUR++;
			list_st = read_Line_CSV(LIGN_CUR);
		}
		close_reader();
		return nb_docu;
	}

    public int chargeParHeure(String file,int col) throws IOException {

        FICHIER_CSV=file;
        L_LIGNES_CSV = new ArrayList<List<String>>();
        L_TITRES_CSV = new ArrayList<String>();
        for (int i = 0 ; i < 24 ;i++){
            L_OBJ.add(new ArrayList<List<String>>());
        }
        open_reader();

        int nb_docu = 0;
        L_TITRES_CSV = read_Titres_CSV();

        String separe1 = ";";
        if (SEPARE.length() > 0)
            separe1 = SEPARE;
        LIGN_CUR++;

        List<String> list_st = read_Line_CSV(LIGN_CUR);

        while (list_st != null) {

            String[]  st= list_st.get(col).split(" ");
            int hr = Integer.parseInt(st[1].substring(0,2));

            List<List<String>> Ls= (List<List<String>>) L_OBJ.get(hr);

            Ls.add(list_st);

            list_st = read_Line_CSV(LIGN_CUR);
        }
        close_reader();
        return nb_docu;
    }

	// ********************************************************************

	public int chargeCsvAvecTyp() throws IOException {
		L_LIGNES_CSV = new ArrayList<List<String>>();
		L_TITRES_CSV = new ArrayList<String>();
		L_TITRES_TYP = new ArrayList<String>();

		Path path = Paths.get(FICHIER_CSV);
		// System.out.println("Param " + n_fichier +" par2 "+pattern);
		BufferedReader reader = crea_Reader();

		int nb_docu = 0;
		int li_tt = 2;
		String separe1 = ";";
		if (SEPARE.length() > 0)
			separe1 = SEPARE;

		for (String lin = reader.readLine(); lin != null; lin = reader.readLine()) {
			// lin=lin.trim();
			lin += " ";
			if (lin.length() > 2) {
				String[] partsL = null;
				if (separe1.contains("|"))
					separe1 = "\\|";
				partsL = lin.split(separe1);
				if (li_tt == 2) {
					for (int i = 0; i < partsL.length; i++) {
						String d1 = partsL[i];
						if (d1.contains("\""))
							d1 = enleveGui(d1, nb_docu);
						L_TITRES_CSV.add(d1);
					}
					MAX_COL = partsL.length;
					li_tt--;
				} else if (li_tt == 1) {
					for (int i = 0; i < partsL.length; i++) {
						String d1 = partsL[i];
						if (d1.contains("\""))
							d1 = enleveGui(d1, nb_docu);
						L_TITRES_TYP.add(d1);
					}
					MAX_COL = partsL.length;
					li_tt--;
				} else {
					List<String> list_st = new ArrayList<String>();
					for (int i = 0; i < partsL.length; i++) {
						String d1 = partsL[i];
						if (d1.contains("\""))
							d1 = enleveGui(d1, nb_docu);
						list_st.add(d1);
					}
					nb_docu++;
					if (MAX_COL != partsL.length) {
						String s = "ERREUR FORMAT FILE CSV LIGNE= " + (nb_docu + 1) + " nb col attendu = " + MAX_COL + " nb col trouvés " + partsL.length;
						System.out.println(s);
						ERREURS.add(s);
						System.out.println(s);
					}
					if ((nb_docu + 1) % 1000 == 0)
						System.out.println("Ligne= " + (nb_docu + 1) + "\n" + list_st);
					L_LIGNES_CSV.add(list_st);
				}
			}

		}
		reader.close();
		return nb_docu;
	}

	/*******************************************
	 * 
	 * @author frso
	 *
	 */
	// **************************************************************
	public void open_reader() throws IOException {

		READER_F = crea_Reader();
		LIGN_CUR = 0;
	}

	public void close_reader() throws IOException {
		READER_F.close();
		READER_F = null;
	}

	// public String read_LineFcsv0() throws IOException {
	// String l_lu = READER_F.readLine();
	// return l_lu;
	// }

	public String read_LineTxt() throws IOException {
		String l_lu = READER_F.readLine();
		return l_lu;
	}

	public List<String> read_Line_CSV(int lcur) throws IOException {
		List<String> list_st = new ArrayList<String>();

		String lin = READER_F.readLine();
		if (lin == null)
			return null;
		String separe1 = ";";
		if (SEPARE.length() > 0)
			separe1 = SEPARE;
		// lin=lin.trim();
		lin += " ";
		if (lin.length() > 2) {
			String[] partsL = null;
//			if (separe1.contains("|"))
//				separe1 = "\\|";
			partsL = lin.split(SEPARE_Split);
			for (int i = 0; i < partsL.length; i++) {
				String d1 = partsL[i];
				if (d1.contains("\""))
					d1 = enleveGui(d1, i);
				list_st.add(d1);
			}
			if (MAX_COL != partsL.length) {
				String s = "ERREUR FORMAT FILE CSV LIGNE= " + (lcur + 1) + " nb col attendu = " + MAX_COL + " nb col trouvés " + partsL.length;
				System.out.println(s);
				ERREURS.add(s);
			}
		}
		return list_st;
	}

	// **************************************************************
	public List<String> trt_LineFcsv(String l_lu, int li) {
		// String l_lu = READER_F.readLine();

		String[] partsL = null;
		List<String> list_st = new ArrayList<String>();
		String separe1 = ";";
		if (SEPARE.length() > 0)
			separe1 = SEPARE;
		if (separe1.contains("|"))
			separe1 = "\\|";
		partsL = l_lu.split(SEPARE_Split);
		for (int i = 0; i < partsL.length; i++) {
			String d1 = partsL[i];
			if (d1.contains("\""))
				d1 = enleveGui(d1, li);
			list_st.add(d1);
		}

		if ((Check_csv) && (MAX_COL != partsL.length)) {
			String s = "ERREUR FORMAT FILE CSV LIGNE= " + (li + 1) + " nb col attendu = " + MAX_COL + " nb col trouvés " + partsL.length;
			System.out.println(s);
			ERREURS.add(s);
		}
		return list_st;

	}

	// **************************************************************
	public String enleveGui(String d1, int li) {
		if (!Enleve_Guillemets)
			return d1;
		if (d1.contains("\"\""))
			return " ";
		else {
			int deb = d1.indexOf("\"");
			int fin = d1.lastIndexOf("\"");
			if ((Check_csv) && (deb == fin)) {
				String s = "ERREUR Cellule; ligne " + (li + 1) + " ; contenu ={" + d1 + "} manque un guillemet";
				System.out.println(s);
				ERREURS.add(s);
				return d1;
			}
			return d1.substring(deb + 1, fin).trim();
		}
	}

	// **************************************************************
	public List<String> read_Titres_CSV() throws IOException {
		// init MAX_COL
		String li = read_LineTxt();
		String separe1 = decide_separeCsv(li);
		String[] partsL = li.split(SEPARE_Split);

		for (int i = 0; i < partsL.length; i++) {
			String d1 = partsL[i];
			if (d1.contains("\""))
				d1 = enleveGui(d1, 0);
			L_TITRES_CSV.add(d1.trim());
		}
		MAX_COL = partsL.length;
		return L_TITRES_CSV;
	}

	public String decide_separeCsv(String lin) throws IOException {
		if (SEPARE.length()>0) {
			SEPARE_Split=SEPARE;
			if (SEPARE.contains("|"))
				SEPARE_Split = "\\"+SEPARE;
			return SEPARE;
		}
		SEPARE = ";";
		lin += " ";
		if (lin.contains(";"))
			SEPARE = ";";
		else if (lin.contains(","))
			SEPARE = ",";
//		else if (lin.contains("|"))
//			SEPARE = "\\|";
		else if (lin.contains(":"))
			SEPARE = ":";
		else if (lin.contains("="))
			SEPARE = "=";
		else if (lin.contains("\t"))
			SEPARE = "\t";
		else {
			int deb = lin.indexOf("\"");
			if (deb == -1)
				SEPARE = " ";
			else {
				int deb1 = lin.indexOf("\"", deb + 1);
				int deb2 = lin.indexOf("\"", deb1 + 1);
				// System.out.println("deb="+deb+"deb1="+deb1+"deb2="+deb2);
				SEPARE = lin.substring(deb1 + 1, deb2);
			}
		}
		SEPARE_Split=SEPARE;
		return SEPARE;
	}

	// **************************************************************
	public String decide_CODAGE_File() throws IOException {
		CODAGE = "";
		byte fbits[] = getByte(DIRE + FICHIER_CSV);
		int i_ret=detect_UTF8(fbits);

		if (i_ret==1)
			CODAGE = "UTF_8";
		else if (i_ret==2)
			CODAGE = "UTF_16";
		else if  (i_ret==3)
			CODAGE = "UTF_8/-8859_1";
		
		if (CODAGE.length() > 0)
			return CODAGE;

		Path path = Paths.get(DIRE + FICHIER_CSV);

		BufferedReader reader = null;
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("win")) {
			try {
				reader = Files.newBufferedReader(path, StandardCharsets.ISO_8859_1);
				String lin = reader.readLine();
				CODAGE = "ISO_8859_1";
				reader.close();
				return CODAGE;
			} catch (IOException e) {
				System.err.println(e);
				CODAGE = "NONE";
				reader.close();
			}
		}
		try {
			reader = Files.newBufferedReader(path, StandardCharsets.US_ASCII);
			String lin = reader.readLine();
			//CODAGE = "US_ASCII";
            CODAGE = "UTF_8/SB";
			reader.close();

		} catch (IOException e) {
			System.err.println(e);
			CODAGE = "UTF_8";
			reader.close();

		}

		//System.out.println(CODAGE);
		return CODAGE;
	}
	//***************************************
	public int detect_UTF8(byte[] b) {
	int ret=0;
	//System.out.println(b[0]+" ddd  "+ b[1]);

	if ((b[1]== -17) && (b[0]== -69))
	  return 1;
	if ((b[1]== -2) && (b[0]== -1))
	  return 2;	
	// recherche char C2 et C3 + >80
	int lg=b.length;
	int nbExtar=0;
	for (int i=0 ;i<lg-1 ;  i++) {
		//System.out.println(b[i]);
		if (b[i]== -62) {		// C2
			if ((b[i+1]>= -95)&&(b[i+1]<= -5)) {  // >A1 <FB
				System.out.println("==>"+" qqq  "+b[i+1]);
				return 1;
			}
		}
		else if (b[i]== -61) {			//C3
			if ((b[i+1]> -128)&&(b[i+1]<= -65)) { // >=80 <=BF
				System.out.println("==>"+" qqq  "+b[i+1]);
				return 1;
			}
		}if ((b[i]>= -128)&&(b[i]<= -1)) {
			nbExtar++;
		}
	}
	if (nbExtar==0)
		return 3;	// peut être lu 
	return ret;
}
	
	public byte[] getByte(String path) {
	    byte[] getBytes = {};
	    try {
	        File file = new File(path);
	        getBytes = new byte[(int) file.length()];
	        InputStream is = new FileInputStream(file);
	        is.read(getBytes);
	        is.close();
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return getBytes;
	}
//***************************************
	public BufferedReader crea_Reader() throws IOException {
		// decide_Codage et deja appele
		Path path = Paths.get(DIRE + FICHIER_CSV);
		//System.out.println("Open DIR " +DIRE+ " File = "+FICHIER_CSV);
		BufferedReader reader = null;
		if (CODAGE.length()<2)
			decide_CODAGE_File();
		if (CODAGE.contains("UTF_8"))
			reader = Files.newBufferedReader(path, StandardCharsets.UTF_8);
		else if (CODAGE.contains("UTF_16"))
			reader = Files.newBufferedReader(path, StandardCharsets.UTF_16);
		else if (CODAGE.contains("8859"))
			reader = Files.newBufferedReader(path, StandardCharsets.ISO_8859_1);
		else
			reader = Files.newBufferedReader(path, StandardCharsets.US_ASCII);
		READER_F = reader;
		return reader;
	}
//	/***************************************
	public String read_Only_Line_No(int iNb) throws IOException {
		String li="";

		crea_Reader();
		//note ligne0= titres;
		for (int i=0; i< iNb+1 ; i++) {
			li = READER_F.readLine();
		}
		close_reader();
		return li;
		
	}
	public List<String> read_One_Line_List(int iNb) throws IOException {
		List<String> lRet=new ArrayList<String>();

		crea_Reader();
		String li="";
		//note ligne0= titres;
		for (int i=0; i< iNb+1 ; i++) {
			li = READER_F.readLine();
		}
		close_reader();
		String sp[]=li.split(";");
		for (String s:sp)
			lRet.add(s);

		return lRet;

	}
	/********************************************************************************************************
	 * Methodes WRITE
	 ********************************************************************************************************/

	@SuppressWarnings("unchecked")
	public void write_Add_CvsListString(List<String> l_t, String fichier) {
		List<String> s = new ArrayList<String>();
		s.add(list_Str_to_csvLine(l_t));
		write_Add_Lines(s, fichier);
	}


	public void write_Add_String(String st, String fichier) {
		List<String> s = new ArrayList<String>();
		s.add(st);
		write_Add_Lines(s, fichier);
	}

	public void write_Add_Lines(List<String> ligne, String fichier) {
		System.setProperty("file.encoding", "UTF-8");
		Charset charset = Charset.forName("UTF8");
		File fg = new File(fichier);
		Path tracePath = Paths.get(fichier);
		try {
			if (fg.exists()) {
				Files.write(tracePath, ligne, charset, StandardOpenOption.APPEND);
			} else
				Files.write(tracePath, ligne, charset, StandardOpenOption.CREATE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write_del_File(String fichier) {
		// del fichier
		File fg = new File(fichier);
		if (fg.exists()) {
			fg.delete();
		}
	}

	// **********
	public void write_AllCsv(List<String> ti, List<List<Object>> list_o, String fichier) throws IOException {
		write_AllCsv(ti, list_o, fichier,"UTF-8");
	}
	public void write_AllCsv(List<String> ti, List<List<Object>> list_o, String fichier,String encode) throws IOException {

		Charset charset = Charset.forName(encode);
		// del fichier
		File fg = new File(fichier);
		if (fg.exists()) {
			fg.delete();
		}
		Writer out;
		try {
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichier), charset));

			String s = list_Str_to_csvLine(ti);
			out.write(s + "\n");

			for (List<Object> lo : list_o) {

				s = list_Obj_to_csvLine(lo);
				out.write(s + "\n");
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String list_Str_to_csvLine(List<String> l_t) {
		String ret = "";
		String cmp = "";
		for (String s : l_t) {
			ret += cmp + s.trim();
			cmp = SEPARE;
		}
		return ret;
	}

	public String list_Obj_to_csvLine(List<Object> l_t) {
		String ret = "";
		String cmp = "";
		for (Object s : l_t) {
			ret += cmp + s.toString().trim();
			cmp = SEPARE;
		}
		return ret;
	}

	/*************************************************
	 * Génére date-heure au format X = 010100hhmm avec les 4 dernier char qui ont l'heure et minutes
	 * 
	 *************************************************/
	public String genereDateLa(int n) {
		// Génére string date au format jjmmaaaa
		Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
		int tDay = localCalendar.get(Calendar.DATE);
		int tMonth = localCalendar.get(Calendar.MONTH) + 1;
		int tYear = localCalendar.get(Calendar.YEAR);
		Date tTime = localCalendar.getTime();
		int tHeure = localCalendar.get(Calendar.HOUR_OF_DAY);
		int tMin = localCalendar.get(Calendar.MINUTE);
		int tsec1 = localCalendar.get(Calendar.SECOND);
		int tsec2 = localCalendar.get(Calendar.MILLISECOND);

		if (n == 1) { // YYYYMMDDhhmmss
			return String.format("%s%02d%02d%02d%02d%02d", tYear, tMonth, tDay, tHeure, tMin, tsec1);
		}
		if (n == 2) { // MMDDhhmmss
			return String.format("%02d%02d%02d%02d%02d", tMonth, tDay, tHeure, tMin, tsec1, tsec2);
		}

		if (n == 3) { // YYYYMMDD
			return String.format("%s%02d%02d", tYear, tMonth, tDay);
		}
		if (n == 4) { // YYYY-MM-DDThh:mm:ss
			return String.format("%s-%02d-%02dT%02d:%02d:%02d", tYear, tMonth, tDay, tHeure, tMin, tsec1);
		}
		if (n == 5) { // YYYY-MM-DD
			return String.format("%s-%02d-%02d", tYear, tMonth, tDay);
		}
		if (n == 6) { // YYYYMMDDhhmm
			return String.format("%s%02d%02d%02d%02d", tYear, tMonth, tDay, tHeure, tMin);
		}
		if (n == 7) { // JJMMAAAA
			return String.format("%02d%02d%s", tDay, tMonth, tYear);
		}
		if (n == 8) { // AAAAMMJJ
			return String.format("%s%02d%02d", tYear, tMonth, tDay);
		}
		if (n == 9) { // DDhhmmss
			return String.format("%02d%02d%02d%02d", tDay, tHeure, tMin, tsec1);
		}

		return String.format("%s-%02d-%02d %02d-%02d-%02d-%d", tYear, tMonth, tDay, tHeure, tMin, tsec1, tsec2);

	}

	/********************************************************************************************************
	 * get and set Methodes
	 ********************************************************************************************************/
	// returne le contenu d'une ligne du fichier 1
	public List<String> get_LineFcsv(int nli) {
		List<String> sLi = new ArrayList<String>();
		sLi = L_LIGNES_CSV.get(nli);

		return sLi;
	}

	// returne le contenu d'une ligne du fichier elimine le surplus vide
	public List<String> get_LineFcsv_onlyData(int nli) {
		List<String> oLi = new ArrayList<String>();
		int sizL = 0;
		oLi = L_LIGNES_CSV.get(nli);
		for (int i = 0; i < oLi.size(); i++) {
			String s = oLi.get(i).trim();
			if (s.length() > 0) {
				sizL = i;
			}
		}
		sizL++;
		List<String> listL = new ArrayList<String>();
		for (int j = 0; j < sizL; j++) {
			String s = oLi.get(j).toString();
			listL.add(s);
		}
		return listL;
	}

	// returne le contenu d'une ligne du fichier avec l'elemen 0 a blanc
	public List<String> get_LineFcsv_P(int nli) {
		List<String> sLi = new ArrayList<String>();
		sLi = L_LIGNES_CSV.get(nli);
		List<String> listL = new ArrayList<String>();
		listL.add("  ");
		for (int j = 0; j < sLi.size(); j++) {
			String s = sLi.get(j).toString();
			listL.add(s);
		}
		return listL;
	}

	public List<String> get_L_TITRES_TYP() {

		return L_TITRES_TYP;
	}

	public List<String> get_L_TITRES_CSV() {

		return L_TITRES_CSV;
	}

	// rajout de la colonne #
	public List<String> get_L_TITRES_CSV_P() {
		List<String> ls = L_TITRES_CSV;

		return ls;
	}

	public int get_LenFcsv() {
		return L_LIGNES_CSV.size();
	}

	public List<List<String>> get_ALL_LIGNES_CSV() {
		return L_LIGNES_CSV;
	}

	public int get_max_ColCsv() {
		return MAX_COL;
	}

	public int get_max_nbLigCsv() {
		return MAX_NB_LIGN;
	}

	public String get_FICHIER_CSV() {
		return FICHIER_CSV;
	}

	public String get_SEPARE_CSV() {

		return SEPARE;
	}

	public void set_FICHIER_CSV(String s) {
		FICHIER_CSV = s;
	}
	public void set_SEPAR_CSV(String s) {
		SEPARE = s;
	}
	public void set_MAX_COL(int i) {
		MAX_COL = i;
	}

	public int get_PAGE_CUR() {
		return PAGE_CUR;
	}

	public void set_PAGE_CUR(int s) {
		PAGE_CUR = s;
	}

	public int get_PAGE_MAX() {
		return PAGE_MAX;
	}

	public void set_PAGE_MAX(int s) {
		PAGE_MAX = s;
	}

	public int get_PAGE_NBli() {
		return PAGE_NBli;
	}

	public void set_PAGE_NBli(int s) {
		PAGE_NBli = s;
	}

	public void set_Enleve_Guillemets(boolean o) {
		Enleve_Guillemets = o;
	}

	public void set_SEPARE_CSV(String s) {
		SEPARE = s;
		if (SEPARE.contains("|"))
			SEPARE_Split="\\"+SEPARE;

	}

	public void set_CODAGE_File(String s) {
		CODAGE = s;
	}

	public void set_DIRE(String s) {
		DIRE = s;
	}

	public String get_DIRE() {
		return DIRE;
	}

	public List<String> get_ERREURS() {
		return ERREURS;
	}

	public void set_DIR_et_Nom_FICHIER(String s) {
		if (s.contains("\\"))
			s = s.replaceAll("\\\\", "/");
		int deb = s.lastIndexOf("/");
		DIRE = s.substring(0, deb + 1);
		FICHIER_CSV = s.substring(deb + 1);
	}

	public void reset_csv() {
		L_LIGNES_CSV = new ArrayList<List<String>>();
		L_TITRES_CSV = new ArrayList<String>();
		L_TITRES_TYP = new ArrayList<String>();
		ERREURS = new ArrayList<String>();
		MAX_COL = 0;
		MAX_NB_LIGN = 0;
		CODAGE = "";
		SEPARE = "";
		SEPARE_Split="";
	}

	/********************************************************************************************************
	 * MAIN
	 * 
	 * @throws IOException
	 * @throws ParseException
	 * 
	 ********************************************************************************************************/
	public static void main(String[] args) throws IOException, ParseException {
		// Instance
		CsvRead csv = new CsvRead();
        int i1=csv.chargeParHeure("/home/frso/01_DEV_K/01_GITLAB/cucumberMod1/tmpCsvDRUID.csv",3);

		csv.set_FICHIER_CSV("\\\\bddm01/2i/CASTO_REC/in/ALL CASES IN ONE/INP_ADRESSE.csv");
		int i = csv.chargeDataFromCsv();

		System.out.println("===FIN=== ");
	}
}
