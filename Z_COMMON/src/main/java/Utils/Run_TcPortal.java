package Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Run_TcPortal {

 //*********
    private String CONF_URL="https://aeenintas-helios-portal-r1.azurewebsites.net\")";
    private String CONF_TOPIC="http://dcvintlifeatl:8180/helios-fit-monitoring-front/#/transactions";

    private String EXCEL_FILE="TS01_PORTAL_REPORT_V02_TA.xlsx";
    private String EXCEL_FEUILLE  ="PORTAL_TESTS";

    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String GLOBAL_URL="02_LOG/";
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String DIR_TS_BAT="01_PORTAL_RUN/";  // dir bat
    private String DIR_TS_PARAM="01_PORTAL_RUN_PARAM/"; // dir param

    private String LAUNCH_ALL_NAME="00_LAUNCH_ALL";
    private ExcelRead EXC = new ExcelRead();
    private String      DIRE_PARAM_only="";

    private String      DIRE_LAUNCH="";

    private String TEST_NAME="SubmitBordereau";

    private HashMap<String,String> MAP_USER=new HashMap<>();
    private HashMap<String,String> MAP_PSW=new HashMap<>();
    private FileAndLog fLog = null;

    private String STR_SENDFILE_BORDEREAU="FILE_TO_SEND= BORDEREAU_005Kb_001.csv;BORDEREAU_005Kb_002.csv;BORDEREAU_005Kb_003.csv;BORDEREAU_005Kb_004.csv;BORDEREAU_005Kb_005.csv;BORDEREAU_005Kb_006.csv;BORDEREAU_005Kb_007.csv;BORDEREAU_005Kb_008.csv;BORDEREAU_005Kb_009.csv;BORDEREAU_005Kb_010.csv;BORDEREAU_005Kb_011.csv;BORDEREAU_005Kb_012.csv;BORDEREAU_005Kb_013.csv;BORDEREAU_005Kb_014.csv;BORDEREAU_005Kb_015.csv;BORDEREAU_005Kb_016.csv;BORDEREAU_005Kb_017.csv;BORDEREAU_005Kb_018.csv;BORDEREAU_005Kb_019.csv;BORDEREAU_005Kb_020.csv;BORDEREAU_005Kb_021.csv;BORDEREAU_005Kb_022.csv;BORDEREAU_005Kb_023.csv;BORDEREAU_005Kb_024.csv;BORDEREAU_005Kb_025.csv;BORDEREAU_005Kb_026.csv;BORDEREAU_005Kb_027.csv;BORDEREAU_005Kb_028.csv;BORDEREAU_005Kb_029.csv;BORDEREAU_005Kb_030.csv;BORDEREAU_005Kb_031.csv;BORDEREAU_005Kb_032.csv;BORDEREAU_005Kb_033.csv;BORDEREAU_005Kb_034.csv;BORDEREAU_005Kb_035.csv;BORDEREAU_005Kb_036.csv;BORDEREAU_005Kb_037.csv;BORDEREAU_005Kb_038.csv;BORDEREAU_005Kb_039.csv;BORDEREAU_005Kb_040.csv;BORDEREAU_005Kb_041.csv;BORDEREAU_005Kb_042.csv;BORDEREAU_005Kb_043.csv;BORDEREAU_005Kb_044.csv;BORDEREAU_005Kb_045.csv;BORDEREAU_005Kb_046.csv;BORDEREAU_005Kb_047.csv;BORDEREAU_005Kb_048.csv;BORDEREAU_005Kb_049.csv;BORDEREAU_005Kb_050.csv;BORDEREAU_005Kb_051.csv;BORDEREAU_005Kb_052.csv;BORDEREAU_005Kb_053.csv;BORDEREAU_005Kb_054.csv;BORDEREAU_005Kb_055.csv;BORDEREAU_005Kb_056.csv;BORDEREAU_005Kb_057.csv;BORDEREAU_005Kb_058.csv;BORDEREAU_005Kb_059.csv;BORDEREAU_005Kb_060.csv;BORDEREAU_005Kb_061.csv;BORDEREAU_005Kb_062.csv;BORDEREAU_005Kb_063.csv;BORDEREAU_005Kb_064.csv;BORDEREAU_005Kb_065.csv;BORDEREAU_005Kb_066.csv;BORDEREAU_005Kb_067.csv;BORDEREAU_005Kb_068.csv;BORDEREAU_005Kb_069.csv;BORDEREAU_005Kb_070.csv;BORDEREAU_005Kb_071.csv;BORDEREAU_005Kb_072.csv;BORDEREAU_005Kb_073.csv;BORDEREAU_005Kb_074.csv;BORDEREAU_005Kb_075.csv;BORDEREAU_005Kb_076.csv;BORDEREAU_005Kb_077.csv;BORDEREAU_005Kb_078.csv;BORDEREAU_005Kb_079.csv;BORDEREAU_005Kb_080.csv;BORDEREAU_005Kb_081.csv;BORDEREAU_005Kb_082.csv;BORDEREAU_005Kb_083.csv;BORDEREAU_005Kb_084.csv;BORDEREAU_005Kb_085.csv;BORDEREAU_005Kb_086.csv;BORDEREAU_005Kb_087.csv;BORDEREAU_005Kb_088.csv;BORDEREAU_005Kb_089.csv;BORDEREAU_005Kb_090.csv;BORDEREAU_005Kb_091.csv;BORDEREAU_005Kb_092.csv;BORDEREAU_005Kb_093.csv;BORDEREAU_005Kb_094.csv;BORDEREAU_005Kb_095.csv;BORDEREAU_005Kb_096.csv;BORDEREAU_005Kb_097.csv;BORDEREAU_005Kb_098.csv;BORDEREAU_005Kb_099.csv;BORDEREAU_005Kb_100.csv;BORDEREAU_030Mb_003.csv;BORDEREAU_030Mb_004.csv;BORDEREAU_090Mb_002.csv";
    private String STR_SENDFILE_OTHER_REQUESTS="\n" +
            "FILE_TO_SEND= OTHER_002Mb_001.xls ;OTHER_002Mb_002.xls ;OTHER_002Kb_001.ppt  ;OTHER_002Kb_002.ppt  ;OTHER_300Mb_001.txt ;OTHER_300Mb_002.txt ;OTHER_003Mb_001.pdf ;OTHER_003Mb_002.pdf;OTHER_001Mb_001.docx ;OTHER_001Mb_002.docx ;OTHER_002Mb_001.xlsx ;OTHER_002Mb_002.xlsx ;OTHER_001Kb_001. pptx ;OTHER_001Kb_002. pptx ;OTHER_001Mb_001.doc;OTHER_001Mb_002.doc";
    private String STR_SENDFILE_AMQP="FILE_TO_SEND= BORDEREAU_001Kb_001.csv;BORDEREAU_001Kb_002.csv ; BORDEREAU_001Kb_003.csv; BORDEREAU_001Kb_004.csv; BORDEREAU_001Kb_005.csv;BORDEREAU_001Kb_006.csv; BORDEREAU_001Kb_007.csv; BORDEREAU_001Kb_008.csv; BORDEREAU_001Kb_009.csv; BORDEREAU_001Kb_010.csv; BORDEREAU_001Kb_011.csv; BORDEREAU_001Kb_012.csv; BORDEREAU_001Kb_013.csv ; BORDEREAU_001Kb_014.csv; BORDEREAU_001Kb_015.csv; BORDEREAU_001Kb_016.csv; BORDEREAU_001Kb_017.csv ; BORDEREAU_001Kb_018.csv; BORDEREAU_001Kb_019.csv; BORDEREAU_001Kb_020.csv ; BORDEREAU_001Kb_021.csv; BORDEREAU_001Kb_022.csv; BORDEREAU_001Kb_023.csv; BORDEREAU_001Kb_024.csv;BORDEREAU_001Kb_025.csv; BORDEREAU_001Kb_026.csv; BORDEREAU_001Kb_027.csv; BORDEREAU_001Kb_028.csv ;BORDEREAU_001Kb_029.csv; BORDEREAU_001Kb_030.csv; BORDEREAU_001Kb_031.csv; BORDEREAU_001Kb_032.csv; BORDEREAU_001Kb_033.csv ; BORDEREAU_001Kb_034.csv ; BORDEREAU_001Kb_035.csv;BORDEREAU_001Kb_036.csv; BORDEREAU_001Kb_037.csv;BORDEREAU_001Kb_038.csv; BORDEREAU_001Kb_039.csv;BORDEREAU_001Kb_040.csv; BORDEREAU_001Kb_041.csv; BORDEREAU_001Kb_042.csv; BORDEREAU_001Kb_043.csv; BORDEREAU_001Kb_044.csv ;BORDEREAU_001Kb_045.csv; BORDEREAU_001Kb_046.csv; BORDEREAU_001Kb_047.csv; BORDEREAU_001Kb_048.csv; BORDEREAU_001Kb_049.csv ;BORDEREAU_001Kb_050.csv ;";
    private String STR_CEDENT_AMQP="CEDENT_NUMBER =5606";
    private String STR_SENDFILE_AMQN="FILE_TO_SEND=Corrupted_001.docx;Corrupted_001.xlsx;AMQx_Macro_P_.xlsm;02_ACTIV_MQ.7z;whoami.exe"



    ;

    //********************************************************
    //********************************************************

    public Run_TcPortal() throws IOException {
        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");

        if (isOperatingSystemWindows) {
            DIR_ROOT_TS="C:"+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=dire_home+DIR_ROOT_TS;
        }
        DIRE_PARAM_only=DIR_TS_PARAM;
        DIR_TS_PARAM=DIR_RUN+DIR_TS_PARAM;

        EXCEL_FILE=DIR_ROOT_TS+DIR_RUN+EXCEL_FILE;


        DIRE_LAUNCH=DIR_RUN+DIR_TS_BAT;

        fLog = new FileAndLog();

        ExcelRead EXC = new ExcelRead();
        //MAP_USER.put("ATH_SA","TaScorAdmin-1@outlook.com");
        MAP_USER.put("ATH_SA","rsolis-external@scor.com");
        MAP_USER.put("ATH_SS","TaScorSuper-1@outlook.com");
        MAP_USER.put("ATH_SB","TaScorBasic-1@outlook.com");
        MAP_USER.put("ATH_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("ATH_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("ATH_EP","TaExternalPartner-1@outlook.com");
        MAP_PSW.put("ATH_SA","aaaa");
        MAP_PSW.put("ATH_SS","1ScorSuper-1");
        MAP_PSW.put("ATH_SB","1ScorBasic-1");
        MAP_PSW.put("ATH_CS","1CedentSuper-1");
        MAP_PSW.put("ATH_CB","1CedentBasic-1");
        MAP_PSW.put("ATH_EP","1ExternalPartner-1");

        MAP_USER.put("SB_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("SB_CB","TaCedentBasic-1@outlook.com");
        MAP_PSW.put("SB_CS","1CedentSuper-1");
        MAP_PSW.put("SB_CB","1CedentBasic-1");

        MAP_USER.put("SOR_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("SOR_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("SOR_EP","TaExternalPartner-1@outlook.com");
        MAP_PSW.put("SOR_CS","1CedentSuper-1");
        MAP_PSW.put("SOR_CB","1CedentBasic-1");
        MAP_PSW.put("SOR_EP","1ExternalPartner-1");


        MAP_USER.put("BHL_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("BHL_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("BHL_EP","TaExternalPartner-1@outlook.com");
        MAP_PSW.put("BHL_CS","1CedentSuper-1");
        MAP_PSW.put("BHL_CB","1CedentBasic-1");
        MAP_PSW.put("BHL_EP","1ExternalPartner-1");

        MAP_USER.put("ORHL_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("ORHL_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("ORHL_EP","TaExternalPartner-1@outlook.com");
        MAP_PSW.put("ORHL_CS","1CedentSuper-1");
        MAP_PSW.put("ORHL_CB","1CedentBasic-1");
        MAP_PSW.put("ORHL_EP","1ExternalPartner-1");

        MAP_USER.put("EBHL_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("EBHL_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("EBHL_EP","TaExternalPartner-1@outlook.com");
        MAP_PSW.put("EBHL_CS","1CedentSuper-1");
        MAP_PSW.put("EBHL_CB","1CedentBasic-1");
        MAP_PSW.put("EBHL_EP","1ExternalPartner-1");

        MAP_USER.put("DFR_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("DFR_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("DFR_EP","TaExternalPartner-1@outlook.com");

        MAP_PSW.put("DFR_CS","1CedentSuper-1");
        MAP_PSW.put("DFR_CB","1CedentBasic-1");
        MAP_PSW.put("DFR_EP","1ExternalPartner-1");


        MAP_USER.put("EMP_SA","TaScorAdmin-1@outlook.com");
        MAP_USER.put("EMPA_SS","TaScorSuper-1@outlook.com");
        MAP_PSW.put("EMP_SA","1ScorAdmin-1");
        MAP_PSW.put("EMPA_SS","1ScorSuper-1");


        MAP_USER.put("AMQP_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("AMQN_CS","TaCedentSuper-1@outlook.com");
        MAP_PSW.put("AMQP_CS","1CedentSuper-1");
        MAP_PSW.put("AMQN_CS","1CedentSuper-1");


        MAP_USER.put("EORHL_CS","TaCedentSuper-1@outlook.com");
        MAP_USER.put("EORHL_CB","TaCedentBasic-1@outlook.com");
        MAP_USER.put("EORHL_EP","TaExternalPartner-1@outlook.com");
        MAP_PSW.put("EORHL_CS","1CedentSuper-1");
        MAP_PSW.put("EORHL_CB","1CedentBasic-1");
        MAP_PSW.put("EORHL_EP","1ExternalPartner-1");

    }


    //*****************************************************************
    public void trtAllLines() throws IOException {

        EXC.set_File_Feuille(EXCEL_FILE, EXCEL_FEUILLE);
        //EXC.openExcelFile();
        EXC.readAllExcel();
        int nb = EXC.get_nbLign();
        int lecLign = 4;
        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("win")) {
            LAUNCH_ALL_NAME = LAUNCH_ALL_NAME + ".bat";
            creerLanceToutWin();
        } else {
            LAUNCH_ALL_NAME = LAUNCH_ALL_NAME + ".sh";
            creerLanceToutLin();
        }
        while (EXC.get_valCol(lecLign, 0).length() > 1 && lecLign < nb) {
            String sPack = EXC.get_valCol(lecLign, 3);
            if (sPack.contentEquals("PORTAL"))
                trtLigneExel(lecLign);
            lecLign++;
        }
        EXC.saveXLSX_file();
    }

    public void trtLigneExel(int lecLign) throws IOException {
            List<String> fileT=new ArrayList<>();
            String NomTest=EXC.get_valCol(lecLign,0);
            String NomTestBat=EXC.get_valCol(lecLign,1).trim();
            NomTestBat=NomTestBat.replace(" ","_");
            NomTestBat=(lecLign+1)+"__"+NomTestBat+"_"+NomTest;
            NomTest=(lecLign+1)+"__"+NomTest;

            String param[]=EXC.get_valCol(lecLign,2).split("\\n");

            fileT.add("CNX_LOGIN_PORTAL   = "+CONF_URL);
            fileT.add("CNX_BROWSER      = FIREFOX");
            String tmp1[]=NomTest.split("-");
            String taUser=MAP_USER.get(tmp1[2]);
            String taPsw =MAP_PSW.get(tmp1[2]);
            fileT.add("CNX_TA_USER      = "+taUser);
            fileT.add("CNX_TA_PASSWORD  = "+taPsw);
            String typTest[]=tmp1[2].split("_");
            switch ( typTest[0]){
                case "ATH": //ACCESS TO HISTORY
                    break;
                case "SB":  // SUBMIT BORDEREAU
                    fileT.add("SENDING_TYPE = VARIABLE_9");
                    fileT.add("TYPE_BORDEREAU = "+"Claims");
                    fileT.add("DIR_FILE_TO_SEND=  00_BORDEREAUX/");
                    fileT.add("DATE_BORDEREAU = 04/2013-04/2013");
                    fileT.add(STR_SENDFILE_BORDEREAU);
                    break;
                case "SOR":  // SUBMIT OTHER REQUESTS
                    fileT.add("SENDING_TYPE = VARIABLE");
                    fileT.add("DIR_FILE_TO_SEND=  01_OTHER_REQUESTS/");
                    fileT.add(STR_SENDFILE_OTHER_REQUESTS);
                    break;
                case "BHL":  // BORDEREAU HISTORY LIST
                    break;
                case "ORHL":  // OTHER REQUESTS HISTORY LIST
                    break;
                case "EBHL":  //EXPORT BORDEREAU HISTORY LIST
                    break;

                case "DFR":  // DISPLAY FILTER RESULTS
                    if (param.length>1 && param.length<5){
                        String sPar1=param[1].trim();
                        fileT.add(sPar1);
                        fileT.add(param[2].trim());
                        fileT.add(param[3].trim());
                    }
                    break;
                case "EMP": // ERROR MANAGEMENTS PAGE
                    break;
                case "AMQP": // ACTIVEMQ JMS CHECK
                    fileT.add("SENDING_TYPE = VARIABLE_1");
                    fileT.add("TYPE_BORDEREAU = "+"Claims");
                    fileT.add("DIR_FILE_TO_SEND=  00_BORDEREAUX/");
                    fileT.add("DATE_BORDEREAU = 01/2010-02/2010");
                    fileT.add(STR_CEDENT_AMQP);
                    fileT.add(STR_SENDFILE_AMQP);
                    fileT.add("URL_TOPIC="+CONF_TOPIC);
                    break;
                case "AMQN": // ACTIVEMQ NEGATIVE CHECK
                    fileT.add("SENDING_TYPE = FIXE");
                    fileT.add("TYPE_BORDEREAU = "+"Claims");
                    fileT.add("DIR_FILE_TO_SEND=  02_ACTIV_MQ/");
                    fileT.add("DATE_BORDEREAU = 01/2011-02/2011");
                    fileT.add(STR_CEDENT_AMQP);
                    fileT.add(STR_SENDFILE_AMQN);
                    fileT.add("URL_TOPIC="+CONF_TOPIC);
                    break;
                default:
                    System.out.println("nom tes = "+NomTest);
                    break;
            }
            System.out.println(DIR_TS_PARAM+NomTest);
           if (!NomTest.contains("#")){
               EXC.ecrit_Lign_StringCell(lecLign+1,5,"NT");
               fLog.del_File(DIR_ROOT_TS+DIR_TS_PARAM+NomTest+".txt");
               fLog.Add_Lines(fileT,DIR_ROOT_TS+DIR_TS_PARAM+NomTest+".txt");
               String os = System.getProperty("os.name").toLowerCase();
                if(os.contains("win")){
                    fLog.del_File(    DIR_ROOT_TS+DIRE_LAUNCH+NomTestBat+".bat");
                    CreaFileBatTcList(DIR_ROOT_TS+DIRE_LAUNCH+NomTestBat+".bat",NomTest+".txt",typTest[0],DIRE_PARAM_only);
                    System.out.println("file BAT = "+NomTestBat+".bat");
                    ajoutLanceToutWin(NomTest+".txt",typTest[0],DIRE_PARAM_only);

                }else{
                    fLog.del_File(  DIR_ROOT_TS+DIRE_LAUNCH+NomTestBat+".sh");
                    creerShellLinux(DIR_ROOT_TS+DIRE_LAUNCH+NomTestBat+".sh",NomTest+".txt",typTest[0],DIRE_PARAM_only);
                    System.out.println("file Sh = "+NomTestBat+".sh");
                    ajoutLanceToutLin(NomTest+".txt",typTest[0],DIRE_PARAM_only);
                }
            }
    }

 //************************************************************
    public void CreaFileBatTcList(String fileBat, String fileParam, String ty,String dirParam) throws IOException {
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);

        fileT.add("ECHO OFF");
        fileT.add("Title \"Test "+fileBat+"\"");
        //System.out.println(" Ouvre ="+fichierParam);
        fileT.add("REM #Launch test : "+ty);
        fileT.add("REM ");
        fileT.add("java -jar ..\\Ta_Portal_TS1.jar "+ty+" "+dirParam+fileParam );

        fLog.Add_Lines(fileT,fileBat);
    }



    public void creerShellLinux(String fileBat, String fileParam,String ty,String dirParam) throws IOException {
        // vérifier si la dir existe
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);

        fileT.add("#!/bin/bash");
        fileT.add("# Launch test ");
        fileT.add("# ");
        fileT.add("# ");
        fileT.add("java -jar ../Ta_Portal_TS1.jar "+ty+" "+dirParam+fileParam );
        fLog.Add_Lines(fileT,fileBat);

    }
    //**************************************
    public void creerLanceToutWin() throws IOException{
        // vérifier si la dir existe

        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH + LAUNCH_ALL_NAME ;
        System.out.println("bat="+fileBat);
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);
        fileT.add("ECHO OFF");
        fLog.Add_Lines(fileT,fileBat);
    }

    public void ajoutLanceToutWin( String fileParam, String ty,String dirParam) throws IOException {


        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH+LAUNCH_ALL_NAME;
        System.out.println(" AJOUT =" + ty);
        List<String> fileT = new ArrayList<>();
        fileT.add("echo \"=======\"");

        fileT.add("java -jar ../Ta_Portal_TS1.jar "+ty+" "+dirParam+fileParam );
        fileT.add("\ntimeout 5 > NUL");

        fileT.add("echo \"***********************************************************************\" ");
        fLog.Add_Lines(fileT,fileBat);
    }
    public void creerLanceToutLin() throws IOException{
        // vérifier si la dir existe

        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH+LAUNCH_ALL_NAME;
        System.out.println("sh="+fileBat);
        List<String> fileT=new ArrayList<>();
        fLog.del_File(fileBat);
        fileT.add("#!/bin/bash");
        fileT.add("#");
        fileT.add("#");
        fLog.Add_Lines(fileT,fileBat);
    }

    public void ajoutLanceToutLin( String fileParam, String ty,String dirParam) throws IOException {


        String fileBat = DIR_ROOT_TS+DIRE_LAUNCH + LAUNCH_ALL_NAME ;
        System.out.println(" AJOUT =" + ty);
        List<String> fileT = new ArrayList<>();
        fileT.add("echo \"=======\"");

        fileT.add("java -jar ../Ta_Portal_TS1.jar "+ty+" "+dirParam+fileParam );
        fileT.add("echo sleep 10");

        fileT.add("echo \"***********************************************************************\" ");
        fLog.Add_Lines(fileT,fileBat);
    }
    /********************************************************************************************************
     * MAIN
     * @throws IOException
     *
     ********************************************************************************************************/
    public static void main(String[] args) throws IOException {
        // Instance
        Run_TcPortal re =new Run_TcPortal();
        re.trtAllLines();
    }
}

