package Utils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelRead {

    private FileInputStream FIS;
    private XSSFWorkbook BOOK = null;
    private XSSFCell CELL;
    private XSSFRow ROW;
    private XSSFSheet P_HOJA1;
    private XSSFSheet       P_HOJA2;

    private String          FIC_param    = "C:/01_DEV/02_Casto_CSV_CAL/GenCsvFromExcel/Gen_monosource1.txt";
    private String          FICHIER_EXC      = "C:\\01_DEV_SCOR\\04_TS1_Portal\\00_LaunchTest\\TS01_PortalRapport.xlsx";
    private String          EXC_FEUILLE  ="TC_DataSet";
    private String          EXC_COL      ="";
    private String          EXC_GUILLEMET ="";
    private String          TA_separe =";";
    private String          TA_saveFic ="";

    private List<List<String>> L_LIGNES_EXC = new ArrayList<>();
    private int MAX_COL = 0;
    private int MAX_NB_LIGN = 0;

    /********************************************************************************************************
     * CONSTRUCTEUR
     * Author Roberto SOLISConstructeur du test
     * Reçoit en parametre le nom du navigateur
     *  FIREFOX, CHROME ou IExplorer
     * @throws IOException
     * @throws Exception
     ******************************************************************************************************/
    public ExcelRead() throws IOException {


        System.setProperty("file.encoding", "UTF-8");
        //Rechercher le le nom du fichier excel

    }
    public int  readAllExcel() {
        System.out.println("FICHIER_EXCEL ="+FICHIER_EXC);
        System.out.println("FEUILLE ="+EXC_FEUILLE);
        System.out.println("COLONNE_A ="+EXC_COL);
        System.out.println("DONNES_AVEC_GUILLEMETS ="+EXC_GUILLEMET);
        System.out.println("SEPARATEUR ="+TA_separe) ;
        System.out.println("Fichier a generer ="+TA_saveFic) ;
        //lecParam(FIC_param );
        int ret=0;

        try {
            openExcelFile();
            openFeuille(EXC_FEUILLE);
            List<String> list_l=new  ArrayList<String>();
            boolean fin=false;
            int lign=0;
            while (!fin){

                list_l=lecture_Lign(lign);


                if ((list_l==null))
                    fin=true;
                else if ((list_l.size()<1)){
                    fin=true;
                }else{
                    System.out.println(list_l);
                    L_LIGNES_EXC.add(list_l);
                }
                lign++;
            }
            List<String> p_l=new ArrayList<String>();
            MAX_NB_LIGN=lign;

        }catch (Exception e) {
            return -1;
        }
        ret=MAX_NB_LIGN;
        return ret;

    }


    public void openExcelFile(){
        System.out.println("\ndire = "+FICHIER_EXC);



        try {
            FIS=new FileInputStream(new File(FICHIER_EXC));

            BOOK = new XSSFWorkbook(FIS);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out.println("Erreur ouverture= "+FICHIER_EXC);
            e.printStackTrace();
        }

    }

    public String  openFeuille(String sheet1){
        try {
            P_HOJA1 = BOOK.getSheet(sheet1);
        }
        catch  (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

        return "OK";
    }
    public void saveXLSX_file() throws IOException {
        // Write the output to the file
        FileOutputStream fileOut = new FileOutputStream(FICHIER_EXC);


        BOOK.write(fileOut);

        fileOut.close();

        // Closing the workbook
        BOOK.close();
    }

    //******************  li_LignCol_Sheet1
    public List<String> lecture_Lign(int lign){
        // Attention pour EXCEL N°ligne/Col commence à 0
        // La ligne affiche 1 est la ligne 0 lue par java
        XSSFCell poNum;
        List<String> list_l=new  ArrayList<String>();
        try {
            ROW=P_HOJA1.getRow(lign);
            if (ROW==null)return null;
            for (Cell poNum1 : ROW ){
                String c=cellToString(poNum1);
                list_l.add(c);
            }
        }
        catch  (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return list_l;
    }
    public void ecrit_Lign_StringCell(int lign,int col,String txt){
        // ligne col doit exister
        // Attention pour EXCEL N°ligne/Col commence à 0
        // La ligne affiche 1 est la ligne 0 lue par java
        XSSFCell poNum;

        try {
            ROW=P_HOJA1.getRow(lign-1);
            CELL=ROW.getCell(col-1);
            if (CELL==null)
                CELL=ROW.createCell(col-1);
            CELL.setCellType(CellType.STRING);
            CELL.setCellValue(txt);
        }
        catch  (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    //******************************
    public static String cellToString(Cell cell) {
        CellType type;
        Object result=null;
        type = cell.getCellType();

        switch (type) {

            case NUMERIC: // numeric value in Excel
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    Date da= cell.getDateCellValue();
                    //System.out.println ("Row No.: " + cell.getDateCellValue());
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    result =  sdf.format(cell.getDateCellValue());
                    //System.out.println(result);
                }else{
                    Double du = cell.getNumericCellValue();
                    String s= String.format("%.0f", du);
                    result=s;
                }
                break;
            case FORMULA: // precomputed value based on formula
                Double du = cell.getNumericCellValue();
                String s= String.format("%.0f", du);
                result=s;
                break;
            case STRING: // String Value in Excel
                result = cell.getStringCellValue();
                break;
            case BLANK:
                result = "";
            case BOOLEAN: //boolean value
                result: cell.getBooleanCellValue();
                break;
            case ERROR:
            default:
                throw new RuntimeException("There is no support for this type of cell");
        }

        return result.toString();
    }
    public void set_File_Feuille(String s,String f) {
        FICHIER_EXC = s;
        EXC_FEUILLE=f;
    }


    // returne le contenu d'une ligne du fichier 1
    public List<String> get_LineExc(int nli) {
        List<String> sLi = new ArrayList<String>();
        sLi = L_LIGNES_EXC.get(nli);

        return sLi;
    }
    public String get_valCol(int nli,int col) {
        List<String> sLi = new ArrayList<String>();
        sLi = L_LIGNES_EXC.get(nli);
        sLi.get(col);
        return sLi.get(col);
    }
    public int get_nbLign() {
        return MAX_NB_LIGN;
    }
    /********************************************************************************************************
     * MAIN
     * @throws IOException
     *
     ********************************************************************************************************/
    public static void main(String[] args) throws IOException {
        // Instance
        ExcelRead re =new ExcelRead();
        re.readAllExcel();
    }
}
