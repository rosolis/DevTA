package Utils;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import org.apache.commons.io.FileUtils;

public class FileAndLog {

    public boolean OUT_LOGS=false;

    public FileAndLog(){
        ;
    }
    public void setLogs(boolean b){
        OUT_LOGS=b;
    }

    //**************************************************************
    public void Add_Log(String st, String fichier) {
        if (!OUT_LOGS) return;
        Add_String(st,fichier);
    }
    public void Add_String(String st, String fichier) {
        List<String> s = new ArrayList<String>();
        s.add(st);
        Add_Lines(s, fichier);
    }

    public void Add_Lines(List<String> ligne, String fichier) {

        System.setProperty("file.encoding", "UTF-8");
        Charset charset = Charset.forName("UTF8");
        File fg = new File(fichier);
        Path tracePath = Paths.get(fichier);
        try {
            if (fg.exists()) {
                Files.write(tracePath, ligne, charset, StandardOpenOption.APPEND);
            } else
                Files.write(tracePath, ligne, charset, StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void del_File(String fichier) {
        // del fichier
        File fg = new File(fichier);
        if (fg.exists()) {
            fg.delete();
        }
    }
    public void rename_File(String source,String dest) throws IOException {
    	File sr = new File(source); 
    	File copied = new File(dest);
    	boolean success = sr.renameTo(copied);
    	}
    
    public void copy_File(String source,String dest) throws IOException {
    	File sr = new File(source); 
    	File copied = new File(dest);
    	FileUtils.copyFile(sr, copied);
    	}

    //**************************************************************
    public void writeStatistic(String fl,LinkedHashMap<String,Object> tl){
        File fg = new File(fl);
        List<String> keys = new ArrayList<String>(tl.keySet());
        List<String> print_s  = new ArrayList<String>();;
        String stmp="";
        if (!fg.exists()) {
            for (int i=0;i< keys.size();i++){
                if (i>0)stmp +=";";
                stmp +=keys.get(i);
            }
            print_s.add(stmp);
        }
        stmp="";
        for (int i=0;i< keys.size();i++){
            if (i>0)stmp +=";";
            stmp +=tl.get(keys.get(i));
        }
        print_s.add(stmp);

        Add_Lines(print_s,fl);
    }
    /*************************************************
     * Génére date-heure au format X = 010100hhmm avec les 4 dernier char qui ont l'heure et minutes
     *
     *************************************************/
    public String logDate(int n) {
        // Génére string date au format jjmmaaaa
        TimeZone timeZone =TimeZone.getTimeZone("UTC");
        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        if (n>100){
            localCalendar = Calendar.getInstance(timeZone);
            n=n-100;
        }

        int tDay   = localCalendar.get(Calendar.DATE);
        int tMonth = localCalendar.get(Calendar.MONTH) + 1;
        int tYear  = localCalendar.get(Calendar.YEAR);
        Date tTime = localCalendar.getTime();
        int tHeure = localCalendar.get(Calendar.HOUR_OF_DAY);
        int tMin   = localCalendar.get(Calendar.MINUTE);
        int tsec1  = localCalendar.get(Calendar.SECOND);
        int tsec2  = localCalendar.get(Calendar.MILLISECOND);

        if (n == 1) { // YYYY-MM-DDThh:mm:ss-SS
            return String.format("%s-%02d-%02d %02d-%02d-%02d-%d", tYear, tMonth, tDay, tHeure, tMin, tsec1, tsec2);
        }
        if (n == 2) { // YYYY-MM-DDThh:mm:ss
            return String.format("%s-%02d-%02dT%02d-%02d-%02d", tYear, tMonth, tDay, tHeure, tMin, tsec1);
        }
        if (n == 3) { // YYYY-MM-DD
            return String.format("%s-%02d-%02d", tYear, tMonth, tDay);
        }
        if (n == 4) { // mm-dd-hh-mm-ss
            return String.format("%02d-%02d-%02d-%d", tMonth, tDay, tHeure, tMin, tsec1);
        }

        if (n == 5) { // YYYYMMDDhhmmss
            return String.format("%s%02d%02d%02d%02d%02d", tYear, tMonth, tDay, tHeure, tMin, tsec1);
        }

        if (n == 6) { // YYYYMMDDhhmm
            return String.format("%s%02d%02d%02d%02d", tYear, tMonth, tDay, tHeure, tMin);
        }
        if (n == 7) { // YYYYMMDD
            return String.format("%s%02d%02d", tYear, tMonth, tDay);
        }
        if (n == 8) { // DDhhmmss
            return String.format("%02%02d%02d%02d%02d", tMonth,tDay, tHeure, tMin, tsec1);
        }
        if (n == 9) { // AAAAMMJJ
            return String.format("%s%02d%02d", tYear, tMonth, tDay);
        }
        if (n == 10) { // JJ/MM/AAAA hh:mm:ss-SS
            return String.format("%s/%02d/%02d %02d:%02d:%02d-%d", tDay,tMonth,tYear,tHeure, tMin, tsec1, tsec2);
        }
        if (n == 11) { // JJ/MM/AAAA hh:mm:ss
            return String.format("%s/%02d/%02d %02d:%02d:%02d", tDay,tMonth,tYear,tHeure, tMin, tsec1, tsec2);
        }

        return String.format("%s-%02d-%02d %02d-%02d-%02d-%d", tYear, tMonth, tDay, tHeure, tMin, tsec1, tsec2);

    }
}
