package AccesToHeliosMonitoring;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import Helper.SeleniumHelp;
import com.google.gson.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.internal.filter.ValueNode;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;

import Helper.SeleniumUtil;




public class PageAccesToHeliosMonitoring {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private String FIC_LOGS = "";

    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = new SeleniumHelp();
    private List<String> LOG_TEST = new ArrayList<>();
    private List<WebElement> TR_ROWS_HML = new ArrayList<>();
    private int NB_CLICK_POP=0;
    /***********************************************************************
     *
     * @param driver
     * @return
     */

    public PageAccesToHeliosMonitoring(WebDriver driver, SeleniumUtil su, SeleniumHelp sh, WebDriverWait wait, List<String> d) throws IOException {
        this.driver = driver;
        this.WAIT = wait;
        sUtil=su;
        sHelp=sh;
        LOG_TEST = d;
        PageFactory.initElements(this.driver, this);
    }

    public WebDriver GetDriver() {

        return this.driver;
    }

    //*************************************************************************
    @FindBy(xpath = "/html/body/app-root/main/app-transactions-page/app-transactions-table/div[3]/table/tbody/tr[1]/td[1]")
    private WebElement ClickTransact;
    @FindBy(xpath = "//*[@id=\"mat-input-0\"]")
    private WebElement TransIDField;
    @FindBy(xpath = "//*[@id=\"mat-select-0\"]/div/div[1]/span/span")
    private WebElement MenuRefreshNo;
    @FindBy(xpath = "//*[@id=\"mat-option-2\"]/span")
    private WebElement RefreshNo;

    public void setTransID(String Id) throws Exception {
        LOG_TEST.add("Find Transaction Id =" + Id );
        MenuRefreshNo.click();
        sUtil.tempo(4);
        RefreshNo.click();
        sUtil.tempo(4);

        TransIDField.sendKeys(Id);
        sUtil.tempo(4);
        Long l=0L;
        ButtonRefresh.click();
        l=sHelp.waitEleLaEtAffiche(ClickTransact,10,2);
        if (l==0)
            ButtonRefresh.click();
            l=sHelp.waitEleLaEtAffiche(ClickTransact,10,2);
        //sUtil.tempo(3);
        ClickTransact.click();
        sUtil.tempo(3);
    }

    @FindBy(xpath = "/html/body/app-root/main/app-transactions-page/app-transactions-table/div[1]/div/button[2]")
    private WebElement ButtonRefresh;
    @FindBy(xpath = "/html/body/app-root/main/app-transactions-page/app-transactions-table/div[3]/table/tbody/tr[2]/td/div/app-message-events-table/table/tbody")
    private WebElement TableMonitBody;
    @FindBy(xpath = "//*[@id=\"mat-dialog-0\"]/app-message-event-dialog/mat-dialog-content/code/pre")
    private WebElement JsonPOP_009;
    public String waitingEvent(String Id) throws Exception {
        LOG_TEST.add("Wait event = " + Id );
        String sRes = "";
        int max = 20;
        boolean trouve = false;
        while (max > 0) {
            ButtonRefresh.click();
            sUtil.tempo(3);
            ClickTransact.click();
            sUtil.tempo(3);
            TR_ROWS_HML = TableMonitBody.findElements(By.cssSelector("tr"));
            int iCount = TR_ROWS_HML.size();

            boolean rows = false;
            int iGet = 0;
            while (!rows) {
                TR_ROWS_HML = TableMonitBody.findElements(By.cssSelector("tr"));
                WebElement wLi = TR_ROWS_HML.get(iGet);
                List<WebElement> EVcount = wLi.findElements(By.tagName("td"));
                WebElement we1 = EVcount.get(0);
                WebElement we2 = EVcount.get(1);
                WebElement we3 = EVcount.get(2);
                String sEve = we3.getText();
                if (sEve.contains("HML_015"))
                    System.out.println(sEve);
                System.out.println(sEve);
                if (sEve.contains(Id)) {
                    trouve = true;
                    rows = true;
                    we3.click();
                    //String S=JsonPOP_009.getText();
                    String sNb=Integer.toString(NB_CLICK_POP);
                    String findEle="//*[@id=\"mat-dialog-"+sNb+"\"]";
                    WebElement we4  = driver.findElement(By.xpath(findEle));

                    sRes=we4.getText();
                    we4.findElement(By.xpath("app-message-event-dialog/mat-dialog-actions/button/span")).click();
                    NB_CLICK_POP++;
                    System.out.println(sEve);
                }
                iGet++;
                if (iGet >= iCount) {
                    rows = true;

                }
            }
            if (trouve)
                max = 0;
            else {
                sUtil.tempo(6);
                max--;
            }
        }
        if (trouve)
            LOG_TEST.add("Event = " + Id +": TROUVE");
        else {
            LOG_TEST.add("Event = " + Id +" : NON TROUVE");
        }
        return sRes;
    }
    // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
    // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT

    public String okDataHLM_015NoNull(String sHml15, Map<String, Object> d_upload){
        // d_upload =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT

        String sOk="";
        int iDeb=sHml15.indexOf("{");
        int iFin=sHml15.lastIndexOf("}");
        String sJson=sHml15.substring(iDeb,iFin+1);

        //jsonPath

        Object document = Configuration.defaultConfiguration().jsonProvider().parse(sJson);

        String fl = JsonPath.read(document, "$.fileInformations[0].unitaryFileInformations[0].filename");
        String funcId = JsonPath.read(document, "$.fileInformations[0].unitaryFileInformations[0].functionalId");
        String indexId = JsonPath.read(document, "$.fileInformations[0].unitaryFileInformations[0].indexationId");
        if (funcId.contains("null"))
            sOk=fl+" : functionalId HAVE NULL";

        if (indexId.contains("null"))
            sOk=fl+" : indexationId HAVE NULL";
        return sOk;
    }
    public String okDataHLM_009(String sHm009, Map<String, Object> d_upload) throws ParseException {
        // d_upload =GR_BORD ORIGIN_BORD NB_BORD PERIOD_BORD SENDER_BORD TRANS_ID TYPE_BORD   SENDING_DATE USER_BORD
        //   FILES_LOADED MILLI_TOT
        String sOri = (String) d_upload.get("GR_BORD") + ";";
        sOri += (String) d_upload.get("ORIGIN_BORD") + ";";
        sOri += (String) d_upload.get("NB_BORD") + ";";
        String sTmp = formatPeriodeWith_0((String) d_upload.get("PERIOD_BORD"));
        sOri+=sTmp+";";
        sOri += (String) d_upload.get("SENDER_BORD") + ";";
        sTmp = (String) d_upload.get("SENDING_DATE");
        int iDeb=sTmp.lastIndexOf("(");
        int iFin=sTmp.lastIndexOf(")");

        String ssTmp = sTmp.substring(0, iDeb) + sTmp.substring(iDeb+1, iFin);

        sTmp=formatDateWith_AMgmt(ssTmp,"dd/MM/yyyy hh:mm");

        sOri += sTmp;

        String sOk = "";
        iDeb = sHm009.indexOf("[");
        iFin = sHm009.lastIndexOf("]");
        String sJson = sHm009.substring(iDeb, iFin + 1);
        //sJson="{\"tmp\": "+sJson +"}";
        //jsonPath
        String sDest = "";
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(sJson);

        sTmp = JsonPath.read(document, "$.[12].value"); //GroupName
        sDest = sTmp + ";";
        sTmp = JsonPath.read(document, "$.[10].value"); //OriginOfTransfer
        sDest += sTmp + ";";
        sTmp = JsonPath.read(document, "$.[3].value"); //NumUploadedDocument
        sDest += sTmp + ";";
        ssTmp = JsonPath.read(document, "$.[6].value") + " - "; //FromPeriod
        ssTmp += JsonPath.read(document, "$.[7].value"); //ToPeriod
        sTmp = formatPeriodeWith_0(ssTmp);

        sDest += sTmp + ";";
        sTmp = JsonPath.read(document, "$.[11].value"); //SentBy
        sDest += sTmp + ";";
        ssTmp = JsonPath.read(document, "$.[13].value"); //CreationDate
        //6/17/2019 12:13:11 PM
        sTmp=formatDateWith_AMgmt(ssTmp,"MM/dd/yyyy hh:mm:ss");

        sDest += sTmp;

        if (!sOri.contains(sDest))
            sOk = sDest;

        return sOk;
    }

    public String okData_De001_CheckSubFiles(String txtEvent, Map<String, Object> d_upload){
        // d_upload =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT

        String sOk="";
        int iDeb=txtEvent.indexOf("{");
        int iFin=txtEvent.lastIndexOf("}");
        String sJson=txtEvent.substring(iDeb,iFin+1);

        //jsonPath

        //Object document = Configuration.defaultConfiguration().jsonProvider().parse(sJson);

        List<Map<String, Object>> fileInfo = JsonPath.read(sJson, "$.fileInformations");

        for (Map<String, Object> chaqueFi:fileInfo){

            List<Map<String, Object>> unitFileInfo = (List<Map<String, Object>>)chaqueFi.get("unitaryFileInformations");
            for (Map<String, Object> chaqueUnitFileInfo:unitFileInfo){
                String flUfi   = (String)chaqueUnitFileInfo.get("filename");

                List<Map<String, Object>> subfl = (List<Map<String, Object>>)chaqueUnitFileInfo.get("subfiles");
                for (Map<String, Object> lisSubfiles:subfl){
                    String subFna   = (String)lisSubfiles.get("subfileName");
                    String subfId   = (String)lisSubfiles.get("subfileId");
                    if (subFna.contains("null"))
                        sOk=subFna+" : subfileName HAVE NULL";

                    if (subfId.contains("null"))
                        sOk=subfId+" : subfileId HAVE NULL";
                }
            }
            System.out.println(sOk);
        }

        return sOk;

    }
    public String formatPeriodeWith_0(String per){
        String sRet ="";
        // 1/2017 - 1/2017
        String sp1[]=per.split("-");
        String sp2[]=sp1[0].split("/");
        String sp3[]=sp1[1].split("/");

        int i= Integer.parseInt(sp2[0].trim());
        sp2[0]=String.format("%02d", i);
        sp2[1]=sp2[1].trim();
        sp3[1]=sp3[1].trim();
        i= Integer.parseInt(sp3[0].trim());
        sp3[0]=String.format("%02d", i);
        return sp2[0]+"/"+sp2[1]+" - "+sp3[0]+"/"+sp3[1];
    }
    public String formatDateWith_AMgmt(String dat,String form) throws ParseException {
        //String entree = "MM/dd/yyyy HH:mm:ss";
        String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm";
        SimpleDateFormat dFormat = new SimpleDateFormat(form);
        dFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Date d1 = dFormat.parse(dat);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_NOW);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDate = dateFormat.format(d1).toString();
        System.out.println(formattedDate);
        return formattedDate;
    }
}
