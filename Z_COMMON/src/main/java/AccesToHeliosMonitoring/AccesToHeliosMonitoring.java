package AccesToHeliosMonitoring;


import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class AccesToHeliosMonitoring {



    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;

    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;

    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS

    private String FILE_PARAM="OutlookPar.txt";
    private int NUM_LIN_TEST=0;
    private boolean logsTrue = true;
    private FileAndLog sFlog = null;

    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String DIR_TS_PARAM=DIR_RUN;
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String GLOBAL_URL="02_LOG/";
    private String BROWSER_FROM_SYS="NO";
    private String BROWSER_FOR_ALL_TEST="";

    Properties SYS_CONFIG = new Properties();
    private String CONF_URL="http://dcvintlifeatl:8180/helios-fit-monitoring-front/#/transactions";
    private String ADRESSE_MAIL="";
    private String ADRESSE_PASW="";
    private String LIST_ACTIONS[]=null;
    private List<String> LOG_TEST   =new ArrayList<>();

    private String TRANSACT_ID="";
    private String SUBJECT_GR="Bordereaux submission confirmation for ";
    private String HANDLE_0="";
    private String HANDLE_1="";

    // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
    // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
    private Map<String, Object>  DATA_UPLOADED = new HashMap<>();

    //********************************************************
    //********************************************************


    public AccesToHeliosMonitoring(List<String> lo ) throws Exception {
        LOG_TEST=lo;
        AccesToHeliosMonitoringINIT();
    }

    public AccesToHeliosMonitoring( ) throws Exception {
        AccesToHeliosMonitoringINIT();
    }

    public void AccesToHeliosMonitoringINIT() throws IOException {

        String dire_lancement = System.getProperty("user.dir");
        System.out.println("dire_lancement:" + dire_lancement);

        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        int deb = dire_lancement.lastIndexOf("0A_TS1_Portal");
        String sp=dire_lancement.substring(0,deb-1);
        if (isOperatingSystemWindows) {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        }
        System.out.println("Start= "+DIR_ROOT_TS);

        sFlog = new FileAndLog();
        sFlog.setLogs(logsTrue);
        sUtil= new SeleniumUtil();

        InputStream inputRoot = new FileInputStream(DIR_ROOT_TS+"00_SYS_CONFIG.txt");

        SYS_CONFIG.load(inputRoot);
        DIR_REPORT = SYS_CONFIG.getProperty("DIR_REPORTS");

        DIR_JDD=SYS_CONFIG.getProperty("DIR_JDD");
        DIR_LOGS=SYS_CONFIG.getProperty("DIR_LOGS");
        DIR_DRIVER=SYS_CONFIG.getProperty("DIR_DRIVER");
        DIR_TS_PARAM=SYS_CONFIG.getProperty("DIR_TS_INTEGRATION");
        GLOBAL_URL=SYS_CONFIG.getProperty("SYS_CNX_LOGIN_PORTAL");
        BROWSER_FOR_ALL_TEST=SYS_CONFIG.getProperty("SYS_CNX_BROWSER");
        CONF_BROWSER=BROWSER_FOR_ALL_TEST.trim();
        DIR_RUN=DIR_ROOT_TS+DIR_RUN;

        String sF[]=FILE_PARAM.split("\\.");
        String sF2=sF[0].replace("\\","-");
        sF2=sF2.replace("/","-");

        DIR_LOGS=DIR_ROOT_TS+DIR_LOGS+sF2+ "_"+sFlog.logDate(4)+".log";
        sHelp = new SeleniumHelp(SYS_CONFIG,LOG_TEST);
        sHelp.setDIR_ROOT_TS(DIR_ROOT_TS);
        DIR_TS_PARAM=DIR_ROOT_TS+DIR_TS_PARAM;
    }

    //********************************************************

    //********************************************************
    public void setUp() throws Exception {

        String dire_lancement = System.getProperty("user.dir");



        LOG_TEST.add("START TEST  = " +"Open hElios Monitoring");

        LOG_TEST.add("Dir lancement ="+dire_lancement);
        driver = sHelp.openWebdriver(driver, CONF_BROWSER); // FIREFOX  CHROME IEXPLORER PHANTOMJS

        sFlog.Add_String("apres INIT =" + CONF_BROWSER, DIR_LOGS);
        LOG_TEST.add("INIT OK =" + CONF_URL );
        try {

            WAIT = new WebDriverWait(driver, 10);
            Dimension dime = new Dimension(1400, 900);
            driver.manage().window().setSize(dime);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

            LOG_TEST.add("TRY TO OPEN URL =" + CONF_URL );

            driver.get(CONF_URL);

        } catch (Exception e) {
            e.printStackTrace();
            LOG_TEST.add("CRASH ");
            sHelp.screenShotCrash(driver);
        }

    }

    @SuppressWarnings("static-access")
	public void trtMonitoringActions(Map<String, Object> d_upload,String acts) throws Exception {
        // d_upload =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
        DATA_UPLOADED=d_upload;
        TRANSACT_ID=(String)DATA_UPLOADED.get("TRANS_ID");
        LIST_ACTIONS=acts.split(":");
        PageAccesToHeliosMonitoring monoPage = new PageAccesToHeliosMonitoring(driver, sUtil,sHelp,WAIT, LOG_TEST);

        System.out.println("\n--->CALL TEST = ");
        try {

            for(String act : LIST_ACTIONS){
                String txtEvent="";

                act=act.trim();
                switch (act) {
                    case "FindTransID": // FI
                        LOG_TEST.add("FIN TRANSACTION ID = "+TRANSACT_ID);
                        monoPage.setTransID(TRANSACT_ID);
                        break;
                    case "WaitPOR_009": // FI
                        LOG_TEST.add("CALL CHECK EVENT = POR_009");
                        txtEvent=monoPage.waitingEvent("POR_009");
                        LOG_TEST.add("CALL CHECK EVENT = POR_009");
                        String stat=monoPage.okDataHLM_009(txtEvent,DATA_UPLOADED);
                        if(stat.length()>1){
                            LOG_TEST.add(stat);
                        }
                        break;

                    case "WaitHML_015": // FI
                        LOG_TEST.add("CALL WAIT EVENT = HML_015");
                        txtEvent=monoPage.waitingEvent("HML_015");
                        LOG_TEST.add("CALL CHECK EVENT = HML_015");

                        stat=monoPage.okDataHLM_015NoNull(txtEvent,DATA_UPLOADED);
                        if(stat.length()>1){
                            LOG_TEST.add(stat);
                        }
                        break;
                    case "WaitDE_001": // FI
                        LOG_TEST.add("CALL WAIT EVENT = DE_001");
                        txtEvent=monoPage.waitingEvent("DE_001");
                        LOG_TEST.add("CALL CHECK EVENT = HML_015");
                        stat=monoPage.okData_De001_CheckSubFiles(txtEvent,DATA_UPLOADED);
                        if(stat.length()>1){
                            LOG_TEST.add(stat);
                        }
                        break;

                    case "POP1": // FI
                        JOptionPane jop1 = new JOptionPane();
                        jop1.showMessageDialog(null, "Clic OK pour Continuer",
                                "Tempo", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case "CLOSE": // FI
                        driver.close();
                        if (driver !=null)
                            driver.quit();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            int i=LOG_TEST.size();
//            String s=LOG_TEST.get(i-1);
//            s="Fail test when "+s;
//            LOG_TEST.set(i-1,s);
//            LOG_TEST.add("CRASH");
            if (driver !=null)
                driver.quit();
            sUtil.genExeption();
        }
        System.out.println("---> FIN TEST ");
    }
    //***************************************************


    //***************************************************


    public boolean logGetIfOK(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return false;
        else
            return true;
    }
    public String logGetLastAction(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return LOG_TEST.get(i-2);
        else
            return LOG_TEST.get(i-1);
    }

    public void setTansactID(String s){
        TRANSACT_ID=s;
    }
    public void setSendigData(String gr){
        SUBJECT_GR=SUBJECT_GR+gr;
    }
    /************************************************************************************
     * ********************   MAIN
     ************************************************************************************/

    public  static void main(String[] args) throws Exception{

        Map<String, Object>  D_UPLOADED = new HashMap<>();
        String  tagMail="";
        String  tagAction="";
        String fileParam="";


        if (args.length ==2) {
            System.out.println(args[0] +"+ "+args[1] );
            tagMail=args[0] ;
            tagAction=args[1] ;
            System.out.println(tagMail+"+ "+tagAction);
        }else{
//            D_UPLOADED.put("TRANS_ID","d5d81029-0b13-4033-a707-2e43f1e76b38");
//            D_UPLOADED.put("TYPE_BORD","Claims"); // Claims=4
//            D_UPLOADED.put("PERIOD_BORD","07/2019 - 08/2019");
//            D_UPLOADED.put("SENDER_BORD","cuser9 INT Tests");
//            D_UPLOADED.put("SENDING_DATE","17/06/2019 (1:02 PM)");
//            D_UPLOADED.put("GR_BORD","Aegon Group A");
//            D_UPLOADED.put("ORIGIN_BORD","Portal");
//            D_UPLOADED.put("NB_BORD","2");
            //252b0ba7-da47-46b2-8f5e-399dd4e2e239
            D_UPLOADED.put("TRANS_ID","216d788d-2d3f-4ad6-b9e2-df8a3c238a02");

            D_UPLOADED.put("TYPE_BORD","Claims"); // Claims=4
            D_UPLOADED.put("PERIOD_BORD","1/2017 - 10/2018");
            D_UPLOADED.put("SENDER_BORD","TA CedentSuper-4");
            D_UPLOADED.put("SENDING_DATE","24/06/2019 (12:54 PM)");
            D_UPLOADED.put("GR_BORD","Aegon Group A");
            D_UPLOADED.put("ORIGIN_BORD","Portal");
            D_UPLOADED.put("NB_BORD","2");
            D_UPLOADED.put("FILES_LOADED","C:\\0A_TS1_Portal\\06_JDD\\00_BORDEREAUX\\BORDEREAU_001Kb_044.csv;C:\\0A_TS1_Portal\\06_JDD\\00_BORDEREAUX\\BORDEREAU_001Kb_057.csv");

            //tagAction ="FindTransID:WaitHML_015:CLOSE";
            //tagAction ="FindTransID:WaitPOR_009:CLOSE";
            tagAction ="FindTransID:WaitDE_001:CLOSE";
        }



        /*********  Construit le test ***********************
         *
         */
        AccesToHeliosMonitoring ta = new AccesToHeliosMonitoring();
        //ta.setTansactID("17e69c1b-a5d8-42c8-8f9d-ee9106a72dca");
        //ta.setSubject_Gr("Aegon");
        ta.setUp();

        ta.trtMonitoringActions(D_UPLOADED,tagAction);;

        System.exit(0);
    }

}
