package AccesOutlook;

import Helper.SeleniumUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class PageAccesOutlook {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private String FIC_LOGS="";

    private SeleniumUtil sUtil = new SeleniumUtil();

    private List<String> LOG_TEST   =new ArrayList<>();

    /***********************************************************************
     *
     * @param driver
     * @return
     */

    public PageAccesOutlook(WebDriver driver,WebDriverWait wait,List<String>  d) {
        this.driver = driver;
        this.WAIT=wait;
        LOG_TEST=d;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {

        return this.driver;
    }
    //*************************************************************************
    @FindBy(css = ".btn")
    private WebElement ButtonPrimaryCss;
    public void clickButtonPrimary() throws Exception{
        String wHandl = driver.getWindowHandle();
        WAIT.until(ExpectedConditions.elementToBeClickable(ButtonPrimaryCss));
        System.out.println(wHandl);
        ButtonPrimaryCss.click();
        //sFlog
        sUtil.tempo(2);
    }
    public void loginOutlook(String s_login, String s_pass ) throws Exception {

        LOG_TEST.add("wait   ");


        //WAIT.until(ExpectedConditions.urlContains("microsoftonline.com"));
        sUtil.tempo(2);
        LOG_TEST.add("User "+s_login);
        EnterUser(s_login);

        LOG_TEST.add("Pass "+s_pass);
        EnterPwdVal(s_pass);
    }



    @FindBy(css = "#idSIButton9")
    private WebElement SeconnecterSuivantCss;
    @FindBy(css = "#i0116")
    private WebElement SeConnecterCss;
    public void EnterUser(String id) throws Exception{

        while (!SeConnecterCss.isDisplayed())
            sUtil.tempo(4);

        SeConnecterCss.sendKeys(id);
        LOG_TEST.add("Clic user button next");
        SeconnecterSuivantCss.click();

        //WAIT.until(ExpectedConditions.visibilityOf(verifEle));
        sUtil.tempo(1);
    }

    @FindBy(css = "#idA_PWD_ForgotPassword")
    private WebElement verifEle;
    @FindBy(css = "#idSIButton9")
    private WebElement GoConnexion;
    @FindBy(css = "#i0118")
    private WebElement Pwd1Css ;
    public void EnterPwdVal(String id) throws Exception {
        LOG_TEST.add("Wait url psw page");

        sUtil.tempo(2);

        Pwd1Css.sendKeys(id);
        LOG_TEST.add("Clic psw button next");
        GoConnexion.click();

        sUtil.tempo(4);
    }
    public void dispBoite()throws Exception {
        sUtil.tempo(3);
        moreActions.click();
        sUtil.tempo(4);
        inView.click();
        sUtil.tempo(3);
    }


    @FindBy(css = ".c-action-menu > button:nth-child(1)")
    private WebElement moreActions;
    @FindBy(css = "#home\\.banner\\.profile\\.view-inbox")
    private WebElement inView;


    @FindBy(xpath = "//*[@id=\"app\"]/div/div[2]/div/div[1]/div[3]/div[2]/div/div[1]/div[2]/div/div/div/div/div")
    private WebElement TabAllEmail;
    public void clickLastEmail(String id) throws Exception{
        List<WebElement> optionCount = TabAllEmail.findElements
                (By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[1]/div[3]/div[2]/div/div[1]/div[2]/div/div/div/div/div/div"));

        WebElement we0=optionCount.get(1);
        we0.findElement(By.xpath("div/div/div/div[2]/div[1]/div[1]/span")).click();
        String nId=getTransID();
        boolean byes=false;
        if (!id.contentEquals(nId))
            sUtil.genExeption();

    }

    @FindBy(xpath = "//*[@id=\"app\"]/div/div[2]/div/div[1]/div[3]/div[2]/div/div[3]/div/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/div[1]/div/div/font/span/div/a")
    private WebElement GetTransID;
    public String getTransID() throws Exception {
        String sp[] = GetTransID.getText().split("=");
        return sp[1];

    }

    @FindBy(xpath = "//*[@id=\"app\"]/div/div[2]/div/div[1]/div[3]/div[2]/div/div[3]/div/div/div/div/div[1]/div/div/div/span")
    private WebElement getSubject;
    public void checkSubject(String sub) throws Exception {
        String sp = getSubject.getText();
        if (!sub.contentEquals(sp))
            sUtil.genExeption();
    }


}
