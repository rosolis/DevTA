package AccesOutlook;

import CommonWeb.PageLoginPortal;
import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class AccesOutlook {



    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;

    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;

    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS

    private String FILE_PARAM="OutlookPar.txt";
    private int NUM_LIN_TEST=0;
    private boolean logsTrue = true;
    private FileAndLog sFlog = null;

    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String DIR_TS_PARAM=DIR_RUN;
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String GLOBAL_URL="02_LOG/";
    private String BROWSER_FROM_SYS="NO";
    private String BROWSER_FOR_ALL_TEST="";

    Properties SYS_CONFIG = new Properties();
    private String CONF_URL="https://login.live.com/";
    private String ADRESSE_MAIL="";
    private String ADRESSE_PASW="";
    private String LIST_ACTIONS[]=null;
    private List<String> LOG_TEST   =new ArrayList<>();

    private String TRANSACT_ID="";
    private String SUBJECT_GR="Bordereaux submission confirmation for ";
    private String HANDLE_0="";
    private String HANDLE_1="";

    //********************************************************
    //********************************************************


    public AccesOutlook() throws IOException {
        String os = System.getProperty("os.name").toLowerCase();

    }
    public AccesOutlook(String mail, String act) throws Exception {

        String dire_lancement = System.getProperty("user.dir");
        System.out.println("dire_lancement:" + dire_lancement);

        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        int deb = dire_lancement.lastIndexOf("0A_TS1_Portal");
        String sp=dire_lancement.substring(0,deb-1);
        if (isOperatingSystemWindows) {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        }
        System.out.println("Start= "+DIR_ROOT_TS);
        String sp1[]=mail.split(":");

        ADRESSE_MAIL=sp1[0].trim();
        ADRESSE_PASW=sp1[1].trim();

        LIST_ACTIONS=act.split(":");
        sFlog = new FileAndLog();
        sFlog.setLogs(logsTrue);
        sUtil= new SeleniumUtil();

        InputStream inputRoot = new FileInputStream(DIR_ROOT_TS+"00_SYS_CONFIG.txt");

        SYS_CONFIG.load(inputRoot);
        DIR_REPORT = SYS_CONFIG.getProperty("DIR_REPORTS");

        DIR_JDD=SYS_CONFIG.getProperty("DIR_JDD");
        DIR_LOGS=SYS_CONFIG.getProperty("DIR_LOGS");
        DIR_DRIVER=SYS_CONFIG.getProperty("DIR_DRIVER");
        DIR_TS_PARAM=SYS_CONFIG.getProperty("DIR_TS_INTEGRATION");
        GLOBAL_URL=SYS_CONFIG.getProperty("SYS_CNX_LOGIN_PORTAL");
        BROWSER_FOR_ALL_TEST=SYS_CONFIG.getProperty("SYS_CNX_BROWSER");
        CONF_BROWSER=BROWSER_FOR_ALL_TEST.trim();
        DIR_RUN=DIR_ROOT_TS+DIR_RUN;

        String sF[]=FILE_PARAM.split("\\.");
        String sF2=sF[0].replace("\\","-");
        sF2=sF2.replace("/","-");

        DIR_LOGS=DIR_ROOT_TS+DIR_LOGS+sF2+ "_"+sFlog.logDate(4)+".log";
        sHelp = new SeleniumHelp(SYS_CONFIG,LOG_TEST);
        sHelp.setDIR_ROOT_TS(DIR_ROOT_TS);
        DIR_TS_PARAM=DIR_ROOT_TS+DIR_TS_PARAM;
    }

    //********************************************************

    //********************************************************
    public void setUp() throws Exception {

        String dire_lancement = System.getProperty("user.dir");



        LOG_TEST.add("START TEST  = " +"Open outlook mail");

        LOG_TEST.add("Dir lancement ="+dire_lancement);
        driver = sHelp.openWebdriver(driver, CONF_BROWSER); // FIREFOX  CHROME IEXPLORER PHANTOMJS

        sFlog.Add_String("apres INIT =" + CONF_BROWSER, DIR_LOGS);
        LOG_TEST.add("INIT OK =" + CONF_URL );
        try {

            WAIT = new WebDriverWait(driver, 10);
            Dimension dime = new Dimension(1400, 800);
            driver.manage().window().setSize(dime);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

            LOG_TEST.add("TRY TO OPEN URL =" + CONF_URL );

            driver.get(CONF_URL);

            PageAccesOutlook outlookPage = new PageAccesOutlook(driver, WAIT, LOG_TEST);
            sUtil.tempo(2);

            LOG_TEST.add("TRY TO LOGIN WITH USER AND PSW "+"Login user = "+ADRESSE_MAIL+ " ");

            outlookPage.loginOutlook(ADRESSE_MAIL, ADRESSE_PASW);

            sUtil.tempo(3);
            //WAIT.until(ExpectedConditions.urlContains("microsoftonline.com"));
            driver = outlookPage.GetDriver();


        } catch (Exception e) {
            e.printStackTrace();
            LOG_TEST.add("CRASH ");
            sHelp.screenShotCrash(driver);
        }

    }

    public void trtOutlookActions() throws Exception {
        PageAccesOutlook outlookPage = new PageAccesOutlook(driver, WAIT, LOG_TEST);
        HANDLE_0 = driver.getWindowHandle();
        outlookPage.dispBoite();
        int iHandle = 0;
        for (String winHandle : driver.getWindowHandles()) {
            iHandle++;
            HANDLE_1 = winHandle;
        }
        sUtil.tempo(2);
        driver.switchTo().window(HANDLE_1);


        System.out.println("\n--->CALL TEST = ");
        try {
            LOG_TEST.add("FIN TEST  _-------------");
            for(String act : LIST_ACTIONS){
                act=act.trim();
                switch (act) {
                    case "FindTransID": // FI
                        outlookPage.clickLastEmail(TRANSACT_ID);
                        break;
                    case "CheckSubject": // FI
                        outlookPage.checkSubject(SUBJECT_GR);
                        break;
                    case "POP1": // FI
                        JOptionPane jop1 = new JOptionPane();
                        jop1.showMessageDialog(null, "Clic OK pour Continuer",
                                "Tempo", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case "CLOSE": // FI
                        driver.close();
                        if (driver !=null)
                            driver.quit();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            int i=LOG_TEST.size();
            String s=LOG_TEST.get(i-1);
            s="Fail test when "+s;
            LOG_TEST.set(i-1,s);
            LOG_TEST.add("CRASH");
            if (driver !=null)
                driver.quit();
            sUtil.genExeption();
        }
        System.out.println("---> FIN TEST ");
    }
    //***************************************************


    //***************************************************


    public boolean logGetIfOK(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return false;
        else
            return true;
    }
    public String logGetLastAction(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return LOG_TEST.get(i-2);
        else
            return LOG_TEST.get(i-1);
    }

    public void setTansactID(String s){
        TRANSACT_ID=s;
    }
    public void setSubject_Gr(String gr){
        SUBJECT_GR=SUBJECT_GR+gr;
    }
    /************************************************************************************
     * ********************   MAIN
     ************************************************************************************/

    public  static void main(String[] args) throws Exception{
        HashMap<String,String> map_test=new HashMap<>();

        String  tagMail="";
        String  tagAction="";
        String fileParam="";


        if (args.length ==2) {
            System.out.println(args[0] +"+ "+args[1] );
            tagMail=args[0] ;
            tagAction=args[1] ;
            System.out.println(tagMail+"+ "+tagAction);
        }else{
            tagMail ="TaCedentSuper-4@outlook.com:1CedentSuper-4";
            tagAction ="FindTransID:CheckSubject:CLOSE";
        }



        /*********  Construit le test ***********************
         *
         */
        AccesOutlook ta = new AccesOutlook(tagMail,tagAction);
        ta.setTansactID("88b610cf-ea61-4ecd-b441-80983b2002ba");
        ta.setSubject_Gr("Aegon");
        ta.setUp();

        ta.trtOutlookActions();;

        System.exit(0);
    }

}
