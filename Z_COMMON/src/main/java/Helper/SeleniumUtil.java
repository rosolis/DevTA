package Helper;



import org.apache.commons.io.FileUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


public class SeleniumUtil {

    /***********************************************************************
     *
     */
    public SeleniumUtil() {
        ;
    }

    //**********************************************************************


    public void memoClipboard(String st) {
        StringSelection selection = new StringSelection(st);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }
    public void tempo(int n) throws Exception {
        // n=millisecondes
        int t_1 = 500;
        switch (n) {
            case 0:
                t_1 = 250;
                break;
            case 1:
                t_1 = 500;
                break;
            case 2:
                t_1 = 1000;
                break;
            case 3:
                t_1 = 2500;
                break;
            case 4:
                t_1 = 5000;
                break;
            case 5:
                t_1 = 10000;
                break;
            case 6:
                t_1 = 20000;
                break;
            default:
                t_1 = 500;
        }
        Thread.sleep(t_1);

    }

    public List<String> genPeriodFromDateTo(){
        List<String> lRet=new ArrayList<>();

        int y1=getIntRandomNum(2015,2020,4);
        int y2=getIntRandomNum(2015,2020,4);
        int m1=getIntRandomNum(1,12,2);
        int m2=getIntRandomNum(1,12,2);

        String tmp="";
        int ca=0;
        if (y1>y2 ) {
            ca=0;
        }else if (y1<y2 ){
            ca=1;
        }else if (m1>m2 ){ // cas egaux
            ca=0;
        }else
            ca =1;

        switch (ca){
            case 0:
                tmp = String.format("%02d/%04d", m2, y2);
                lRet.add(tmp);
                tmp = String.format("%02d/%04d", m1, y1);
                lRet.add(tmp);
                break;
            case 1:
                tmp = String.format("%02d/%04d", m1, y1);
                lRet.add(tmp);
                tmp = String.format("%02d/%04d", m2, y2);
                lRet.add(tmp);
                break;

                default:
                    tmp = String.format("%02d/%04d", m1, y1);
                    lRet.add(tmp);
                    tmp = String.format("%02d/%04d", m2, y2);
                    lRet.add(tmp);
                    break;
        }

       return lRet;

    }
    public String genRandomDatemmDDyyyy(String sep,int yearmin,int yearmax){
        return getStrRandomNum(1,29,2)+sep+   getStrRandomNum(1,12,2)+sep+getStrRandomNum(yearmin,yearmax,4);
    }

    public String getStrRandomNum(int min,int max,int len){
        Random rand = new Random();
        int n = rand.nextInt(max-min);
        if (n<min)
            n+=min;

        String cpt = "";
        if (len>0)
            cpt = String.format("%0"+len+"d", n);
        else
            cpt =String.format("%d", n);
        return cpt;
    }
    public int getIntRandomNum(int min,int max,int len){
        Random rand = new Random();
        int n = rand.nextInt(max-min);
        if (n<min)
            n+=min;
        return n;
    }
    /*************************************************
     * Génére date-heure au format X = 010100hhmm avec les 4 dernier char qui ont l'heure et minutes
     *
     *************************************************/
    public String genereDateLa(int n) {
        // Génére string date au format jjmmaaaa
        // Génére string date au format jjmmaaaa
        TimeZone timeZone =TimeZone.getTimeZone("UTC");
        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        if (n>100){
            localCalendar = Calendar.getInstance(timeZone);
            n=n-100;
        }

        int tDay   = localCalendar.get(Calendar.DATE);
        int tMonth = localCalendar.get(Calendar.MONTH) + 1;
        int tYear  = localCalendar.get(Calendar.YEAR);
        Date tTime = localCalendar.getTime();
        int tHeure = localCalendar.get(Calendar.HOUR_OF_DAY);
        int tMin   = localCalendar.get(Calendar.MINUTE);
        int tsec1  = localCalendar.get(Calendar.SECOND);
        int tsec2  = localCalendar.get(Calendar.MILLISECOND);



        if (n == 1) { // YYYY-MM-DDThh:mm:ss-SS
            return String.format("%s-%02d-%02d %02d-%02d-%02d-%d", tYear, tMonth, tDay, tHeure, tMin, tsec1, tsec2);
        }
        if (n == 2) { // YYYY-MM-DDThh:mm:ss
            return String.format("%s-%02d-%02dT%02d-%02d-%02d", tYear, tMonth, tDay, tHeure, tMin, tsec1);
        }
        if (n == 3) { // YYYY-MM-DD
            return String.format("%s-%02d-%02d", tYear, tMonth, tDay);
        }
        if (n == 4) { // mm-dd-hh-mm-ss
            return String.format("%02d-%02d-%02d-%d", tMonth, tDay, tHeure, tMin, tsec1);
        }

        if (n == 5) { // YYYYMMDDhhmmss
            return String.format("%s%02d%02d%02d%02d%02d", tYear, tMonth, tDay, tHeure, tMin, tsec1);
        }

        if (n == 6) { // YYYYMMDDhhmm
            return String.format("%s%02d%02d%02d%02d", tYear, tMonth, tDay, tHeure, tMin);
        }
        if (n == 7) { // YYYYMMDD
            return String.format("%s%02d%02d", tYear, tMonth, tDay);
        }
        if (n == 8) { // DDhhmmss
            return String.format("%02%02d%02d%02d%02d", tMonth,tDay, tHeure, tMin, tsec1);
        }
        if (n == 9) { // AAAAMMJJ
            return String.format("%s%02d%02d", tYear, tMonth, tDay);
        }
        if (n == 20) { // JJ/MM/AAAA hh:mm:ss-SS
            return String.format("%s/%02d/%02d %02d:%02d:%02d-%d", tDay,tMonth,tYear,tHeure, tMin, tsec1, tsec2);
        }
        if (n == 21) { // JJ/MM/AAAA hh:mm:ss
            return String.format("%s/%02d/%02d %02d:%02d:%02d", tDay,tMonth,tYear,tHeure, tMin, tsec1, tsec2);
        }

        return String.format("%s-%02d-%02d %02d-%02d-%02d-%d", tYear, tMonth, tDay, tHeure, tMin, tsec1, tsec2);

    }

    public int timeCompare(Date date1,Date date2) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        System.out.println("date1 : " + sdf.format(date1));
        System.out.println("date2 : " + sdf.format(date2));
        return date1.compareTo(date2);
    }

    public int timeFourchette(Date date0,Date date1,Date date2) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if ((date0.compareTo(date1) == 0)||(date0.compareTo(date2) == 0)) {
            System.out.println("Date1 is equal to Date2");
            return 0;
        }
        if ((date0.compareTo(date1) > 0)&&(date0.compareTo(date2) < 0)) {
            System.out.println("Date1 is equal to Date2");
            return 0;
        }else
            return 1;
    }
    public Date timeConvertStringTopicToDate(String fmtDate) throws ParseException {
        SimpleDateFormat sdf =
            new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",Locale.getDefault());
        // Give it to me in GMT time.
        //sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date   date       = sdf.parse(fmtDate);
        return date;
    }
    public Date timeAddMinToDate(Date dt, int min){
         Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.MINUTE, min);
        Date tTime = cal.getTime();
        return tTime;
    }


    //    **********************************************************
    //********************************************************
    public List<Long> getSizeFiles(List<String> list_Files){
        List<Long> sRet=new ArrayList<>();
        Long tot=new Long(0);
        int nb=list_Files.size();
        for (String f : list_Files){
            File file =new File(f);
            Long bytes = file.length();
            Long kilobytes = (bytes / 1024);
            Long mega = (kilobytes / 1024);
            tot=tot+mega;
        }
        sRet.add(new Long(nb)); //"NB FILES : "+nb+" ; "+ "MEGABYTES : "+tot);
        sRet.add(tot); // "+tot);
        System.out.println("megabytes : " + tot);
        return sRet;
    }
    public Long getSizeOneFile(String f){

        Long tot=new Long(0);
        File file =new File(f);
        Long bytes = file.length();
        Long kilobytes = (bytes / 1024);
        Long mega = (kilobytes / 1024);
        return mega;
    }
    public void genExeption(){
        int a=100;
        int b=1;
        b--;
        int c=a/b;
    }

    public void del_File(String fichier) {
        // del fichier
        File fg = new File(fichier);
        if (fg.exists()) {
            fg.delete();
        }
    }
    public void rename_File(String source,String dest) throws IOException {
        File sr = new File(source);
        File copied = new File(dest);
        boolean success = sr.renameTo(copied);
    }

    public void copy_File(String source,String dest) throws IOException {
        File sr = new File(source);
        File copied = new File(dest);
        FileUtils.copyFile(sr, copied);
    }


    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        SeleniumUtil sUt = new SeleniumUtil();

        System.out.println("===FIN=== ");
    }
}

