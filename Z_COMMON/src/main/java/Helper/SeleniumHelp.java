package Helper;


import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.FileAndLog;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.*;


public class SeleniumHelp {
	Properties SYS_CONFIG = new Properties();

	//protected static WebDriver driver = null;
	private WebDriverWait WAIT = null;
	private String CONF_VAR = "";
	private FileAndLog sFlog = new FileAndLog();


	private String FIC_LOGS = "";

	private static final String GECKO_DRIVER_BASE_PATH = "gecko/geckodriver";
	private static final String WIN_GECKO_DRIVER_EXTENSION = ".exe";
	private static final String LINUX_GECKO_DRIVER_EXTENSION = "-linux";
	private static final String MACOS_GECKO_DRIVER_EXTENSION = "-macos";
	private ClassLoader classloader = null;
	private String DIR_ROOT_TS = "/04_TS1_Portal/";


	private String DIR_TS_PARAM = "01_ParamPORTAL/";
	private String DIR_REPORT = "05_REPORTS/";
	private String DIR_JDD = "06_JDD/";
	private String DIR_LOGS = "07_LOG/";
	private String DIR_DRIVER = "08_DRIVERS/";
	private String GLOBAL_URL = "02_LOG/";
	private String BROWSER_FROM_SYS = "NO";
	private String BROWSER_FOR_ALL_TEST = "";
	private String EXCEL_REPORT = "";
	private List<String> LOG_TEST = new ArrayList<>();

	//******************************************************************
	public SeleniumHelp() throws IOException {

		;

	}

	public SeleniumHelp(Properties prop, List<String> dirlogs) throws IOException {

		LOG_TEST = dirlogs;
		SYS_CONFIG = prop;


	}

	public void setPropert(Properties prop) {
		SYS_CONFIG = prop;
	}

	public void setDIR_ROOT_TS(String s) throws IOException {

		DIR_ROOT_TS = s;
		InputStream inputRoot = new FileInputStream(DIR_ROOT_TS + "00_SYS_CONFIG.txt");

		SYS_CONFIG.load(inputRoot);
		DIR_REPORT = SYS_CONFIG.getProperty("DIR_REPORTS");
		DIR_JDD = SYS_CONFIG.getProperty("DIR_JDD");
		DIR_LOGS = SYS_CONFIG.getProperty("DIR_LOGS");
		DIR_DRIVER = SYS_CONFIG.getProperty("DIR_DRIVERS");
		DIR_TS_PARAM = SYS_CONFIG.getProperty("DIR_TS1");
		GLOBAL_URL = SYS_CONFIG.getProperty("SYS_CNX_LOGIN_PORTAL");
		BROWSER_FROM_SYS = SYS_CONFIG.getProperty("BROWSER_ALL");
		BROWSER_FOR_ALL_TEST = SYS_CONFIG.getProperty("SYS_CNX_BROWSER");
	}

	//******************************************************************
	public String getDirWebDriver(String nav) {

		String os = System.getProperty("os.name").toLowerCase();
		boolean isOperatingSystemWindows = os.contains("win");
		String sDriver = "";
		String sPlus = "_L";
		if (isOperatingSystemWindows) {
			sPlus = "_W";
		} else if (os.contains("mac")) {
			sPlus = "_M";
		} else {
			sPlus = "_L";
		}


		if (nav.contains("FIREFOX"))
			sDriver = SYS_CONFIG.getProperty("PILOT_GECKO" + sPlus).trim();
		if (nav.contains("CHROME"))
			sDriver = SYS_CONFIG.getProperty("PILOT_CHROME" + sPlus).trim();
		if (nav.contains("IEXPLORE"))
			sDriver = SYS_CONFIG.getProperty("PILOT_IE" + sPlus).trim();
		if (nav.contains("OPERA"))
			sDriver = SYS_CONFIG.getProperty("PILOT_OPERA" + sPlus).trim();

		return sDriver;

	}

	public List<String> getFileToSendFromJddBord(Properties TEST_CONFIG) throws IOException {
		List<String> lRet = new ArrayList<>();
		String sTmp = TEST_CONFIG.getProperty("SENDING_TYPE").trim();
		lRet.add(sTmp);
		String dir_fileToSend = TEST_CONFIG.getProperty("DIR_FILE_TO_SEND").trim();
		if (dir_fileToSend.contains("00_BORD")) {
			sTmp = TEST_CONFIG.getProperty("TYPE_BORDEREAU").trim();
			lRet.add(sTmp);
			sTmp = TEST_CONFIG.getProperty("DATE_BORDEREAU").trim();
			lRet.add(sTmp);
		}

		String fileToSend = TEST_CONFIG.getProperty("FILE_TO_SEND").trim();
		//String fileRename = TEST_CONFIG.getProperty("DIR_FILE_RENAME").trim();
		String[] st = fileToSend.split(";");
		String os = System.getProperty("os.name").toLowerCase();

		for (String s : st) {
			s = s.trim();
			String spli[] = s.split("\\.");
			String t = sFlog.logDate(5);

			String sDirJdd = DIR_ROOT_TS + DIR_JDD + dir_fileToSend + s;
			//String sDirRename = DIR_ROOT_TS+DIR_JDD+fileRename+spli[0]+"_"+t+"."+spli[1];
			//sFlog.copy_File(sDirJdd,sDirRename);

			if (os.contains("win")) {
				if (sDirJdd.contains("/"))
					sDirJdd = sDirJdd.replaceAll("/", "\\\\");
			} else {
				if (sDirJdd.contains("\\"))
					sDirJdd = sDirJdd.replaceAll("\\\\", "/");
			}

			lRet.add(sDirJdd);
		}
		return lRet;
	}

	public List<String> getFileToSendFromJddOR(Properties TEST_CONFIG) {
		List<String> lRet = new ArrayList<>();
		String sTmp = TEST_CONFIG.getProperty("SENDING_TYPE").trim();
		lRet.add(sTmp);
		String dir_fileToSend = TEST_CONFIG.getProperty("DIR_FILE_TO_SEND").trim();
		String fileToSend = TEST_CONFIG.getProperty("FILE_TO_SEND").trim();
		String[] st = fileToSend.split(";");
		String os = System.getProperty("os.name").toLowerCase();

		for (String s : st) {
			s = s.trim();
			String sDirJdd = DIR_ROOT_TS + DIR_JDD + dir_fileToSend + s;

			if (os.contains("win")) {
				if (sDirJdd.contains("/"))
					sDirJdd = sDirJdd.replaceAll("/", "\\\\");
			} else {
				if (sDirJdd.contains("\\"))
					sDirJdd = sDirJdd.replaceAll("\\\\", "/");
			}
			lRet.add(sDirJdd);
		}
		return lRet;
	}

	public Path getGeckoDriver() throws URISyntaxException, IOException {
		String driverClassPath = GECKO_DRIVER_BASE_PATH;
		String os = System.getProperty("os.name").toLowerCase();
		boolean isOperatingSystemWindows = os.contains("win");
		if (isOperatingSystemWindows) {
			driverClassPath += WIN_GECKO_DRIVER_EXTENSION;
		} else if (os.contains("mac")) {
			driverClassPath += MACOS_GECKO_DRIVER_EXTENSION;
		} else {
			driverClassPath += LINUX_GECKO_DRIVER_EXTENSION;
		}
		URL url = classloader.getResource(driverClassPath);
		String p = classloader.getResource(driverClassPath).getPath();
		System.out.println("URL = " + url);
		System.out.println("URLP = " + p);
		String s_url = classloader.getResource(driverClassPath).toString();
		System.out.println("S_URL = " + s_url);
		System.out.println(driverClassPath);
		System.out.println("URL======> " + url);
		Path geckoFile = null;
		if (s_url.startsWith("jar:")) {
			URI uri = getClass().getClassLoader().getResource("/" + driverClassPath).toURI();
			geckoFile = Paths.get(getClass().getClassLoader().getResource("/" + driverClassPath).toURI());
		} else {
			geckoFile = Paths.get(getClass().getClassLoader().getResource(driverClassPath).toURI());
		}
		System.out.println("geckodriver= " + geckoFile);
		// if file is not windows, we must set its posix file permissions
		System.out.println("====> operating = " + isOperatingSystemWindows);
		if (!isOperatingSystemWindows) {
			Files.setPosixFilePermissions(geckoFile,
					ImmutableSet.of(PosixFilePermission.OWNER_READ,
							PosixFilePermission.OWNER_WRITE,
							PosixFilePermission.OWNER_EXECUTE,
							PosixFilePermission.GROUP_EXECUTE,
							PosixFilePermission.OTHERS_EXECUTE));
		}
		return geckoFile;
	}

	public InputStream getConfigPath() throws Exception {
		String res = "..\\00_PARAM_TA\\CONFIG.txt";
		String url = classloader.getResource(res).toString();
		System.out.println("CONFIG url = " + url.toString());
		Path conf = null;
		InputStream url1 = null;
		if (url.startsWith("jar:")) {
			url1 = classloader.getResourceAsStream(res);
		} else {
			url1 = classloader.getResourceAsStream(res);
		}
		return url1;
	}

	public Path getFileSendPath(ClassLoader classloader) throws URISyntaxException, IOException {
		String res = "fileSend/bordereaux1.csv";
		String url = classloader.getResource(res).toString();
		System.out.println(url.toString());
		Path conf = null;
		if (url.startsWith("/config/")) {
			conf = Paths.get(getClass().getClassLoader().getResource(url).toURI());
		} else {
			conf = Paths.get(getClass().getClassLoader().getResource(res).toURI());
		}
		return conf;
	}


	/********************************************************************************************************
	 * Ouverture du navigateur (firefox, crhome ou Iexplorer
	 ********************************************************************************************************/
	public WebDriver openWebdriver(WebDriver driver, String nav) throws IOException {

		String dire_lancement = System.getProperty("user.dir");
		System.out.println(dire_lancement);

		String s_DirWebDrives = getDirWebDriver(nav);
		s_DirWebDrives = DIR_ROOT_TS + DIR_DRIVER + s_DirWebDrives;
		Dimension dime = null;
		try {
			switch (nav) {

				case "FIREFOX": // FIREFOX

					LOG_TEST.add("Browser Firefox Drive =" + s_DirWebDrives);
					System.setProperty("webdriver.gecko.driver", s_DirWebDrives);
					driver = new FirefoxDriver();
					break;

				case "CHROME": // CHROME
					//ChromeOptions options = new ChromeOptions();
					//options.setBinary("/path/to/other/chrome/binary");
					LOG_TEST.add("Browser Chrome Drive =" + s_DirWebDrives);
					System.setProperty("webdriver.chrome.driver", s_DirWebDrives);
					driver = new ChromeDriver();

					break;
//				case "OPERA": // CHROME
//					//ChromeOptions options = new ChromeOptions();
//					//options.setBinary("/path/to/other/chrome/binary");
//					LOG_TEST.add("Browser Opera Drive ="+s_DirWebDrives);
//					System.setProperty("webdriver.opera.driver", s_DirWebDrives);
//					driver = new OperaDriver();
//
//					break;
				case "IEXPLORER": // IEXPLORER
					LOG_TEST.add("Browser InternetExplorerDriver Drive =" + s_DirWebDrives);
					File file = new File(s_DirWebDrives);
					System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
					driver = new InternetExplorerDriver();

					break;
				case "REMOTEHUB": // FIREFOX

					DesiredCapabilities capabilities = DesiredCapabilities.firefox();

					capabilities.setCapability("version", "");

					capabilities.setPlatform(Platform.LINUX);

					//driver = new RemoteWebDriver(new URL("http://10.19.88.43:4444/wd/hub"),capabilities);
					driver = new RemoteWebDriver(new URL("http://10.19.84.80:4444/wd/hub"), capabilities); //xenon
//					driver.get("http://google.com/");
//					System.out.println("Title is : "+driver.getTitle());
//					driver.quit();
					break;
				default: // Firefox
					System.out.println("===============> DRIVE NO SELECTED =" + nav);
					driver = new FirefoxDriver();
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR DRIVE Exeption" + nav);
			return null;

		}
		return driver;

	}

	public WebDriver simplyOpenWebdriver(WebDriver driver, String nav) throws IOException {

		String dire_lancement = System.getProperty("user.dir");
		System.out.println(dire_lancement);
		String os = System.getProperty("os.name").toLowerCase();
		boolean isOperatingSystemWindows = os.contains("win");
		String dire_home = System.getProperty("user.home");
		String pathDriver = "";
		if (isOperatingSystemWindows) {
			pathDriver = ".exe";
		}

		Dimension dime = null;
		try {
			switch (nav) {

				case "FIREFOX": // FIREFOX

					System.setProperty("webdriver.gecko.driver", "C:\\00_PROGRAM\\Drivers\\geckodriver.exe");
					driver = new FirefoxDriver();
					break;

				case "CHROME": // CHROME
					//ChromeOptions options = new ChromeOptions();
					//options.setBinary("/path/to/other/chrome/binary");
					//LOG_TEST.add("Browser Chrome Drive ="+s_DirWebDrives);
					System.setProperty("webdriver.chrome.driver", "C:\\00_PROGRAM\\Drivers\\chromedriver.exe");
					driver = new ChromeDriver();

					break;

				case "IEXPLORER": // IEXPLORER
					//LOG_TEST.add("Browser InternetExplorerDriver Drive ="+s_DirWebDrives);
					//File file = new File(s_DirWebDrives);
					//System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
					driver = new InternetExplorerDriver();

					break;
				default: // Firefox
					System.out.println("===============> DRIVE NO SELECTED =" + nav);
					driver = new FirefoxDriver();
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR DRIVE Exeption" + nav);
			return null;

		}
		return driver;

	}

	public WebDriver openRemoteWebdriver(WebDriver driver, String testName) throws IOException {

		String dire_lancement = System.getProperty("user.dir");
		System.out.println(dire_lancement);

		sFlog.Add_String("START Remote TEST  = " + testName, FIC_LOGS);
		sFlog.Add_String("Dir lancement =" + dire_lancement, FIC_LOGS);

		InputStream input = new FileInputStream(DIR_ROOT_TS + "00_SYS_CONFIG.txt");

		SYS_CONFIG.load(input);
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();

		capabilities.setCapability("version", "");
		capabilities.setPlatform(Platform.WINDOWS);
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);

		return driver;

	}

	/*********************************************/
	public void sendAvecAction(WebDriver d, WebElement ele1, String key1) {
		Actions actionT;

		actionT = new Actions(d);
		actionT.sendKeys(ele1, key1).build().perform();

	}
	//***********  clickAvecAction

	public boolean clickAvecAction(WebDriver driver, By by) {
		WebElement ele1;
		Actions actionT;
		try {
			ele1 = driver.findElement(by);
			actionT = new Actions(driver);
			actionT.moveToElement(ele1);
			actionT.click(ele1);
			Action action = actionT.build();
			action.perform();

			return true;

		} catch (org.openqa.selenium.NoSuchElementException e) {
			//traceOnly ("Error CLICK avec action sur :"+ by );
			e.printStackTrace();
			//mauvaisT("Click Avec Action fail");
			return false;
		}
	}

	public boolean clickEleAction(WebDriver driver, WebElement el, By by) {
		WebElement ele1;
		ele1 = el.findElement(by);
		return clickEleAction(driver, ele1);
	}

	public boolean clickEleAction(WebDriver driver, WebElement el) {
		Actions actionT;
		try {
			actionT = new Actions(driver);
			actionT.moveToElement(el);
			actionT.click(el);
			Action action = actionT.build();
			action.perform();
			//el.click();
			return true;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			System.out.println("Erreur Click ELE avec action ");
			e.printStackTrace();


			return false;
		}
	}

	public boolean INCIDENT_TECHNIQUE(WebDriver driver) {
		//  actionRes[1]= "INCIDENT THECNIQUE "+ actionRes[1];

		return estLaEtAffiche(driver.findElement(By.id("panel-exception-header")));
	}

	// Ele est present et affiché?
	// **********************************************
	public Long waitEleLaEtAffiche(WebElement ele, int maxsecondes, int plusSeconds) throws InterruptedException {

		// return 0L if not found
		Long mDep = System.currentTimeMillis();
		Long mFin = mDep + (maxsecondes * 1000);
		Long lRet = 0L;
		while (System.currentTimeMillis() <= mFin) {
			if (estLaEtAffiche(ele)) {
				Thread.sleep(plusSeconds * 1000);
				mDep = System.currentTimeMillis() - mDep;
				return mDep;
			}
			Thread.sleep(200);
		}
		return lRet;
	}

	public boolean estLaEtAffiche(WebDriver driver, By by) {
		WebElement ele1;
		ele1 = driver.findElement(by);
		return estLaEtAffiche(ele1);
	}

	public boolean estLaEtAffiche(WebElement ele1) {

		try {
			if (ele1 != null) {
				if (ele1.isDisplayed()) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		} catch (NoSuchElementException e) {
			System.out.println("Erreur find element  ");
			e.printStackTrace();

			return false;
		}
	}

	public boolean selectCombo(WebDriver driver, By by, String txt) throws Exception {
		WebElement ele0 = driver.findElement(by);

		return selectComboByValue(ele0, txt, 1);
	}

	public boolean selectComboByIndex(WebElement ele1, int index) {

		Select dropdown = new Select(ele1);
		ele1.click();
		dropdown.selectByIndex(index);
		ele1.click();
		return true;
	}

	public boolean selectComboByValue(WebElement ele1, String txt, int val) {

		Select dropdown = new Select(ele1);
		ele1.click();
		if (val == 0)
			dropdown.selectByVisibleText(txt);
		else
			dropdown.selectByValue(txt);
		ele1.click();
		return true;
	}

	public void JSvisibleEleBy(WebDriver wd, WebElement ele) {

	String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
		((JavascriptExecutor)wd).executeScript(js, ele);
	}
	public void JSScrollDown(WebDriver wd) {
		JavascriptExecutor jse = (JavascriptExecutor) wd;
		jse.executeScript("window.scrollBy(0,250)");
	}
	public void JSScrollUP(WebDriver wd) {
		JavascriptExecutor jse = (JavascriptExecutor) wd;
		jse.executeScript("window.scrollBy(0,-250)");
	}
	public void screenShotCrash(WebDriver dr) throws IOException{

		String fl=FIC_LOGS.replace("log","png");
        File scrFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(fl),true);
    }

	public void screeShotTrace(WebDriver dr,String pngFile) throws IOException{

		File scrFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(pngFile),true);
	}
}
