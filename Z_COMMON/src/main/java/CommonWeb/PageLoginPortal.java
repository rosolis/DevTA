package CommonWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Helper.SeleniumHelp;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumUtil;

public class PageLoginPortal {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private String FIC_LOGS="";

    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = new SeleniumHelp();
    private List<String> LOG_TEST   =new ArrayList<>();

    /***********************************************************************
     *
     * @param driver
     * @return
     */

    public PageLoginPortal(WebDriver driver, SeleniumUtil su, SeleniumHelp sh, WebDriverWait wait, List<String>  d) throws IOException {
        this.driver = driver;
        this.WAIT=wait;
        sUtil=su;
        sHelp=sh;
        LOG_TEST=d;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {

        return this.driver;
    }
    //*************************************************************************
    //@FindBy(css = ".btn-primary")
    @FindBy(xpath = "./html/body/div[2]/div[2]/div[1]/div/form/div/button")
    private WebElement ButtonPrimaryCss;
    public void clickButtonPrimary() throws Exception{
        String wHandl = driver.getWindowHandle();
        WAIT.until(ExpectedConditions.elementToBeClickable(ButtonPrimaryCss));
        System.out.println(wHandl);
        ButtonPrimaryCss.click();
        //sFlog
        sUtil.tempo(2);
    }
    @FindBy(css = "#idSIButton9")
    private WebElement SeconnecterSuivantCss;
    @FindBy(css = "#i0116")
    private WebElement SeConnecterCss;
    public void EnterIdVal(String id) throws Exception{

        while (!SeConnecterCss.isDisplayed())
            sUtil.tempo(4);
        Long l=sHelp.waitEleLaEtAffiche(SeConnecterCss,15,2);
        SeConnecterCss.sendKeys(id);
        LOG_TEST.add("Clic user button next");
        SeconnecterSuivantCss.click();

        //WAIT.until(ExpectedConditions.visibilityOf(verifEle));
        sUtil.tempo(1);
    }

    @FindBy(css = "#idA_PWD_ForgotPassword")
    private WebElement verifEle;
    @FindBy(css = "#idSIButton9")
    private WebElement GoConnexion;
    @FindBy(css = "#i0118")
    private WebElement Pwd1Css ;
    public void EnterPwdVal(String id) throws Exception{
        LOG_TEST.add("Wait url psw page");
        WAIT.until(ExpectedConditions.urlContains("authorize"));
        sUtil.tempo(2);
        LOG_TEST.add("Wait visibility of psw field");
        WAIT.until(ExpectedConditions.visibilityOf(verifEle));
        Pwd1Css.sendKeys(id);
        LOG_TEST.add("Clic psw button next");
        GoConnexion.click();
        sUtil.tempo(2);
        LOG_TEST.add("Clic psw button next");
        GoConnexion.click();
        //WAIT.until(ExpectedConditions.titleContains("Index - SCOR"));
        sUtil.tempo(3);
    }
    @FindBy(xpath = "//*[@id=\"welcomeCarousel\"]/div/div[5]/a")
    private WebElement clickInitialPopUp ;
    @FindBy(xpath = "//*[@id=\"welcomeCarousel\"]/div/div[6]/a")
    private WebElement clickInitialPopUpscor ;
    @FindBy(xpath = "//*[@id=\"welcomeCarousel\"]/div/div[7]/a")
    private WebElement clickInitialPopUpExtern;
    public void loginPortal(String s_login, String s_pass ) throws Exception {

        LOG_TEST.add("clickButtonPrimary  ");

        clickButtonPrimary();
        sUtil.tempo(2);
        LOG_TEST.add("WAIT LOGIN PAGE  ");

        WAIT.until(ExpectedConditions.urlContains("microsoftonline.com"));
        sUtil.tempo(3);
        LOG_TEST.add("User "+s_login);
        EnterIdVal(s_login);

        Long l=0L;
        Long l2=0L;
        if (s_login.contains("@scor.com")) {

            l=sHelp.waitEleLaEtAffiche(clickInitialPopUpscor,10,2);
            if (l==0)
                l=sHelp.waitEleLaEtAffiche(clickInitialPopUpscor,2,2);
            //sUtil.tempo(3);
            clickInitialPopUpscor.click();

        }else if (s_login.contains("External")) {
            LOG_TEST.add("Pass "+s_pass);
            EnterPwdVal(s_pass);
            l = sHelp.waitEleLaEtAffiche(clickInitialPopUpExtern, 10, 2);
            if (l == 0)
                l2 = sHelp.waitEleLaEtAffiche(clickInitialPopUpExtern, 2, 2);
            //sUtil.tempo(3);
            clickInitialPopUpExtern.click();
        }else{
            LOG_TEST.add("Pass "+s_pass);
            EnterPwdVal(s_pass);
            l=sHelp.waitEleLaEtAffiche(clickInitialPopUp,10,2);
            if (l == 0)
                l2 = sHelp.waitEleLaEtAffiche(clickInitialPopUpExtern, 2, 2);
            clickInitialPopUp.click();
        }
    }
}
