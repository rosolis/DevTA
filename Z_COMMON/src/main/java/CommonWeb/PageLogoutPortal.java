package CommonWeb;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class PageLogoutPortal {
    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private String FIC_LOGS="";
    private SeleniumHelp sHelp = new SeleniumHelp();
    private SeleniumUtil sUtil = new SeleniumUtil();
    private FileAndLog sFileAndLog = new FileAndLog();

    /***********************************************************************
     *
     * @param driver
     * @return
     */

    public PageLogoutPortal(WebDriver driver,SeleniumHelp sh,SeleniumUtil su,WebDriverWait wait,String d) throws IOException {
        this.driver = driver;
        this.WAIT=wait;
        sUtil=su;
        sHelp=sh;
        FIC_LOGS=d;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    //*************************************************************************
    @FindBy(css = "#navbarDropdownMenuLink")
    private WebElement ClickProfile;
        public void clickClickProfile() throws Exception{
        WAIT.until(ExpectedConditions.elementToBeClickable(ClickProfile));
        sHelp.waitEleLaEtAffiche(ClickProfile,3,3);
        ClickProfile.click();
        sUtil.tempo(0);
        ClickLogOut.click();
        sUtil.tempo(2);
    }
    //@FindBy(css = "li.nav-item:nth-child(3) > div:nth-child(2) > a:nth-child(2)")
    @FindBy(xpath = "//*[@id='navbarSupportedContent']/ul[2]/li[3]/div/a[2]")
    private WebElement ClickLogOut;
    public void clickClickLogOut() throws Exception{
        sUtil.tempo(3);
        ClickLogOut.click();
        sUtil.tempo(2);
    }

    @FindBy(css = ".bg-white > h1:nth-child(1)")
    private WebElement ScorPortal;

    @FindBy(css = ".swal2-confirm")
    private WebElement ClickConfirm;
    public void clickClickConfirm() throws Exception{
        sUtil.tempo(3);
        ClickConfirm.click();
        //WAIT.until(ExpectedConditions.textToBePresentInElement(ScorPortal,"SCOR PORTAL"));
        sUtil.tempo(4);

    }
}
