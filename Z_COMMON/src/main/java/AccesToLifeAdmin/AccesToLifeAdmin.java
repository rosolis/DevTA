package AccesToLifeAdmin;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class AccesToLifeAdmin {



    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;

    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;

    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    Properties TEST_CONFIG = new Properties();
    private String FILE_PARAM="";
    private int NUM_LIN_TEST=0;
    private boolean logsTrue = true;
    private FileAndLog sFlog = null;

    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String DIR_TS_PARAM=DIR_RUN;
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String GLOBAL_URL="02_LOG/";
    private String BROWSER_FROM_SYS="NO";
    private String BROWSER_FOR_ALL_TEST="";

    Properties SYS_CONFIG = new Properties();
    private String CONF_URL="http://dcvintlifeaas:8280/helios-la-front/#/login";

    private String LIST_ACTIONS[]=null;
    private List<String> LOG_TEST   =new ArrayList<>();

    private String TRANSACT_ID="";

    private String HANDLE_0="";
    private String HANDLE_1="";

    // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
    // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
    private Map<String, Object> DATA_UPLOADED = new HashMap<>();
    private LinkedHashMap<String, Object> MAP_TIME_LIGNE = new LinkedHashMap<>();
    //********************************************************
    //********************************************************
    public AccesToLifeAdmin( ) throws Exception {
        AccesToLifeAdminINIT();
    }

    public AccesToLifeAdmin(List<String> lo,WebDriver dr, Properties testConf,  WebDriverWait wt, SeleniumUtil sut,
                            SeleniumHelp sIni, LinkedHashMap<String, Object> t_li, List<String> st) throws Exception {
        LOG_TEST=lo;
        driver = dr;


        TEST_CONFIG = testConf;
        WAIT = wt;
        sUtil = sut;
        sHelp = sIni;
        MAP_TIME_LIGNE = t_li;
        LOG_TEST = st;
        AccesToLifeAdminINIT();
    }



    public void AccesToLifeAdminINIT() throws IOException {

        String dire_lancement = System.getProperty("user.dir");
        System.out.println("dire_lancement:" + dire_lancement);

        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        int deb = dire_lancement.lastIndexOf("0A_TS1_Portal");
        String sp=dire_lancement.substring(0,deb-1);
        if (isOperatingSystemWindows) {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        }
        System.out.println("Start= "+DIR_ROOT_TS);

        sFlog = new FileAndLog();
        sFlog.setLogs(logsTrue);
        sUtil= new SeleniumUtil();

        InputStream inputRoot = new FileInputStream(DIR_ROOT_TS+"00_SYS_CONFIG.txt");

        SYS_CONFIG.load(inputRoot);
        DIR_REPORT = SYS_CONFIG.getProperty("DIR_REPORTS");
        CONF_URL = SYS_CONFIG.getProperty("CNX_LOGIN_LIFEADMIN");
        DIR_JDD=SYS_CONFIG.getProperty("DIR_JDD");
        DIR_LOGS=SYS_CONFIG.getProperty("DIR_LOGS");
        DIR_DRIVER=SYS_CONFIG.getProperty("DIR_DRIVER");
        DIR_TS_PARAM=SYS_CONFIG.getProperty("DIR_TS_INTEGRATION");
        GLOBAL_URL=SYS_CONFIG.getProperty("SYS_CNX_LOGIN_PORTAL");
        BROWSER_FOR_ALL_TEST=SYS_CONFIG.getProperty("SYS_CNX_BROWSER");
        CONF_BROWSER=BROWSER_FOR_ALL_TEST.trim();
        DIR_RUN=DIR_ROOT_TS+DIR_RUN;

        String sF[]=FILE_PARAM.split("\\.");
        String sF2=sF[0].replace("\\","-");
        sF2=sF2.replace("/","-");

        DIR_LOGS=DIR_ROOT_TS+DIR_LOGS+sF2+ "_"+sFlog.logDate(4)+".log";
        sHelp = new SeleniumHelp(SYS_CONFIG,LOG_TEST);
        sHelp.setDIR_ROOT_TS(DIR_ROOT_TS);
        DIR_TS_PARAM=DIR_ROOT_TS+DIR_TS_PARAM;
    }

    //********************************************************

    //********************************************************
    public void setUp() throws Exception {

        String dire_lancement = System.getProperty("user.dir");



        LOG_TEST.add("START TEST  = " +"Open hElios LifeAdmin");

        LOG_TEST.add("Dir lancement ="+dire_lancement);
        driver = sHelp.openWebdriver(driver, CONF_BROWSER); // FIREFOX  CHROME IEXPLORER PHANTOMJS

        sFlog.Add_String("apres INIT =" + CONF_BROWSER, DIR_LOGS);
        LOG_TEST.add("INIT OK =" + CONF_URL );
        try {

            WAIT = new WebDriverWait(driver, 10);
            Dimension dime = new Dimension(1400, 900);
            driver.manage().window().setSize(dime);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

            LOG_TEST.add("TRY TO OPEN URL =" + CONF_URL );

            driver.get(CONF_URL);


            driver.findElement(By.xpath("/html/body/scor-root/scor-login/form/div[2]/button")).click();
            sUtil.tempo(3);
        } catch (Exception e) {
            e.printStackTrace();
            LOG_TEST.add("CRASH ");
            sHelp.screenShotCrash(driver);
        }

    }

    @SuppressWarnings("static-access")
    public void trtLifeAdminActions(Map<String, Object> d_upload,String acts) throws Exception {
        // d_upload =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
        DATA_UPLOADED=d_upload;
        TRANSACT_ID=(String)DATA_UPLOADED.get("TRANS_ID");
        LIST_ACTIONS=acts.split(":");
        PageAccesToLifeAdmin monoPage = new PageAccesToLifeAdmin(driver, sUtil,sHelp,WAIT, LOG_TEST);

        System.out.println("\n--->CALL TEST = ");
        try {

            for(String act : LIST_ACTIONS){
                String txtEvent="";
                act=act.trim();

                switch (act) {
                    case "FindSubmissionID": // FI
                        LOG_TEST.add("FIN & open TRANSACTION ID = "+TRANSACT_ID);
                        monoPage.FindSubmissionID(TRANSACT_ID);
                        break;
                    case "Indexation": // FI
                        LOG_TEST.add("Edit and Indexation ");
                        txtEvent=monoPage.EditSelDataGroup("3");
                        LOG_TEST.add("CALL CHECK EVENT = POR_009");
                        String stat=monoPage.EditIndexationAllSheets(DATA_UPLOADED);
                        if(stat.length()>1){
                            LOG_TEST.add(stat);
                        }
                        break;

                    case "POP1": // FI
                        JOptionPane jop1 = new JOptionPane();
                        jop1.showMessageDialog(null, "Clic OK pour Continuer",
                                "Tempo", JOptionPane.INFORMATION_MESSAGE);
                        break;
                    case "CLOSE": // FI
                        driver.close();
                        if (driver !=null)
                            driver.quit();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
//            int i=LOG_TEST.size();
//            String s=LOG_TEST.get(i-1);
//            s="Fail test when "+s;
//            LOG_TEST.set(i-1,s);
//            LOG_TEST.add("CRASH");
            if (driver !=null)
                driver.quit();
            sUtil.genExeption();
        }
        System.out.println("---> FIN TEST ");
    }
    //***************************************************


    //***************************************************


    public boolean logGetIfOK(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return false;
        else
            return true;
    }
    public String logGetLastAction(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return LOG_TEST.get(i-2);
        else
            return LOG_TEST.get(i-1);
    }

    public void setTansactID(String s){
        TRANSACT_ID=s;
    }

    /************************************************************************************
     * ********************   MAIN
     ************************************************************************************/

    public  static void main(String[] args) throws Exception{

        Map<String, Object>  D_UPLOADED = new HashMap<>();
        String  tagMail="";
        String  tagAction="";
        String fileParam="";


        if (args.length ==2) {
            System.out.println(args[0] +"+ "+args[1] );
            tagMail=args[0] ;
            tagAction=args[1] ;
            System.out.println(tagMail+"+ "+tagAction);
        }else{
//            D_UPLOADED.put("TRANS_ID","d5d81029-0b13-4033-a707-2e43f1e76b38");
//            D_UPLOADED.put("TYPE_BORD","Claims"); // Claims=4
//            D_UPLOADED.put("PERIOD_BORD","07/2019 - 08/2019");
//            D_UPLOADED.put("SENDER_BORD","cuser9 INT Tests");
//            D_UPLOADED.put("SENDING_DATE","17/06/2019 (1:02 PM)");
//            D_UPLOADED.put("GR_BORD","Aegon Group A");
//            D_UPLOADED.put("ORIGIN_BORD","Portal");
//            D_UPLOADED.put("NB_BORD","2");
            //252b0ba7-da47-46b2-8f5e-399dd4e2e239
            D_UPLOADED.put("TRANS_ID","0ae67405-84cb-48a5-9880-5a16525b4e60");

            D_UPLOADED.put("TYPE_BORD","Claims"); // Claims=4
            D_UPLOADED.put("PERIOD_BORD","8/2015 - 7/2017");
            D_UPLOADED.put("SENDER_BORD","TA CedentSuper-4");
            D_UPLOADED.put("SENDING_DATE","09/07/2019 (09:10 AM)");
            D_UPLOADED.put("GR_BORD","TA_GROUP");
            D_UPLOADED.put("ORIGIN_BORD","Portal");
            D_UPLOADED.put("NB_BORD","1");
            D_UPLOADED.put("FILES_LOADED","C:\\0A_TS1_Portal\\06_JDD\\00_BORD_EXCEL\\TA_DE_1subfile_01.xlsx");

            //tagAction ="FindTransID:WaitHML_015:CLOSE";
            //tagAction ="FindTransID:WaitPOR_009:CLOSE";
            tagAction ="FindSubmissionID:Indexation:CLOSE";
        }



        /*********  Construit le test ***********************
         *
         */
        AccesToLifeAdmin ta = new AccesToLifeAdmin();
        //ta.setTansactID("17e69c1b-a5d8-42c8-8f9d-ee9106a72dca");
        //ta.setSubject_Gr("Aegon");
        ta.setUp();

        ta.trtLifeAdminActions(D_UPLOADED,tagAction);;

        System.exit(0);
    }

}
