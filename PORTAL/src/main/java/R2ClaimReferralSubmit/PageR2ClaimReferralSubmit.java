package R2ClaimReferralSubmit;

import Helper.ClaimClientGenerator;
import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.RobotUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.*;
import java.util.*;
import java.util.List;


public class PageR2ClaimReferralSubmit {
    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private ClaimClientGenerator sGen =  new ClaimClientGenerator();

    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = new SeleniumHelp();
    private List<String> FILES_UPLOADED = new ArrayList<>();
    /***********************************************************************
     *
     * @param driver
     */

    public PageR2ClaimReferralSubmit(WebDriver driver,SeleniumHelp sh,SeleniumUtil su,WebDriverWait wait) throws IOException {
        this.driver = driver;
        sUtil=su;
        sHelp=sh;
        this.WAIT=wait;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    public List<String> getFILES_UPLOADED(){
        return FILES_UPLOADED;
    }
    //*************** ******************************************

    //***************  PageOject *************

    @FindBy(css = "button.btn:nth-child(1)")
    private WebElement ButtonAddDocu;
    public void clickButtonAddDocu() throws Exception{
        if (!ButtonAddDocu.isDisplayed()) sUtil.tempo(2);
        ButtonAddDocu.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "button.btn:nth-child(1)")
    private WebElement waitAddDoc;
    @FindBy(css = "div.show:nth-child(2) > a:nth-child(1)")
    private WebElement ChoiseBordereau;
    public void clickChoiseBordereau() throws Exception {
        WAIT.until(ExpectedConditions.textToBePresentInElement(waitAddDoc,"Add document"));
        ChoiseBordereau.click();
        //WAIT.until(ExpectedConditions.titleContains("Add - SCOR"));
        sUtil.tempo(2);
    }
    @FindBy(css = "div.show:nth-child(2) > a:nth-child(2)")
    private WebElement ChoiseClaimReferral;
    public void clickChoiseClaimReferral() throws Exception {
        WAIT.until(ExpectedConditions.textToBePresentInElement(waitAddDoc,"Add document"));
        ChoiseClaimReferral.click();
        //WAIT.until(ExpectedConditions.titleContains("Add - SCOR"));
        sUtil.tempo(2);
    }
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationPolicyNumber\"]")
    private WebElement PolicyNumber;
    public void fillPolicyNumber() throws Exception {
        String tmp=sGen.genPolicyNumber();
        PolicyNumber.sendKeys(tmp);
        sUtil.tempo(2);
    }

    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationScorClaimReference\"]")
    private WebElement ScorClaimRef;
    public void fillScorClaimRef() throws Exception {
        String tmp=sGen.genPolicyNumber();
        ScorClaimRef.sendKeys(tmp);
        sUtil.tempo(2);
    }
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationCedentClaimReference\"]")
    private WebElement CedentClaimRef;
    public void fillCedentClaimRef() throws Exception {
        String tmp=sGen.genPolicyNumber();
        CedentClaimRef.sendKeys("");
        CedentClaimRef.sendKeys(tmp);
        sUtil.tempo(2);
    }

    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationOverClaimAssessmentLimitSection\"]/label")
    private WebElement OverClaimAssessmentLimit;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationWithinClaimAssessmentLimitSection\"]/label")
    private WebElement WithinClaimAssessmentLimit;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationWithinClaimAssLimitScorAdviceRequiredSection\"]/label")
    private WebElement WithinClaimAssessmentlimitbutScorAdvicerequired;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimInformationOpinionOnlySection\"]/label")
    private WebElement OpinionOnly;
    public void fillClaimInfo() throws Exception {
        int n=sGen.genClaimInformation();
        switch (n){
            case 0:
                OverClaimAssessmentLimit.click();
                break;
            case 1:
                WithinClaimAssessmentLimit.click();
                break;
            case 2:
                WithinClaimAssessmentlimitbutScorAdvicerequired.click();
                break;
            default:
                OpinionOnly.click();
                break;
        }

        sUtil.tempo(2);
    }
    @FindBy(css = "#ClaimReferralAddClaimantDetailsSurname")
    private WebElement NameClaimant;
    @FindBy(xpath = "//*[@id=\"ClaimReferralAddClaimantDetailsDOB\"]")
    private WebElement Birth;
    @FindBy(xpath = "//*[@id=\"ClaimReferralAddClaimantDetailsSmokerStatus\"]")
    private WebElement Smoker;
    @FindBy(xpath = "//*[@id=\"ClaimReferralAddClaimantDetailsGender\"]")
    private WebElement Gender;
    @FindBy(xpath = "//*[@id=\"ClaimReferralAddClaimantDetailsOccupation\"]")
    private WebElement Occupation;
    @FindBy(xpath = "//*[@id=\"ClaimantDetailsSection\"]/a")
    private WebElement OpenCdetails;
    public void fillClaimantDetails() throws Exception {

        OpenCdetails.click();
        Long l=sHelp.waitEleLaEtAffiche(NameClaimant,4,4);
        if (l==0)
            l=90000+sHelp.waitEleLaEtAffiche(NameClaimant,3,2);
        System.out.println("long ="+l);
        //sUtil.tempo(4);

        Map<String,String> mDetails= sGen.genClaimantDetails();
        String tmp=mDetails.get("NAME");
        System.out.println("Name "+tmp);

        sHelp.JSScrollDown(driver);
        NameClaimant.sendKeys("");
        NameClaimant.sendKeys(tmp);
        sUtil.tempo(1);
        tmp=mDetails.get("BIRTH");
        Birth.sendKeys(tmp);
        Birth.sendKeys(Keys.TAB);
        sUtil.tempo(1);
        tmp=mDetails.get("SMOKER");
        Smoker.sendKeys(tmp);
        sUtil.tempo(1);

        Gender.sendKeys(mDetails.get("GENDER"));
        sUtil.tempo(1);
        Occupation.sendKeys(mDetails.get("OCCUPATION"));
        sUtil.tempo(1);
    }

    @FindBy(xpath = "//*[@id=\"PolicyDetailsSection\"]/a/h2")
    private WebElement OpenPolicyDetails;
    @FindBy(xpath = "//*[@id=\"ClaimReferralPolicyDetailBenefitType\"]")
    private WebElement PolicyDetailBenefitType;
    @FindBy(xpath = "//*[@id=\"ClaimReferralPolicyDetailClaimType\"]")
    private WebElement PolicyDetailClaimType;
    //*[@id="ClaimReferralPolicyDetailDateOfPolicy"]
    @FindBy(xpath = "//*[@id=\"ClaimReferralPolicyDetailDateOfPolicy\"]")
    private WebElement PolicyDetailDateOfPolicy;

    public void fillPolicyDetails() throws Exception {
        OpenPolicyDetails.click();
        sUtil.tempo(2);
        String tmp = sGen.randomBENEFIT_TYPE();
        PolicyDetailBenefitType.sendKeys(tmp);
        PolicyDetailClaimType.sendKeys(sGen.randomCLAIM_TYPE());
        PolicyDetailDateOfPolicy.sendKeys(sUtil.genRandomDatemmDDyyyy("/",1900,2005));
        PolicyDetailDateOfPolicy.sendKeys(Keys.TAB);


    }

    @FindBy(xpath = "//*[@id=\"ClaimsReferralClaimsDetailsSection\"]/a/h2")
    private WebElement OpenClaimDetails;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsCauseOfClaim\"]")
    private WebElement CauseOfClaim;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsDateOfClaim\"]")
    private WebElement DateOfClaim;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsTotalSumAssured_Input\"]")
    private WebElement TotalSumAssured;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsTotalSumAssuredCurrency\"]")
    private WebElement TotalSumCurrency;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsSCORSumAssured_Input\"]")
    private WebElement SCORSumAssured;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsSCORSumAssuredCurrency\"]")
    private WebElement ScorSumAssuredCurrency;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsSchemeName\"]")
    private WebElement SchemeName;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsSalary\"]")
    private WebElement Salary;

    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsDateJoinedEmployer\"]")
    private WebElement DateJoinedEmployer;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsDateJoinedScheme\"]")
    private WebElement DateJoinedScheme;
    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsRiskCommencement\"]")
    private WebElement RiskCommencement;

    @FindBy(xpath = "//*[@id=\"ClaimReferralClaimDetailsSubjectToUWFreeCover\"]")
    private WebElement SubjectToUWFreeCover;

    public void fillClaimDetails() throws Exception {
        OpenClaimDetails.click();
        sUtil.tempo(2);
        String tmp=sGen.randomCAUSE_OF_CLAIM();
        CauseOfClaim.sendKeys(tmp);
        tmp=sUtil.genRandomDatemmDDyyyy("/",2000,2010);
        DateOfClaim.sendKeys(tmp);
        DateOfClaim.sendKeys(Keys.TAB);
        tmp=sUtil.getStrRandomNum(800000,10000000,0);
        TotalSumAssured.sendKeys(tmp);
        TotalSumCurrency.sendKeys("EUR");
        tmp=sUtil.getStrRandomNum(50000,500000,0);
        SCORSumAssured.sendKeys(tmp);
        ScorSumAssuredCurrency.sendKeys("EUR");
        tmp=sGen.randomSCHEME_NAME();
        SchemeName.sendKeys(tmp);
        tmp=sUtil.getStrRandomNum(50000,300000,0);
        Salary.sendKeys(tmp);
        tmp=sUtil.genRandomDatemmDDyyyy("/",1900,2005);
        DateJoinedEmployer.sendKeys(tmp);
        DateJoinedEmployer.sendKeys(Keys.TAB);
        tmp=sUtil.genRandomDatemmDDyyyy("/",1900,2005);
        DateJoinedScheme.sendKeys(tmp);
        DateJoinedScheme.sendKeys(Keys.TAB);
        tmp= sGen.randomSubjectToUWFreeCover();
        SubjectToUWFreeCover.sendKeys(tmp);
    }

    @FindBy(xpath = "//*[@id=\"ClaimReferralIPTDPWOPSection\"]/a/h2")
    private WebElement OpenIPTDPWOPSection;
    @FindBy(xpath = "//*[@id=\"ClaimReferralIPTPDWOPDetailsDateFirstAbsentFromWork\"]")
    private WebElement DateFirstAbsentFromWork;
    @FindBy(xpath = "//*[@id=\"ClaimReferralIPTPDWOPDetailsReviewDateOfClaim\"]")
    private WebElement ReviewDateOfClaim;
    @FindBy(xpath = "//*[@id=\"ClaimReferralIPTPDWOPDetailsEscalationRate\"]")
    private WebElement EscalationRate;
    @FindBy(xpath = "//*[@id=\"ClaimReferralIPTPDWOPDetailsExpiryAge\"]")
        private WebElement DetailsExpiryAge;
    @FindBy(xpath = "//*[@id=\"ClaimReferralIPTPDWOPDetailsDeferredPeriod\"]")
    private WebElement DeferredPeriod;
    @FindBy(xpath = "//*[@id=\"Comments\"]")
    private WebElement Comments;


    public void fillIPTDPWOPSection() throws Exception {
        OpenIPTDPWOPSection.click();
        sUtil.tempo(2);
        DateFirstAbsentFromWork.sendKeys(sUtil.genRandomDatemmDDyyyy("/",2016,2018));
        DateFirstAbsentFromWork.sendKeys(Keys.TAB);
        ReviewDateOfClaim.sendKeys(sUtil.genRandomDatemmDDyyyy("/",2000,2018));
        ReviewDateOfClaim.sendKeys(Keys.TAB);
        DetailsExpiryAge.sendKeys("-100");
        EscalationRate.sendKeys("5%");
        DeferredPeriod.sendKeys("None");
        Comments.sendKeys("None");
    }

    @FindBy(xpath = "/html/body/div/main/form/div[2]/div/div[7]/div[1]/span/input")
    private WebElement uploadDropLocation;
    @FindBy(css = "#dropLocation > input:nth-child(2)")//*[@id="dropLocation"]    //*[@id="dropLocation"]/span
    private WebElement inputLocation;
    public List<String> clickDropLocation(List<String> list_Bord) throws Exception {
        String var = list_Bord.get(0);
        boolean vari = false;

        List<String> lst = new ArrayList<>();
        for (int i = 3; i < list_Bord.size(); i++) {
            lst.add(list_Bord.get(i));
        }
        int sendMAx = 0;
        if (var.contentEquals("VARIABLE")) {
            vari = true;
            sendMAx = sUtil.getIntRandomNum(0, lst.size(), 2);
        } else if (var.contains("VARIABLE_")) {
            vari = true;
            String sp[] = var.split("_");
            sendMAx = Integer.parseInt(sp[1]);
        } else
            sendMAx = lst.size();


        //List<String> lstSended=new ArrayList<>();
        String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
        ((JavascriptExecutor) driver).executeScript(js, inputLocation);

        boolean oui = true;
        int nbSend = 0;

        List<String> f_sended = new ArrayList<>();
        for (int i = 0; i < sendMAx; i++) {
            String filename = "";
            oui = false;
            if (vari) {
                int ivar = sUtil.getIntRandomNum(0, lst.size(), 2);
                filename = lst.get(ivar);
                lst.remove(ivar);
            } else
                filename = lst.get(i);
            System.out.println(filename);

            f_sended.add(filename);

            ((JavascriptExecutor) driver).executeScript(js, inputLocation);
            inputLocation.sendKeys(filename);
            Thread.sleep(1);
            FILES_UPLOADED.add(filename);
            nbSend++;
        }
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", inputLocation);
        WebDriverWait wait_load = new WebDriverWait(driver, 1000);
        for (int i = 0; i < nbSend; i++) {
            wait_load.until(ExpectedConditions.textToBePresentInElement(
                    (driver.findElement(By.cssSelector("ul.document:nth-child(" + (i + 1) + ") > li:nth-child(6)"))), "Completed"));
        }

        System.out.println(nbSend);
        return FILES_UPLOADED;
    }


    @FindBy(xpath = "//*[@id=\"#btnClaimSubmit\"]")
    private WebElement ButtonSubmiClaimRef;
    public void clickButtonSubmitClaimRef() throws Exception {

        sUtil.tempo(3);
        ButtonSubmiClaimRef.click();
        sUtil.tempo(2);

    }

    @FindBy(xpath = "/html/body/div[2]/div/div[3]/button[1]")
    private WebElement ConfirmClaimRef;
    public void clickConfirmClaimRef() throws Exception {
        WAIT.until(ExpectedConditions.elementToBeClickable(ConfirmClaimRef));
        ConfirmClaimRef.click();

        sUtil.tempo(2);
    }




}
