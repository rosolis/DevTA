package R2ClaimReferralSubmit;

import Bordereau_History_List.PageBordereau_History_List;
import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import SubmitBordereau.PageAddBordereaux;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.*;



public class R2ClaimReferralSubmit {

    Properties CONFIG = new Properties();
    Properties TEST_CONFIG = new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR = "";
    private String CONF_BROWSER = "FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME = "";
    private String TEST_PARAM = "";

    private List<String> LOG_TEST = null;

    private String DIR_PARAM = "";
    private String FIC_LOGS = DIR_PARAM + "";

    private LinkedHashMap<String, Object> MAP_TIME_LIGNE = new LinkedHashMap<>();
    private LinkedHashMap<String, Object> STATUS_TEST = new LinkedHashMap<>();

    private long MILLI_DEP = 0;
    private long MILLI_FIN = 0;
    private long MILLI_DIFF = 0;
    private long MILLI_TOT = 0;
    private List<String> FILES_UPLOADED = new ArrayList<>();

    // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE GR_BORD
    // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
    private Map<String, Object> DATA_UPLOADED = new HashMap<>();

    //*****************************************************************************************
    public R2ClaimReferralSubmit() throws IOException {
        ;

    }

    public R2ClaimReferralSubmit(
            WebDriver dr, Properties testConf,  WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String, Object> t_li, List<String> st) throws IOException {
        driver = dr;


        TEST_CONFIG = testConf;
        WAIT = wt;
        sUtil = sut;
        sHelp = sIni;
        MAP_TIME_LIGNE = t_li;
        LOG_TEST = st;
    }

    //*****************************************************************************************
    public void setUp() throws Exception {


    }

    public List<String> getFILES_UPLOADED(){
        return FILES_UPLOADED;
    }
    //********************************************************

    public Map<String, Object>  TestR2ClaimReferralSubmit() throws Exception {

        MILLI_DEP = System.currentTimeMillis();

        PageR2ClaimReferralSubmit pageAdd = new PageR2ClaimReferralSubmit(driver,sHelp,sUtil, WAIT);



        LOG_TEST.add(" Try to clickButtonAddDocu + Choise ClaimReferral  ");

        pageAdd.clickButtonAddDocu();
        pageAdd.clickChoiseClaimReferral();

        pageAdd.fillPolicyNumber();
        pageAdd.fillScorClaimRef();
        pageAdd.fillCedentClaimRef();
        pageAdd.fillClaimInfo();
        pageAdd.fillClaimantDetails();
        pageAdd.fillPolicyDetails();
        pageAdd.fillClaimDetails();
        pageAdd.fillIPTDPWOPSection();



        List<String> list_Bord = sHelp.getFileToSendFromJddBord(TEST_CONFIG);
        String sTest=list_Bord.get(0);


        boolean vari =false;
        if (sTest.contains("VARIABLE"))
            vari =true;


        driver = pageAdd.GetDriver();



        //sUtil.memoClipboard(f_Borde);
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PAGE", MILLI_DIFF);
        System.out.println("NAVIGATE " + MILLI_DIFF);
        LOG_TEST.add("Start Send bordereau ");

        MILLI_DEP = System.currentTimeMillis();
        LOG_TEST.add("Upload files ");
        FILES_UPLOADED= pageAdd.clickDropLocation(list_Bord);
        List<Long> TxtUpload =sUtil.getSizeFiles(FILES_UPLOADED);

        MAP_TIME_LIGNE.put("Nb FILE", TxtUpload.get(0));
        MAP_TIME_LIGNE.put("Nb MEGA", TxtUpload.get(1));
        LOG_TEST.add("Click submit bordereau");

        pageAdd.clickButtonSubmitClaimRef();
        LOG_TEST.add("Click confirm button");
        String dtSend=sUtil.genereDateLa(11);
        LOG_TEST.add("sending date ="+dtSend);

        pageAdd.clickConfirmClaimRef();
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;

        MAP_TIME_LIGNE.put("UPLOAD FILE", MILLI_DIFF);
        System.out.println("UPLOAD FILE" + MILLI_DIFF);

        return DATA_UPLOADED;
    }

    //********************************************************

    public void tearDown() {

        driver.quit();
    }

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        R2ClaimReferralSubmit logP = new R2ClaimReferralSubmit();
        logP.setUp();
        logP.TestR2ClaimReferralSubmit();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }


}

