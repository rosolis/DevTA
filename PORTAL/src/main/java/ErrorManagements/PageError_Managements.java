package ErrorManagements;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;

public class PageError_Managements {
    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = null;

    /***********************************************************************
     *
     * @param driver
     */

    public PageError_Managements(WebDriver driver, WebDriverWait wait) throws IOException {
        this.driver = driver;
        this.WAIT=wait;
        sHelp = new SeleniumHelp();
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    //*************** ******************************************
    //***************  PageOject *************

    @FindBy(css = "#navbarError")
    private WebElement TabErrorManagements;
    public void ClickTabErrorManagements() throws Exception {
        TabErrorManagements.click();
        sUtil.tempo(2);
    }


    @FindBy(css = "#navbarSupportedContent > ul:nth-child(1) > li:nth-child(2) > div > a:nth-child(1)")
    private WebElement SousTabErrorManag;
    public void ClickSousTabErrorManag() throws Exception {
        SousTabErrorManag.click();
        sUtil.tempo(3);
    }
    @FindBy(css = ".ag-body-container")
    private WebElement TabHistory;
    public void cptHistory() throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        int max = 3;
        if (optionCount.size()< max) max=optionCount.size();
        for (int i = 0; i < max; i++) {
            wHandl = driver.getWindowHandle();
            TabHistory.findElement(By.cssSelector("div:nth-child("+(i+1)+") > div:nth-child(1)")).click();
            int iHandle=0;
            String wHandl_last="";
            for(String winHandle : driver.getWindowHandles()){
                iHandle++;
                wHandl_last=winHandle;
            }
            if (iHandle>1){
                driver.switchTo().window(wHandl_last);
                sUtil.tempo(2);
                driver.close();
                driver.switchTo().window(wHandl);
            }
            sUtil.tempo(1);
        }
        sUtil.tempo(2);
    }

    @FindBy(css = "#borderLayout_eGridPanel > div.ag-bl-center.ag-bl-full-height-center > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none > div > span.ag-header-cell-text")
    private WebElement ColDate;
    public void sortByDate() throws Exception {
        ColDate.click();
        sUtil.tempo(3);
    }
    @FindBy(css = "#borderLayout_eGridPanel > div.ag-bl-center.ag-bl-full-height-center > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(2) > div.ag-cell-label-container.ag-header-cell-sorted-none > div > span.ag-header-cell-text")
    private WebElement ColDescription;
    public void sortByDescription() throws Exception {
        ColDescription.click();
        sUtil.tempo(3);
    }
    @FindBy(css = "#borderLayout_eGridPanel > div.ag-bl-center.ag-bl-full-height-center > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(3) > div.ag-cell-label-container.ag-header-cell-sorted-none > div > span.ag-header-cell-text")
    private WebElement ColDocument;
    public void sortByDocumentType() throws Exception {
        ColDocument.click();
        sUtil.tempo(3);
    }

    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[4]/div[2]")
    private WebElement ColStatus;
    public void sortByStatus() throws Exception {
        ColStatus.click();
        sUtil.tempo(3);
    }

    @FindBy(css = "#borderLayout_eGridPanel > div.ag-bl-center.ag-bl-full-height-center > div > div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(5) > div.ag-cell-label-container.ag-header-cell-sorted-none > div > span.ag-header-cell-text")
    private WebElement ColAssign;
    public void sortByAssign() throws Exception {
        ColAssign.click();
        sUtil.tempo(3);
    }

    @FindBy(css = "div.ag-header-cell:nth-child(4) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabSentBy_EP;
    public void sortBySentBy_EP() throws Exception {
        TabSentBy_EP.click();
        sUtil.tempo(2);
    }
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[4]/div[3]/div/div/div[1]/div[5]")
    private WebElement assignTo_UM;
    public void clickAssignTo_UM() throws Exception {
        assignTo_UM.click();
        sUtil.tempo(3);
    }

    @FindBy(xpath = "//*[@id=\"group_20_anchor\"]/i[1]")
    private WebElement Unselect_First_UM;
    public void clickUnselectFirst_UM() throws Exception {
        Unselect_First_UM.click();
        sUtil.tempo(2);
    }

    @FindBy(xpath = "//*[@id=\"errorAssignation\"]/div/div/div[1]/button")
    private WebElement CloseAssign;
    public void clickCloseAssign_UM() throws Exception {
        CloseAssign.click();
        sUtil.tempo(2);
    }
}
