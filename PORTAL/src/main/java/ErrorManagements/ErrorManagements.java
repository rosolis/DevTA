package ErrorManagements;



import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

public class ErrorManagements {
    Properties CONFIG = new Properties();
    Properties TEST_CONFIG = new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME="";
    private String TEST_PARAM="";
    private FileAndLog sFlog = null;
    private String DIR_PARAM="";
    private String FIC_LOGS="";

    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private List<String> LOG_TEST=null;

    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;

    //*****************************************************************************************
    public ErrorManagements() throws IOException {
        sFlog = new FileAndLog();

    }
    public ErrorManagements(
            WebDriver dr, String testName, String fLogs, Properties testConf,
            FileAndLog logs, WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String,Object> mtl, List<String> st) throws IOException {
        driver = dr;
        TEST_NAME=testName;
        FIC_LOGS=fLogs;
        TEST_CONFIG=testConf;
        sFlog = logs;
        WAIT = wt;
        sUtil= sut;
        sHelp = sIni;
        MAP_TIME_LIGNE=mtl;
        LOG_TEST=st;

    }
    //*****************************************************************************************
    public void setUp() throws Exception {



    }


    //********************************************************

    public long TestError_Managements() throws Exception{
        MILLI_DEP = System.currentTimeMillis();

        PageError_Managements pageH = new PageError_Managements(driver,WAIT);

        //try {
        LOG_TEST.add("Try to open Error Management Page    ");
        pageH.ClickTabErrorManagements();
        pageH.ClickSousTabErrorManag();


        //pageH.cptHistory();

        LOG_TEST.add("Try To Sort By Date  ");
        pageH.sortByDate();

        LOG_TEST.add("Try To Sort By Error Description  ");
        pageH.sortByDescription();
        LOG_TEST.add("Try To Sort By Document Type");
        pageH.sortByDocumentType();

        LOG_TEST.add("Try To Sort By Status ");
        pageH.sortByStatus();

        LOG_TEST.add("Try To Sort By Assign to   ");
        pageH.sortByAssign();


        boolean bAssign=false;
        for (String s :LOG_TEST){
            if (s.contains("11_ASSIGN_ERROR_TO_USER_or_GROUP")) {
                bAssign=true;
            }
        }
        if (bAssign){
            pageH.clickAssignTo_UM();

            pageH.clickUnselectFirst_UM();

            pageH.clickCloseAssign_UM();
        }
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PAGE", MILLI_DIFF);

        return MILLI_TOT;
    }

    public void crashTest(){
        int a=100/0;
    }
    //********************************************************

    public void tearDown() {
        if (driver!=null)
            driver.quit();
        System.out.println("End");
    }

    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        ErrorManagements logP = new ErrorManagements();
        logP.setUp();
        logP.TestError_Managements();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }

}
