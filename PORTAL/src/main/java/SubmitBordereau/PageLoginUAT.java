package SubmitBordereau;

import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageLoginUAT {
    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private String FIC_LOGS="";

    private SeleniumUtil sUtil = new SeleniumUtil();
    private FileAndLog sFlog = new FileAndLog();


    /***********************************************************************
     *
     * @param driver
     * @return 
     */

    public PageLoginUAT(WebDriver driver,WebDriverWait wait,String d) {
        this.driver = driver;
        this.WAIT=wait;
        FIC_LOGS=d;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    //*************************************************************************
    @FindBy(css = ".btn")
    private WebElement ButtonPrimaryCss;
    public void clickButtonPrimary() throws Exception{
        ButtonPrimaryCss.click();
        //WAIT.until(ExpectedConditions.titleContains("Connectez-vous"));
        sUtil.tempo(2);
    }
    @FindBy(css = "#idSIButton9")
    private WebElement SeconnecterSuivantCss;
    @FindBy(css = "#i0116")
    private WebElement SeConnecterCss;
    public void EnterIdVal(String id) throws Exception{

    	while (!SeConnecterCss.isDisplayed())
    	    sUtil.tempo(4);
    	
        SeConnecterCss.sendKeys(id);
        SeconnecterSuivantCss.click();
        //WAIT.until(ExpectedConditions.titleContains("Se connecter"));
        sUtil.tempo(1);
    }

    @FindBy(css = "#idSIButton9")
    private WebElement GoConnexion;
    @FindBy(css = "#i0118")
    private WebElement Pwd1Css ;
    public void EnterPwdVal(String id) throws Exception{
    //	while (sUtil.laEtAffiche(Pwd1Css))
    	sUtil.tempo(4);
        Pwd1Css.sendKeys(id);
        GoConnexion.click();
        sUtil.tempo(1);
        GoConnexion.click();
        //WAIT.until(ExpectedConditions.titleContains("Index - SCOR"));
        sUtil.tempo(3);
    }

    public void loginUAT(String s_login, String s_pass ) throws Exception {

        sFlog.Add_String("clickButtonPrimary  ",FIC_LOGS);
        clickButtonPrimary();
        sUtil.tempo(2);
        sFlog.Add_String("LOGIN  ",FIC_LOGS);
        //clickButtonPrimary();
        WAIT.until(ExpectedConditions.urlContains(""));
        sFlog.Add_String("User "+s_login,FIC_LOGS);
        EnterIdVal(s_login);
        sFlog.Add_String("Pass "+s_pass,FIC_LOGS);
        EnterPwdVal(s_pass);
    }
}
