package SubmitBordereau;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Scanner;

import Helper.SeleniumUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.RobotUtils;

public class PageAddBordereaux {
    private WebDriver driver;
    private WebDriverWait WAIT = null;


    private SeleniumUtil sUtil = new SeleniumUtil();
    private List<String> FILES_UPLOADED = new ArrayList<>();
    /***********************************************************************
     *
     * @param driver
     */

    public PageAddBordereaux(WebDriver driver,WebDriverWait wait) {
        this.driver = driver;
        this.WAIT=wait;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    public List<String> getFILES_UPLOADED(){
        return FILES_UPLOADED;
    }
    //*************** ******************************************

    //***************  PageOject *************

    @FindBy(css = "button.btn:nth-child(1)")
    private WebElement ButtonAddDocu;
    public void clickButtonAddDocu() throws Exception{
    	if (!ButtonAddDocu.isDisplayed()) sUtil.tempo(2);
        ButtonAddDocu.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "button.btn:nth-child(1)")
    private WebElement waitAddDoc;
    @FindBy(css = "div.show:nth-child(2) > a:nth-child(1)")
    private WebElement ChoiseBordereau;
    public void clickChoiseBordereau() throws Exception {
        WAIT.until(ExpectedConditions.textToBePresentInElement(waitAddDoc,"Add document"));
        ChoiseBordereau.click();
        //WAIT.until(ExpectedConditions.titleContains("Add - SCOR"));
        sUtil.tempo(2);
    }

    @FindBy(css = "#FromPeriod")
    private WebElement FromDate;
    public void SetFromDate(String da) throws Exception{
        sUtil.tempo(0);
        FromDate.sendKeys(da);
        sUtil.tempo(1);
    }
    @FindBy(css = "#ToPeriod")
    private WebElement ToDate;
    public void SetToDate(String da) throws Exception{
        sUtil.tempo(1);
        ToDate.sendKeys(da);
        sUtil.tempo(1);
    }


    @FindBy(css = "div.formcheck:nth-child(2) > label:nth-child(2)")
    private WebElement SummaryInfo;
    @FindBy(css = "div.formcheck:nth-child(3) > label:nth-child(2)")
    private WebElement ClaimsInfo;
    @FindBy(css = "div.formcheck:nth-child(4) > label:nth-child(2)")
    private WebElement PremiumInfo;
    @FindBy(css = "div.formcheck:nth-child(5) > label:nth-child(2)")
    private WebElement ComplementaryInfo;

    public void clickComplementaryInfo(String st) throws Exception{
        switch (st) {
            case "summary":
                SummaryInfo.click();
                break;
            case "claims":
                ClaimsInfo.click();
                break;
            case "premium":
                PremiumInfo.click();
                break;
            case "complementary information":
                ComplementaryInfo.click();
                break;

            default:
                ComplementaryInfo.click();
                    break;
        }

        sUtil.tempo(1);
    }

    @FindBy(css = "#Subject")
    private WebElement Subject;
    public void EnterSubject() throws Exception{
    String[] sub= {"TA Be yourself; everyone else is already taken. ― Oscar Wilde",
                "TA You can never be overdressed or overeducated",
                "TA I am so clever that sometimes I don't understand a single word of what I am saying.",
                "TA It is what you read when you don't have to that determines what you will be when you can't help it",
                "TA The truth is rarely pure and never simple. A good friend will always stab you in the front",
                "TA To live is the rarest thing in the world. Most people exist, that is all."
        };
        int n = sUtil.getIntRandomNum(0, 6, 2);
        Subject.sendKeys(sub[n]);
        sUtil.tempo(1);
    }

    @FindBy(css = "#Comment")
    private WebElement Comment ;
    public void EnterComment() throws Exception{
        Comment.sendKeys("");
        String[] comm= {"TA The books that the world calls immoral are books that show the world its own shame.― Oscar Wilde, The Picture of Dorian Gray",
                "TA Most people are other people. Their thoughts are someone else's opinions, their lives a mimicry, their passions a quotation.― Oscar Wilde",
                "TA If one cannot enjoy reading a book over and over again, there is no use in reading it at all. ― Oscar Wilde",
                "TA Those who find ugly meanings in beautiful things are corrupt without being charming. This is a fault. Those who find beautiful meanings in beautiful things are the cultivated. For these there is hope. They are the elect to whom beautiful things mean only Beauty. There is no such thing as a moral or an immoral book. Books are well written, or badly written. That is all.― Oscar Wilde",
                "TA You will always be fond of me. I represent to you all the sins you never had the courage to commit ― Oscar Wilde",
                "TA I am not young enough to know everything.The heart was made to be broken ― Oscar Wilde",
                "TA No better way is there to learn to love Nature than to understand Art. It dignifies every flower of the field. And, the boy who sees the thing of beauty which a bird on the wing becomes when transferred to wood or canvas will probably not throw the customary stone."


        };
        int n = sUtil.getIntRandomNum(0, 7, 2);
        Comment.sendKeys(comm[n]);

        sUtil.tempo(1);
    }

    @FindBy(xpath = "/html/body/div/main/form/div[2]/div/div[7]/div[1]/span/input")
    private WebElement uploadDropLocation;
    @FindBy(css = "#dropLocation")
    private WebElement DropLocation;

    @FindBy(css = "#dropLocation > input:nth-child(2)")
    private WebElement inputLocation;
    public List<String> clickDropLocation(List<String> list_Bord) throws Exception {
        String var = list_Bord.get(0);
        boolean vari = false;

        List<String> lst = new ArrayList<>();
        for (int i = 3; i < list_Bord.size(); i++) {
            lst.add(list_Bord.get(i));
        }
        int sendMAx = 0;
        if (var.contentEquals("VARIABLE")) {
            vari = true;
            sendMAx = sUtil.getIntRandomNum(0, lst.size(), 2);
        } else if (var.contains("VARIABLE_")) {
            vari = true;
            String sp[] = var.split("_");
            sendMAx = Integer.parseInt(sp[1]);
        } else
            sendMAx = lst.size();


        //List<String> lstSended=new ArrayList<>();
        String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
        ((JavascriptExecutor) driver).executeScript(js, inputLocation);

        boolean oui = true;
        int nbSend = 0;

        List<String> f_sended = new ArrayList<>();
        for (int i = 0; i < sendMAx; i++) {
            String filename = "";
            oui = false;
            if (vari) {
                int ivar = sUtil.getIntRandomNum(0, lst.size(), 2);
                filename = lst.get(ivar);
                lst.remove(ivar);
            } else
                filename = lst.get(i);
            System.out.println(filename);

            f_sended.add(filename);

            ((JavascriptExecutor) driver).executeScript(js, inputLocation);
            inputLocation.sendKeys(filename);
            Thread.sleep(1);
            FILES_UPLOADED.add(filename);
            nbSend++;
        }
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", inputLocation);
        WebDriverWait wait_load = new WebDriverWait(driver, 1000);
        for (int i = 0; i < nbSend; i++) {
            wait_load.until(ExpectedConditions.textToBePresentInElement(
                    (driver.findElement(By.cssSelector("ul.document:nth-child(" + (i + 1) + ") > li:nth-child(6)"))), "Completed"));
        }

        System.out.println(nbSend);
        return FILES_UPLOADED;
    }
    @FindBy(css = "ul.document > li:nth-child(6)")
    private WebElement waitCompleted;
    @FindBy(css = "#submitForm")
    private WebElement SubmitBord;
    public void clickSubmitBord() throws Exception {

        sUtil.tempo(3);
        SubmitBord.click();
        sUtil.tempo(2);

    }
    @FindBy(css = ".swal2-confirm")
    private WebElement okPerformed;
    @FindBy(css = ".swal2-confirm")
    private WebElement SubmitConfirm;
    public void clickSubmitConfirm() throws Exception {
        WAIT.until(ExpectedConditions.elementToBeClickable(SubmitConfirm));
        SubmitConfirm.click();
        WAIT.until(ExpectedConditions.elementToBeClickable(okPerformed));
        okPerformed.click();

        Thread.sleep(5);
    }

    //*************** *************

    public void JavascriptDragAndDrop(String dragSimuJs,WebElement source, WebElement target) throws FileNotFoundException {
        String script = new Scanner(new File(dragSimuJs)).useDelimiter("\\A").next();
        script += "simulateHTML5DragAndDrop(arguments[0], arguments[1])";
        //((JavascriptExecutor) driver).executeScript(
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript(script, source, target);
    }
    public static String convertFileToBase64String(String fileName) throws IOException {

        File file = new File(fileName);
        int length = (int) file.length();
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[length];
        reader.read(bytes, 0, length);
        reader.close();
        String encodedFile = Base64.getEncoder().encodeToString(bytes);

        return encodedFile;
    }
    public void dropActioCain(){
        WebElement eleW=driver.findElement(By.cssSelector("#dropLocation"));

        //ActionChains(driver).move_to_element(eleW).key_down(Keys.CONTROL).send_keys('c').key_up(Keys.CONTROL).perform();
    }
    public void dropJs(String fileRec) {
        String id = "dropLocation";
        String fileName = "MonFichier.csv";
        String base64IFile = null;
        try {
            base64IFile = convertFileToBase64String(fileRec);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ((JavascriptExecutor) driver).executeScript("var myZone = Dropzone.forElement('#" + id + "');" +
                "base64Image = '" + base64IFile + "';" +
                "function base64toBlob(b64Data, contentType, sliceSize) {  \n" +
                "    contentType = contentType || '';\n" +
                "    sliceSize = sliceSize || 512;\n" +
                "\n" +
                "    var byteCharacters = atob(b64Data);\n" +
                "    var byteArrays = [];\n" +
                "\n" +
                "    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {\n" +
                "        var slice = byteCharacters.slice(offset, offset + sliceSize);\n" +
                "\n" +
                "        var byteNumbers = new Array(slice.length);\n" +
                "        for (var i = 0; i < slice.length; i++) {\n" +
                "            byteNumbers[i] = slice.charCodeAt(i);\n" +
                "        }\n" +
                "\n" +
                "        var byteArray = new Uint8Array(byteNumbers);\n" +
                "\n" +
                "        byteArrays.push(byteArray);\n" +
                "    }\n" +
                "\n" +
                "    var blob = new Blob(byteArrays, {type: contentType});\n" +
                "    return blob;\n" +
                "}" +
                "var blob = base64toBlob(base64Image, 'image / png');" +
                "blob.name = '" + fileName + "';" +
                "myZone.addFile(blob);  "
        );
    }
    public void zUploadFile (String filePath) throws  AWTException, InterruptedException {
        RobotUtils rb= new RobotUtils();
        // Put path to your image in a clipboard
        StringSelection ss = new StringSelection(filePath);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        // OR use java robot for entire filepath
        Thread.sleep(20);

        // Imitate mouse events like ENTER, CTRL+C, CTRL+V
        rb.typeKeyPress("CTRL_V");
        Thread.sleep(20);

        rb.typeKeyPress("ENTER");
    }

}
