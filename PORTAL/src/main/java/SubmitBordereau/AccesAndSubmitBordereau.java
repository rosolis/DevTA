package SubmitBordereau;

import java.io.IOException;
import java.util.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Bordereau_History_List.PageBordereau_History_List;
import Helper.SeleniumHelp;
import Helper.SeleniumUtil;

public class AccesAndSubmitBordereau {

    Properties CONFIG = new Properties();
    Properties TEST_CONFIG = new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR = "";
    private String CONF_BROWSER = "FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME = "";
    private String TEST_PARAM = "";

    private List<String> LOG_TEST = null;

    private String DIR_PARAM = "";
    private String FIC_LOGS = DIR_PARAM + "";

    private LinkedHashMap<String, Object> MAP_TIME_LIGNE = new LinkedHashMap<>();
    private LinkedHashMap<String, Object> STATUS_TEST = new LinkedHashMap<>();

    private long MILLI_DEP = 0;
    private long MILLI_FIN = 0;
    private long MILLI_DIFF = 0;
    private long MILLI_TOT = 0;
    private List<String> FILES_UPLOADED = new ArrayList<>();

    // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE GR_BORD
    // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
    private Map<String, Object>  DATA_UPLOADED = new HashMap<>();

    //*****************************************************************************************
    public AccesAndSubmitBordereau() throws IOException {
        ;

    }

    public AccesAndSubmitBordereau(
            WebDriver dr, Properties testConf,  WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String, Object> t_li, List<String> st) throws IOException {
        driver = dr;


        TEST_CONFIG = testConf;
        WAIT = wt;
        sUtil = sut;
        sHelp = sIni;
        MAP_TIME_LIGNE = t_li;
        LOG_TEST = st;
    }

    //*****************************************************************************************
    public void setUp() throws Exception {


    }

    public List<String> getFILES_UPLOADED(){
        return FILES_UPLOADED;
}
    //********************************************************

    public Map<String, Object>  TestAccesAndSubmitBordereau() throws Exception {

        MILLI_DEP = System.currentTimeMillis();

        PageAddBordereaux pageAdd = new PageAddBordereaux(driver, WAIT);



        LOG_TEST.add(" Try to clickButtonAddDocu + Choise Bordereau  ");

        pageAdd.clickButtonAddDocu();
        pageAdd.clickChoiseBordereau();


        List<String> list_Bord = sHelp.getFileToSendFromJddBord(TEST_CONFIG);
        String sTest=list_Bord.get(0);
        boolean vari =false;
        if (sTest.contains("VARIABLE"))
            vari =true;

        LOG_TEST.add(" Enter field From date To Date ");
        String periodFrom="";
        String periodTo="";
        if (vari) {
            List<String> dates_gen = sUtil.genPeriodFromDateTo();
            periodFrom=dates_gen.get(0);
            periodTo=dates_gen.get(1);
            pageAdd.SetFromDate(periodFrom);
            pageAdd.SetToDate(periodTo);
        } else {
            
            String s1[] = list_Bord.get(2).split("-");
            String s2 = s1[0].trim();
            if (s2.length() == 6) s2 = "0" + s2;
            periodFrom=s2;
            pageAdd.SetFromDate(periodFrom);
            s2 = s1[1].trim();
            if (s2.length() == 6) s2 = "0" + s2;
            periodTo=s2;
            pageAdd.SetToDate(periodTo);
        }
        LOG_TEST.add("periodFrom ="+periodFrom);
        LOG_TEST.add("periodTo ="+periodTo);

        LOG_TEST.add("Try to enter ComplementaryInfo ");
        int nComment = sUtil.getIntRandomNum(0, 4, 2);
        String sV="";
        if (vari) {
            switch (nComment) {
                case 0:
                    sV="summary";
                    break;
                case 1:
                    sV="claims";
                    break;
                case 2:
                    sV="premium";
                    break;
                case 3:
                    sV="complementary information";
                    break;

                default:
                    sV="claims";
                    break;
            }
            pageAdd.clickComplementaryInfo(sV);
        } else {
        	sV=list_Bord.get(1);
            String i1[] = sV.split(",");
            for (String s : i1) {
                pageAdd.clickComplementaryInfo(s.trim().toLowerCase());
            }
        }
        LOG_TEST.add("Type ="+sV);
        LOG_TEST.add("Enter subject and comment  ");
        pageAdd.EnterSubject();
        pageAdd.EnterComment();

        driver = pageAdd.GetDriver();



        //sUtil.memoClipboard(f_Borde);
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PAGE", MILLI_DIFF);
        System.out.println("NAVIGATE " + MILLI_DIFF);
        LOG_TEST.add("Start Send bordereau ");

        MILLI_DEP = System.currentTimeMillis();
        LOG_TEST.add("Upload files ");
        FILES_UPLOADED= pageAdd.clickDropLocation(list_Bord);
        List<Long> TxtUpload =sUtil.getSizeFiles(FILES_UPLOADED);

        MAP_TIME_LIGNE.put("Nb FILE", TxtUpload.get(0));
        MAP_TIME_LIGNE.put("Nb MEGA", TxtUpload.get(1));
        LOG_TEST.add("Click submit bordereau");

        pageAdd.clickSubmitBord();
        LOG_TEST.add("Click confirm button");
        String dtSend=sUtil.genereDateLa(11);
        LOG_TEST.add("sending date ="+dtSend);

        pageAdd.clickSubmitConfirm();
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;

        MAP_TIME_LIGNE.put("UPLOAD FILE", MILLI_DIFF);
        System.out.println("UPLOAD FILE" + MILLI_DIFF);
        
        PageBordereau_History_List pbl=new PageBordereau_History_List(driver,WAIT);
        //MAP ="TRANS_ID","TYPE_BORD","PERIOD_BORD","SENDER_BORD","USER_BORD","ORIGIN_BORD","NB_BORD","SENDING_DATE";

        DATA_UPLOADED= pbl.getTransactionId_FromPage(1, "");
        // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE USER_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT CNX_TA_USER
        DATA_UPLOADED.put("CNX_TA_USER",TEST_CONFIG.getProperty("CNX_TA_USER").trim());
        String filesAll="";
        String scomple="";
        String fu="";
        String add="";
        for (String s: FILES_UPLOADED) {
            if(fu.length()>2)
                fu += ";"+s;
            else
                fu = s;
        }
        DATA_UPLOADED.put("FILES_LOADED",fu);

        DATA_UPLOADED.put("MILLI_TOT",MILLI_TOT);
        LOG_TEST.add("Submit Transaction Id ="+ DATA_UPLOADED.get("TRANS_ID"));
        // clefs =TRANS_ID TYPE_BORD PERIOD_BORD SENDER_BORD SENDING_DATE GR_BORD
        // ORIGIN_BORD NB_BORD FILES_LOADED MILLI_TOT
        System.out.println(DATA_UPLOADED.get("TRANS_ID"));
        System.out.println(DATA_UPLOADED.get("PERIOD_BORD"));
        System.out.println(DATA_UPLOADED.get("SENDING_DATE"));
        System.out.println(DATA_UPLOADED.get("GR_BORD"));
        System.out.println(DATA_UPLOADED.get("FILES_LOADED"));
        System.out.println("End Submit");

    return DATA_UPLOADED;
    }

    //********************************************************

    public void tearDown() {

        driver.quit();
    }

	/********************************************************************************************************
	 * MAIN
	 * @throws Exception 
	 * 
	 * @throws IOException

	 * 
	 ********************************************************************************************************/
	public static void main(String[] args) throws Exception  {
		// Instance
        AccesAndSubmitBordereau logP = new AccesAndSubmitBordereau();
		logP.setUp();
		logP.TestAccesAndSubmitBordereau();
		logP.tearDown();
		System.out.println("===FIN=== ");
	}
    
    
}
