package AccessToHistory;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;


public class PageAccessToHistory {
    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = null;
    List<String> LOG_TEST;
    /***********************************************************************
     *
     * @param driver
     */

    public PageAccessToHistory(WebDriver driver, WebDriverWait wait,List<String> st) throws IOException {
        this.driver = driver;
        this.WAIT=wait;
        sHelp = new SeleniumHelp();
        LOG_TEST=st;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    //*************** ******************************************
    //***************  PageOject *************

    @FindBy(css = "div.col-md-12:nth-child(3) > ul:nth-child(1) > li:nth-child(2)")
    private WebElement TabOtherRequest;
    public void ClickOtherRequest() throws Exception {
        TabOtherRequest.click();
        sUtil.tempo(2);
    }


    @FindBy(css = "div.col-md-12:nth-child(3) > ul:nth-child(1) > li:nth-child(1)")
    private WebElement TabBackBordereau;
    public void ClickBackBordereaux() throws Exception {
        TabBackBordereau.click();
        sUtil.tempo(2);
    }
    @FindBy(css = ".ag-body-container")
    private WebElement TabHistory;
    public void cptHistory() throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        int max = 3;
        if (optionCount.size()< max) max=optionCount.size();
        for (int i = 0; i < max; i++) {
            wHandl = driver.getWindowHandle();
            TabHistory.findElement(By.cssSelector("div:nth-child("+(i+1)+") > div:nth-child(1)")).click();
            int iHandle=0;
            String wHandl_last="";
            for(String winHandle : driver.getWindowHandles()){
                iHandle++;
                wHandl_last=winHandle;
            }
            if (iHandle>1){
                driver.switchTo().window(wHandl_last);
                sUtil.tempo(2);
                driver.close();
                driver.switchTo().window(wHandl);
            }
            sUtil.tempo(1);
        }
        sUtil.tempo(2);
    }

    @FindBy(css = "div.ag-header-cell:nth-child(1) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabType;
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[4]/div[3]/div/div/div[1]/div[1]")
    private WebElement readType_AV;
    public void sortByType() throws Exception {
        String s1=readType_AV.getText();
        TabType.click();
        sUtil.tempo(3);
        String s2=readType_AV.getText();
        if (!s2.contains("Claims"))
            sUtil.genExeption();
    }

    @FindBy(css = "div.ag-header-cell:nth-child(2) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabPeriod;
    public void sortByPeriod() throws Exception {
        TabPeriod.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(2) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabSendingDates;
    public void sortBySendingDates() throws Exception {
        TabSendingDates.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(6) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabSentBy;
    public void sortBySentBy() throws Exception {
        TabSentBy.click();
        sUtil.tempo(2);
    }
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[1]/div/div/span[1]")
    private WebElement TabSendingDates_EP;
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[4]/div[3]/div/div/div[1]/div[1]")
    private WebElement readSendingDates_AVep;

    public void sortBySendingDates_EP() throws Exception {
        String s1=readSendingDates_AVep.getText();
        TabSendingDates_EP.click();
        sUtil.tempo(3);
        String s2=readSendingDates_AVep.getText();
        if(s1.contains(s2))
            sUtil.genExeption();
    }
    @FindBy(css = "div.ag-header-cell:nth-child(4) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabSentBy_EP;
    public void sortBySentBy_EP() throws Exception {
        TabSentBy_EP.click();
        sUtil.tempo(2);
    }
    @FindBy(xpath = "//*[@id=\"navbarDropdown\"]")
    private WebElement openPageManag;
    @FindBy(xpath = "//*[@id=\"navbarSupportedContent\"]/ul[1]/li[4]/div/a[3]")
    private WebElement cedentUserPageManag;
    public void openPageManagement() throws Exception {
        openPageManag.click();
        sUtil.tempo(0);
        cedentUserPageManag.click();
        sUtil.tempo(3);
    }

    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[4]/div[3]/div/div/div[1]/div[1]")
    private WebElement readEmailAd_UM;
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[1]/div[2]/div/span[1]")
    private WebElement sortEmailAd_UM;
    public void sortByEmailAddress_UM() throws Exception {
        String strAvant=readEmailAd_UM.getText();
        sortEmailAd_UM.click();
        sUtil.tempo(3);
        String strApres=readEmailAd_UM.getText();
 //       if (strAvant.contains(strApres))
 //           sUtil.genExeption();
    }

    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[2]/div[2]/div/span[1]")
    private WebElement readDisplay_UM;
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[2]/div[2]/div/span[1]")
    private WebElement sortDisplayName_UM;
    public void sortByDisplayName_UM() throws Exception {
        String strAvant=sortDisplayName_UM.getText();
        sortDisplayName_UM.click();
        sUtil.tempo(3);
        String strApres=sortDisplayName_UM.getText();
        if (strAvant.contains(strApres))
            sUtil.genExeption();
    }

    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[3]/div[2]/div/span[1]")
    private WebElement readRole_UM;
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[3]/div[2]/div/span[1]")
    private WebElement sortRole_UM;
    public void sortByRole_UM() throws Exception {
        String strAvant=sortRole_UM.getText();
        sortRole_UM.click();

        sUtil.tempo(3);
        String strApres=sortRole_UM.getText();
        if (strAvant.contains(strApres))
            sUtil.genExeption();
    }

    @FindBy(xpath = "")
    private WebElement readStatus_UM;
    @FindBy(xpath = "//*[@id=\"borderLayout_eGridPanel\"]/div[1]/div/div[1]/div[3]/div/div/div[4]/div[2]/div/span[1]")
    private WebElement sortStatus_UM;
    public void sortByStatus_UM() throws Exception {
        sortStatus_UM.click();
        sUtil.tempo(2);
    }

    @FindBy(xpath = "//*[@id=\"UsrName\"]")
    private WebElement setSearchUser_UM;
    public void setBySearchUser_UM() throws Exception {
        setSearchUser_UM.sendKeys("TA CEDENT");
        sUtil.tempo(2);
    }
    @FindBy(xpath = "//*[@id=\"formSearchUser\"]/div/div/button/i")
    private WebElement clickSetSearchUser_UM;
    public void clicksetBySearchUser_UM() throws Exception {
        clickSetSearchUser_UM.click();
        sUtil.tempo(2);
    }
}
