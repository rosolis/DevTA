package AccessToHistory;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

public class AccessToHistory {
    Properties CONFIG = new Properties();
    Properties TEST_CONFIG = new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME="AccessToHistory";
    private String TEST_PARAM="01_AccessToHistory_PARAM.txt";
    private FileAndLog sFlog = null;
    private String DIR_PARAM="";
    private String FIC_LOGS="";

    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private List<String> LOG_TEST=null;

    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;

    //*****************************************************************************************
    public AccessToHistory() throws IOException {
        sFlog = new FileAndLog();

    }
    public AccessToHistory(
            WebDriver dr, String testName, String fLogs, Properties testConf,
            FileAndLog logs, WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String,Object> mtl, List<String> st) throws IOException {
            driver = dr;
            TEST_NAME=testName;
            FIC_LOGS=fLogs;
            TEST_CONFIG=testConf;
            sFlog = logs;
            WAIT = wt;
            sUtil= sut;
            sHelp = sIni;
        MAP_TIME_LIGNE=mtl;
        LOG_TEST=st;

    }
    //*****************************************************************************************
    public void setUp() throws Exception {



    }


    //********************************************************

    public long TestAccesToHistory() throws Exception{
        MILLI_DEP = System.currentTimeMillis();

        PageAccessToHistory pageH = new PageAccessToHistory(driver,WAIT,LOG_TEST);

        //try {
        LOG_TEST.add("Try to click line and open 3 pages from  BORDEREAUX    ");
        pageH.cptHistory();
        boolean bCedent=true;
        boolean bScorAd=false;
        boolean bExternamPa=false;
        for (String s :LOG_TEST){
            if (s.contains("TaExternalPartner")) {
                bCedent=false;
                bExternamPa=true;
            }
            if (s.contains("@scor.com")) {
                bScorAd=true;
            }
        }

        if (bCedent || bScorAd){
            LOG_TEST.add("Try To Sort By Type  ");
            pageH.sortByType();

            LOG_TEST.add("Try To Sort By Period  ");
            pageH.sortByPeriod();
            LOG_TEST.add("Try To Sort By SendingDates  ");
            pageH.sortBySendingDates();

            LOG_TEST.add("Try To Sort By SentBy   ");
            pageH.sortBySentBy();
        }
        if (bExternamPa) {
            LOG_TEST.add("Try To Sort By SendingDates  ");
            pageH.sortBySendingDates_EP();

            LOG_TEST.add("Try To Sort By SentBy   ");
            pageH.sortBySentBy_EP();
        }
        if (bScorAd){
            LOG_TEST.add("Try To Open Page USER MANAGEMENT ");
            pageH.openPageManagement();
            LOG_TEST.add("Try To Sort By Email Address   ");
            pageH.sortByEmailAddress_UM();
            LOG_TEST.add("Try To Sort By DISPLAY NAME   ");
            pageH.sortByDisplayName_UM();

            LOG_TEST.add("Try To Sort By ROLE");
            pageH.sortByRole_UM();
            LOG_TEST.add("Try To Sort By Status");
            pageH.sortByStatus_UM();
            LOG_TEST.add("Try To Search user by name");
            pageH.setBySearchUser_UM();
        }
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PAGE", MILLI_DIFF);


        return MILLI_TOT;
    }

    public void crashTest(){
        int a=100/0;
    }
    //********************************************************

    public void tearDown() {
    if (driver!=null)
        driver.quit();
    System.out.println("End");
    }

    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        AccessToHistory logP = new AccessToHistory();
        logP.setUp();
        logP.TestAccesToHistory();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }

}
