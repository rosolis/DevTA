package ActiveMq_Dms_Check;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PageActiveMq_Dms_Check {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = null;
    private FileAndLog sFlog = null;
    private String FIC_LOGS = "";
    private List<WebElement> TR_ROWS = new ArrayList<>();
    private List<WebElement> TR_ROWS_HML = new ArrayList<>();
    private int TR_COUNT = 0;

    /***********************************************************************
     *
     * @param driver
     */

    public PageActiveMq_Dms_Check(WebDriver driver, WebDriverWait wait) throws IOException {
        this.driver = driver;
        this.WAIT = wait;
        PageFactory.initElements(this.driver, this);
    }

    public WebDriver GetDriver() {
        return this.driver;
    }

    public void setShelp(SeleniumHelp sH) {
        sHelp = sH;
    }

    public void setSflog(FileAndLog sF, String s) {
        sFlog = sF;
        FIC_LOGS = s;
    }

    //*************** ******************************************
    //***************  PageOject *************

    @FindBy(css = ".example-container > table:nth-child(1) > tbody:nth-child(2)")
    private WebElement TableBody;


    public int getAllTrEle() throws Exception {
        sUtil.tempo(4);
        TR_ROWS = TableBody.findElements(By.cssSelector(".example-element-row"));
        TR_COUNT = TR_ROWS.size();
        return TR_COUNT;
    }

    public String geRowCedantNb(int readLigne) {
        WebElement curRow = TR_ROWS.get(readLigne - 1);
        List<WebElement> TDcount = curRow.findElements(By.tagName("td"));
        String sCedent = TDcount.get(1).getText();
        System.out.println("Cedant Number = " + sCedent + " row =" + readLigne);
        return sCedent;
    }


    @FindBy(xpath = "//*[@id=\"mat-input-0\"]")
    private WebElement IdFilter;

    public void setIdFilter(String filter) {
        IdFilter.sendKeys(filter);
        IdFilter.sendKeys(Keys.RETURN);
    }

    public List<String> getIDRowNb(int readLigne) {
        List<String> lRet = new ArrayList<>();
        WebElement curRow = TR_ROWS.get(readLigne - 1);
        List<WebElement> TDcount = curRow.findElements(By.tagName("td"));
        lRet.add(TDcount.get(0).getText());
        lRet.add(TDcount.get(1).getText());
        lRet.add(TDcount.get(2).getText());


        System.out.println("date lue = " + lRet + " row =" + readLigne);
        return lRet;
    }

    //@FindBy(css = "body > app-root > main > app-transactions-page > app-transactions-table > div > table > tbody > tr:nth-child(4) > td > div > app-message-events-table > table > tbody")
    @FindBy(css = ".example-detail-row > table > tbody")
    private WebElement TableHmlBody;

    public int findHml_015(int lineResult) throws Exception {

        WebElement curRow = TR_ROWS.get(lineResult - 1);
        List<WebElement> TDcount = curRow.findElements(By.tagName("td"));
        WebElement eLe = TDcount.get(2);
        eLe.click();
        sUtil.tempo(3);

        int result = 0;
        TR_ROWS = TableBody.findElements(By.cssSelector(".example-element-row"));

        List<WebElement> TDetail = TableBody.findElements(By.cssSelector(".example-detail-row"));
        curRow = TDetail.get(lineResult - 1);
        TR_ROWS_HML = curRow.findElements(By.cssSelector(".element-row"));
        List<String> lEve = new ArrayList<>();
        for (WebElement ele : TR_ROWS_HML) {
            List<WebElement> EVcount = ele.findElements(By.tagName("td"));
            WebElement we1 = EVcount.get(0);
            WebElement we2 = EVcount.get(1);
            WebElement we3 = EVcount.get(2);
            String sEve = we3.getText();
            lEve.add(sEve);
            String s1 = we1.getText();
            System.out.println("-> " + we1.getText());
            System.out.println("-> " + we2.getText());
            System.out.println("-> " + we3.getText());

            if (sEve.contains("HML_015"))
                result = lineResult;

        }
        if (lEve.contains("HML_014")) {
            System.out.println("-> ");
            result = lineResult;

        }

        return result;
    }
}

