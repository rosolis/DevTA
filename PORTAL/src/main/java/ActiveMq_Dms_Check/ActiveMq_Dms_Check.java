package ActiveMq_Dms_Check;


import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.*;

public class ActiveMq_Dms_Check {

    Properties CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME= "Bordereau_History_List";
    private String TEST_PARAM="03_ConsultHistoryLine_PARAM.txt";
    private FileAndLog sFlog = null;
    private String TOPIC_URL="http://dcvintlifeatl:8180/helios-fit-monitoring-front/#/transactions";
    //private String CEDENT_NUMBER="40637";
    private String CEDENT_NUMBER="56006";
    private String TRANSACTIO_ID="";

    private int IMAX_LINES_CEDENT_SEARCH_=4;
    private int IMAX_REFRESH_NUMBER=15;
    private int IMAX_REFRESH_TROUVE=10;
    private int MINUTES_PLUS=2;  // Date to send + MINUTES_PLUS=

    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;

    private String DIR_PARAM="";
    private String FIC_LOGS=DIR_PARAM+"02_LOG\\";
    private List<String> FILES_UPLOADED = new ArrayList<>();
    //*****************************************************************************************
    public ActiveMq_Dms_Check() throws IOException {
        sFlog = new FileAndLog();
        sHelp= new SeleniumHelp();
        sUtil=new SeleniumUtil();

    }
    public ActiveMq_Dms_Check(
            WebDriver dr, String testName, String fLogs, Properties testConf, FileAndLog logs, WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String,Object> t_li, List<String> files_uploaded) throws IOException {
        driver = dr;
        TEST_NAME=testName;
        FIC_LOGS=fLogs;
        TEST_CONFIG=testConf;
        sFlog = logs;
        WAIT = wt;
        sUtil= sut;
        sHelp = sIni;
        MAP_TIME_LIGNE=t_li;
        FILES_UPLOADED=files_uploaded;
    }
    //*****************************************************************************************
    public void setUp() throws Exception {

        sFlog.Add_String("START TEST  = " +TEST_NAME,FIC_LOGS);
        driver = sHelp.simplyOpenWebdriver(driver, "CHROME"); // F

    }

    //********************************************************

    public Long TestActiveMqTopic(String dateSend) throws Exception{
        MILLI_DEP = System.currentTimeMillis();

        //String Topic_Url=TEST_CONFIG.getProperty("URL_TOPIC");
        //String CEDENT_NUMBER=TEST_CONFIG.getProperty("CEDENT_NUMBER");


        System.out.println("Sending Date = "+dateSend);
        PageActiveMq_Dms_Check pageH = new PageActiveMq_Dms_Check(driver,WAIT);
        //driver.get(Topic_Url);

        Date dtToSend=sUtil.timeConvertStringTopicToDate(dateSend);
        //dtToSend =sUtil.timeAddMinToDate(dtToSend,-2);
        Date dtPlus=sUtil.timeAddMinToDate(dtToSend,MINUTES_PLUS);

        pageH.setShelp(sHelp);
        pageH.setSflog(sFlog,FIC_LOGS);

        int lineResult=0;
        driver.get(TOPIC_URL);
        sUtil.tempo(4);
        while (IMAX_REFRESH_NUMBER>0){

            driver.navigate().refresh();
            int nbLinesH= pageH.getAllTrEle();

            if (nbLinesH<IMAX_LINES_CEDENT_SEARCH_)
                IMAX_LINES_CEDENT_SEARCH_=nbLinesH;
            int iNbTry=5;
            int readLigne=1;
            boolean bFound=false;
            boolean bEnded=false;
            boolean bCedentFound=false;
            List<String>cdat =new ArrayList<>();
            while (!bEnded){

                if(TRANSACTIO_ID.length()>2){
                    pageH.setIdFilter(TRANSACTIO_ID);
                    nbLinesH= pageH.getAllTrEle();
                }
                //String Cedent_Nb=pageH.geRowCedantNb(readLigne);
                cdat=pageH.getIDRowNb(readLigne);
                String Cedent_Nb=cdat.get(1);
                if (Cedent_Nb.contains(CEDENT_NUMBER)){
                    lineResult=readLigne;
                    bCedentFound=true;
                }
                else{
                    readLigne++;
                    if (readLigne>IMAX_LINES_CEDENT_SEARCH_)
                        bEnded=true;
                }
                if (bCedentFound) {
                    String dateFound=cdat.get(2);
                    Date DateTopic=sUtil.timeConvertStringTopicToDate(dateFound);
                    int i=sUtil.timeFourchette(DateTopic,dtToSend,dtPlus);
                    if (i ==0){
                        bEnded=true;
                        lineResult=readLigne;
                        if(TRANSACTIO_ID.length()<1){
                            TRANSACTIO_ID=cdat.get(0);
                            IMAX_REFRESH_NUMBER=IMAX_REFRESH_TROUVE;
                        }
                    }
                    else{
                        readLigne++;
                        if (readLigne>IMAX_LINES_CEDENT_SEARCH_)
                            bEnded=true;
                    }
                }

            }
            IMAX_REFRESH_NUMBER--;
            if (lineResult>0){
                lineResult=pageH.findHml_015(lineResult);
            }
            if (lineResult>0) {
                bEnded = true;
                IMAX_REFRESH_NUMBER = 0;
            }
        }
        if (lineResult==0)
            sUtil.genExeption();


        //sUtil.memoClipboard(f_Borde);
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN-MILLI_DEP;
        MILLI_TOT=MILLI_TOT+MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PROCESS = ",MILLI_DIFF);
        System.out.println("NAVIGATE PROCESS = "+MILLI_DIFF);

    return MILLI_TOT;

    }
    //********************************************************

    public void tearDown() {
        if (driver!=null)
            driver.quit();
        System.out.println("End");
    }

    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        ActiveMq_Dms_Check logP = new ActiveMq_Dms_Check();
        logP.setUp();
        logP.TestActiveMqTopic("03/05/2019 08:51:23");
        logP.tearDown();
        System.out.println("===FIN=== ");
    }

}
