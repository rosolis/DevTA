package Export_Bordereau_History_List;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;

public class PageExportBordereau_History_List {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = null;
    private FileAndLog sFlog = null;
    private String FIC_LOGS="";
    /***********************************************************************
     *
     * @param driver
     */

    public PageExportBordereau_History_List(WebDriver driver, WebDriverWait wait) throws IOException {
        this.driver = driver;
        this.WAIT=wait;

        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    public void setShelp(SeleniumHelp sH) {sHelp=sH; }
    public void setSflog(FileAndLog sF, String s) {sFlog=sF; FIC_LOGS=s;}

    //*************** ******************************************
    //***************  PageOject *************
    @FindBy(css = ".ag-body-container")
    private WebElement CountTabHistory;
    public int countHistoryEle() throws Exception {
        List<WebElement> optionCount = CountTabHistory.findElements(By.cssSelector(".ag-row"));
        sUtil.tempo(0);
        return optionCount.size();
    }

    @FindBy(css = "button.ag-paging-button:nth-child(6)")
    private WebElement ButtonToLastPage;
    public boolean ifExisteLastPage(){

        return sHelp.estLaEtAffiche(ButtonToLastPage);
    }
    public void clickLastPage() throws Exception {
        ButtonToLastPage.click();
        sUtil.tempo(2);
    }
    @FindBy(css = ".ag-body-container")
    private WebElement AccesTabHistory;
    public void exportBordereauList(int nbLinesH) throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        ButtonExport.click();

        sUtil.tempo(2);
    }

    @FindBy(css = "button.btn:nth-child(2) > span:nth-child(1)")
    private WebElement ButtonExport;


    @FindBy(css = ".ag-body-container")
    private WebElement TabHistory;
    public void cptHistory() throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        int max = 8;
        if (optionCount.size()< max) max=optionCount.size();
        for (int i = 0; i < max; i++) {
            wHandl = driver.getWindowHandle();

            String nameH= TabHistory.findElement(By.cssSelector("div:nth-child("+(i+1)+") > div:nth-child(1)")).getText();

            TabHistory.findElement(By.cssSelector("div:nth-child("+(i+1)+") > div:nth-child(1)")).click();
            int iHandle=0;
            for(String winHandle : driver.getWindowHandles()){
                iHandle++;
            }
            if (iHandle>1){
                driver.switchTo().window(wHandl); }
            sUtil.tempo(2);
        }
        sUtil.tempo(2);
    }

}
