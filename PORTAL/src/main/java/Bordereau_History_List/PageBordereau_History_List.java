package Bordereau_History_List;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageBordereau_History_List {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = null;
    private FileAndLog sFlog = null;
    private String FIC_LOGS="";
    /***********************************************************************
     *
     * @param driver
     */

    public PageBordereau_History_List(WebDriver driver, WebDriverWait wait) throws IOException {
        this.driver = driver;
        this.WAIT=wait;

        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    public void setShelp(SeleniumHelp sH) {sHelp=sH; }
    public void setSflog(FileAndLog sF, String s) {sFlog=sF; FIC_LOGS=s;}

    //*************** ******************************************
    //***************  PageOject *************
    @FindBy(css = ".ag-body-container")
    private WebElement CountTabHistory;
    public int countHistoryEle() throws Exception {
        List<WebElement> optionCount = CountTabHistory.findElements(By.cssSelector(".ag-row"));
        sUtil.tempo(0);
        return optionCount.size();
    }

    @FindBy(css = "button.ag-paging-button:nth-child(6)")
    private WebElement ButtonToLastPage;
    public boolean ifExisteLastPage(){

        return sHelp.estLaEtAffiche(ButtonToLastPage);
    }
    public void clickLastPage() throws Exception {
        ButtonToLastPage.click();
        sUtil.tempo(2);
    }
    @FindBy(css = ".ag-body-container")
    private WebElement AccesTabHistory;
    public void accesLineHistory(int from, int max) throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        if (from==0)from++;
        if ((from) > max) max=from;

        for (int i = from; i <=max; i++) {
            if (i>3){
                WebElement we=AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(1)" ));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", we);
            }
            sUtil.tempo(0);
            wHandl = driver.getWindowHandle();

            String oType   = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(1)" )).getText();
            String oPeriod   = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(2)" )).getText();
            String oNumber = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i )+") > div:nth-child(3)" )).getText();
            String oSending = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i )+") > div:nth-child(4)" )).getText();
            String oOrigin = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(5)" )).getText();
            String oSendBy = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(6)" )).getText();

            String oUserG = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(7)" )).getText();

            AccesTabHistory.findElement(By.cssSelector("div:nth-child(" + (i) + ") > div:nth-child(1)")).click();
            sUtil.tempo(2);
            String wHandl_1 = driver.getWindowHandle();
            int iHandle = 0;
            for (String winHandle : driver.getWindowHandles()) {
                iHandle++;
                wHandl_1 = winHandle;
            }
            sUtil.tempo(2);
            driver.switchTo().window(wHandl_1);

            String dType = driver.findElement(By.cssSelector("div.col-md-4:nth-child(1) > div:nth-child(2)")).getText();
            String dPeriodF = driver.findElement(By.cssSelector("div.col-md-3:nth-child(2) > div:nth-child(2)")).getText();
            String dPeriodT = driver.findElement(By.cssSelector(".col-md-2 > div:nth-child(2)")).getText();
            String dSendingD = driver.findElement(By.cssSelector("div.col-md-4:nth-child(8) > div:nth-child(1) > span:nth-child(2)")).getText();
            String dOrigin = driver.findElement(By.cssSelector("div.col-md-12:nth-child(4) > div:nth-child(3) > div:nth-child(2)")).getText();
            String dSentBy = driver.findElement(By.cssSelector("div.col-md-3:nth-child(6) > div:nth-child(1) > span:nth-child(2)")).getText();
            String dUserG = driver.findElement(By.cssSelector(".word-wrap")).getText();
            sUtil.tempo(0);
            String[] tm = oPeriod.split("-");
            List<WebElement> docCount = driver.findElements(By.cssSelector(".document"));
            int iDocCount=docCount.size();
            String dNumber=Integer.toString(iDocCount);
            String oPeriodF = tm[0].trim();
            if (oPeriodF.length() == 6) oPeriodF = "0" + oPeriodF;
            String oPeriodT = tm[1].trim();
            if (oPeriodT.length() == 6) oPeriodT = "0" + oPeriodT;
            dSendingD = dSendingD.substring(0, 10);
            String source = oType + ";" + oPeriodF + ";" + oPeriodT + ";" + oNumber + ";" + oSending + ";" +oOrigin + ";" + oSendBy + ";" + oUserG;
            sFlog.Add_String("CMP String Source Line="+source,FIC_LOGS);
            String dest = dType + ";" + dPeriodF + ";" + dPeriodT + ";" + dNumber+ ";" + dSendingD + ";" + dOrigin + ";" + dSentBy + ";" + dUserG;
            sFlog.Add_String("CMP String Target Line="+dest,FIC_LOGS);
            driver.close();
            driver.switchTo().window(wHandl);
            sUtil.tempo(2);
        }


        sUtil.tempo(2);
    }


    @FindBy(css = ".ag-body-container")
    private WebElement AccesTabOthHistory;
    public void accesOthLineHistory(int from, int max) throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        if (from==0)from++;
        if ((from) > max) max=from;

        for (int i = from; i <=max; i++) {
            if (i>3){
                WebElement we=AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(1)" ));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", we);
            }
            sUtil.tempo(0);
            wHandl = driver.getWindowHandle();

            String oSendingDate   = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(1)" )).getText();
            String oNumberDoc   = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i)+") > div:nth-child(3)" )).getText();
            String oSentBy = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (i )+") > div:nth-child(4)" )).getText();

            AccesTabHistory.findElement(By.cssSelector("div:nth-child(" + (i) + ") > div:nth-child(1)")).click();
            sUtil.tempo(2);
            String wHandl_1 = driver.getWindowHandle();
            int iHandle = 0;
            for (String winHandle : driver.getWindowHandles()) {
                iHandle++;
                wHandl_1 = winHandle;
            }
            sUtil.tempo(2);
            driver.switchTo().window(wHandl_1);

            String dSendingDate = driver.findElement(By.cssSelector("div.col-md-4:nth-child(5) > div:nth-child(1) > span:nth-child(2)")).getText();

            String dSentBy = driver.findElement(By.cssSelector(".col-md-3 > div:nth-child(1) > span:nth-child(2)")).getText();

            sUtil.tempo(0);

            driver.close();
            driver.switchTo().window(wHandl);
            sUtil.tempo(2);
        }


        sUtil.tempo(2);
    }

    @FindBy(css = ".ag-body-container")
    private WebElement TabHistory;
    public void cptHistory() throws Exception {

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        int max = 8;
        if (optionCount.size()< max) max=optionCount.size();
        for (int i = 0; i < max; i++) {
            wHandl = driver.getWindowHandle();

            String nameH= TabHistory.findElement(By.cssSelector("div:nth-child("+(i+1)+") > div:nth-child(1)")).getText();

            TabHistory.findElement(By.cssSelector("div:nth-child("+(i+1)+") > div:nth-child(1)")).click();
            int iHandle=0;
            for(String winHandle : driver.getWindowHandles()){
                iHandle++;
            }
            if (iHandle>1){
                driver.switchTo().window(wHandl); }
            sUtil.tempo(2);
        }
        sUtil.tempo(2);
    }
    public Map getTransactionId_FromPage(int nbLi, String fic) throws Exception {
        Map<String, String> retMap = new HashMap<>();

        List<WebElement> optionCount = TabHistory.findElements(By.cssSelector(".ag-row"));
        String wHandl = driver.getWindowHandle();
        sUtil.tempo(0);
        if (nbLi==0)
        	nbLi++;

        sUtil.tempo(0);
        wHandl = driver.getWindowHandle();

        String oType   = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi)+") > div:nth-child(1)" )).getText();
        String oPeriod   = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi)+") > div:nth-child(2)" )).getText();
        String oNumber = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi)+") > div:nth-child(3)" )).getText();
        String oSending = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi )+") > div:nth-child(4)" )).getText();
        String oOrigin = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi)+") > div:nth-child(5)" )).getText();
        String oSendBy = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi)+") > div:nth-child(6)" )).getText();

        String oUserG = AccesTabHistory.findElement(By.cssSelector(".ag-body-container > div:nth-child(" + (nbLi)+") > div:nth-child(7)" )).getText();

        AccesTabHistory.findElement(By.cssSelector("div:nth-child(" + (nbLi) + ") > div:nth-child(1)")).click();
        sUtil.tempo(2);
        String wHandl_1 = driver.getWindowHandle();
        int iHandle = 0;
        for (String winHandle : driver.getWindowHandles()) {
            iHandle++;
            wHandl_1 = winHandle;
        }
        sUtil.tempo(2);
        driver.switchTo().window(wHandl_1);
        sUtil.tempo(2);

        String tipSpl[]=driver.getCurrentUrl().split("transactionId=");
        String trans_Id=tipSpl[1];
        String dType = driver.findElement(By.cssSelector("div.col-md-4:nth-child(1) > div:nth-child(2)")).getText();
        String dPeriodF = driver.findElement(By.cssSelector("div.col-md-3:nth-child(2) > div:nth-child(2)")).getText();
        String dPeriodT = driver.findElement(By.cssSelector(".col-md-2 > div:nth-child(2)")).getText();
        String dSendingD = driver.findElement(By.cssSelector("div.col-md-4:nth-child(8) > div:nth-child(1) > span:nth-child(2)")).getText();
        String dOrigin = driver.findElement(By.cssSelector("div.col-md-12:nth-child(4) > div:nth-child(3) > div:nth-child(2)")).getText();
        String dSentBy = driver.findElement(By.cssSelector("div.col-md-3:nth-child(6) > div:nth-child(1) > span:nth-child(2)")).getText();
        String dUserG = driver.findElement(By.cssSelector(".word-wrap")).getText();

        driver.close();
        driver.switchTo().window(wHandl);
        sUtil.tempo(2);
        retMap.put("TRANS_ID",trans_Id);
        retMap.put("TYPE_BORD",oType);
        retMap.put("PERIOD_BORD",oPeriod);
        retMap.put("SENDER_BORD",oSendBy);
        retMap.put("SENDING_DATE",dSendingD);
        retMap.put("GR_BORD",oUserG);
        retMap.put("ORIGIN_BORD",oOrigin);
        retMap.put("NB_BORD",oNumber);
        return retMap;
    }

}
