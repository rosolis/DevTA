package Bordereau_History_List;


import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Properties;

public class Bordereau_History_List {

    Properties CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME= "Bordereau_History_List";
    private String TEST_PARAM="03_ConsultHistoryLine_PARAM.txt";
    private FileAndLog sFlog = null;

    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;

    private String DIR_PARAM="";
    private String FIC_LOGS=DIR_PARAM+"02_LOG\\";
    //*****************************************************************************************
    public Bordereau_History_List() throws IOException {
        sFlog = new FileAndLog();

    }
    public Bordereau_History_List(
            WebDriver dr, String testName, String fLogs, Properties testConf, FileAndLog logs, WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni,LinkedHashMap<String,Object> t_li) throws IOException {
        driver = dr;
        TEST_NAME=testName;
        FIC_LOGS=fLogs;
        TEST_CONFIG=testConf;
        sFlog = logs;
        WAIT = wt;
        sUtil= sut;
        sHelp = sIni;
        MAP_TIME_LIGNE=t_li;
    }
    //*****************************************************************************************
    public void setUp() throws Exception {

        sFlog.Add_String("START TEST  = " +TEST_NAME,FIC_LOGS);


    }

    //********************************************************

    public Long TestConsultHistoryLine() throws Exception{
        MILLI_DEP = System.currentTimeMillis();

        PageBordereau_History_List pageH = new PageBordereau_History_List(driver,WAIT);
        pageH.setShelp(sHelp);
        pageH.setSflog(sFlog,FIC_LOGS);
        sFlog.Add_String("Access to Historic OK  ",FIC_LOGS);
        sFlog.Add_String("Count number of history lines",FIC_LOGS);

        int nbLinesH= pageH.countHistoryEle();

        sFlog.Add_String("Number of history lines first page = "+nbLinesH,FIC_LOGS);
        int clickLines=5;
        sFlog.Add_String("Access to "+clickLines+" first lines from history",FIC_LOGS);
        if (nbLinesH>0){
            if (nbLinesH >clickLines)
                pageH.accesLineHistory(1,clickLines);
            else
                pageH.accesLineHistory(1,nbLinesH);
            sFlog.Add_String("Access to "+clickLines+" last lines from history",FIC_LOGS);
            // acces to last pages
            if (pageH.ifExisteLastPage()){
                // click last page
                pageH.clickLastPage();
                nbLinesH= pageH.countHistoryEle();
                sFlog.Add_String("Number of history lines last page = "+nbLinesH,FIC_LOGS);
                if (nbLinesH >clickLines)
                    pageH.accesLineHistory(nbLinesH-(clickLines-1),nbLinesH);
                else
                    pageH.accesLineHistory(1,nbLinesH);
            }

        }else
            sFlog.Add_String("EMPTY EMPTY ->HISTORY LINES ARE EMPTY ",FIC_LOGS);


        sFlog.Add_String("Back to HOME page and End Test  ",FIC_LOGS);
        //sUtil.memoClipboard(f_Borde);
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN-MILLI_DEP;
        MILLI_TOT=MILLI_TOT+MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PROCESS = ",MILLI_DIFF);
        System.out.println("NAVIGATE PROCESS = "+MILLI_DIFF);

    return MILLI_TOT;

    }
    //********************************************************

    public void tearDown() {
        if (driver!=null)
            driver.quit();
        System.out.println("End");
    }

    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        Bordereau_History_List logP = new Bordereau_History_List();
        logP.setUp();
        logP.TestConsultHistoryLine();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }

}
