package R2ClaimReferralFilterAndHistory;


        import Helper.SeleniumHelp;
        import Helper.SeleniumUtil;
        import Utils.FileAndLog;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.support.ui.WebDriverWait;

        import java.io.IOException;
        import java.util.LinkedHashMap;
        import java.util.List;
        import java.util.Properties;

public class R2ClaimReferralFilterAndHistory {

    Properties CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME= "Bordereau_History_List";
    private String TEST_PARAM="03_ConsultHistoryLine_PARAM.txt";


    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;

    private String DIR_PARAM="";

    private List<String> LOG_TEST = null;
    //*****************************************************************************************
    public R2ClaimReferralFilterAndHistory() throws IOException {
        ;
    }
    public R2ClaimReferralFilterAndHistory(
            WebDriver dr, Properties testConf,  WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String, Object> t_li, List<String> st) throws IOException {
        driver = dr;


        TEST_CONFIG = testConf;
        WAIT = wt;
        sUtil = sut;
        sHelp = sIni;
        MAP_TIME_LIGNE = t_li;
        LOG_TEST = st;
    }
    //*****************************************************************************************
    public void setUp() throws Exception {
        ;
    }

    //********************************************************

    public Long TestR2ClaimReferralFilterAndHistory() throws Exception{
        MILLI_DEP = System.currentTimeMillis();

        PageR2ClaimReferralFilterAndHistory pageH = new PageR2ClaimReferralFilterAndHistory(driver,WAIT);

        pageH.setShelp(sHelp);
        pageH.setLOG_TEST(LOG_TEST);

        LOG_TEST.add("Test R2 Claim Referral Filter And History ");
        pageH.goPageClaimReferral();



        int nbLinesH= pageH.countHistoryEle();

        LOG_TEST.add("Number of history lines first page = "+nbLinesH);
        int clickLines=5;
        LOG_TEST.add("Access to "+clickLines+" first lines from history");
        if (nbLinesH>0){
            if (nbLinesH >clickLines)
                pageH.accesLineHistory(1,clickLines);
            else
                pageH.accesLineHistory(1,nbLinesH);
            LOG_TEST.add("Access to "+clickLines+" last lines from history");
            // acces to last pages
            if (pageH.ifExisteLastPage()){
                // click last page
                pageH.clickLastPage();
                nbLinesH= pageH.countHistoryEle();
                LOG_TEST.add("Number of history lines last page = "+nbLinesH);
                if (nbLinesH >clickLines)
                    pageH.accesLineHistory(nbLinesH-(clickLines-1),nbLinesH);
                else
                    pageH.accesLineHistory(1,nbLinesH);
            }

        }else
            LOG_TEST.add("EMPTY EMPTY ->HISTORY LINES ARE EMPTY ");


        LOG_TEST.add("Back to HOME page and End Test  ");
        //sUtil.memoClipboard(f_Borde);
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN-MILLI_DEP;
        MILLI_TOT=MILLI_TOT+MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PROCESS = ",MILLI_DIFF);
        System.out.println("NAVIGATE PROCESS = "+MILLI_DIFF);

        return MILLI_TOT;

    }
    //********************************************************

    public void tearDown() {
        if (driver!=null)
            driver.quit();
        System.out.println("End");
    }

    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        R2ClaimReferralFilterAndHistory logP = new R2ClaimReferralFilterAndHistory();
        logP.setUp();
        logP.TestR2ClaimReferralFilterAndHistory();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }

}
