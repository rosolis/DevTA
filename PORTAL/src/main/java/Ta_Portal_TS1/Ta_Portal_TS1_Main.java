package Ta_Portal_TS1;

import AccessToHistory.AccessToHistory;
import ActiveMq_Dms_Check.ActiveMq_Dms_Check;
import ActiveMq_Negative_Checks.ActiveMq_Negative_Check;
import Bordereau_History_List.Bordereau_History_List;
import CommonWeb.PageLoginPortal;
import CommonWeb.PageLogoutPortal;
import DisplayFilterResults.DisplayFilterResults;
import ErrorManagements.ErrorManagements;
import Export_Bordereau_History_List.Export_Bordereau_History_List;
import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Other_Req_History_List.Other_Req_History_List;
import R2ClaimReferralFilterAndHistory.R2ClaimReferralFilterAndHistory;
import R2ClaimReferralSubmit.R2ClaimReferralSubmit;
import SubmitBordereau.AccesAndSubmitBordereau;
import SubmitOtherRequest.AccesSubmitOtherRequest;
import Utils.ExcelRead;
import Utils.FileAndLog;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Ta_Portal_TS1_Main {

    Properties SYS_CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_URL="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME="SubmitBordereau";
    private String FILE_PARAM="";
    private int NUM_LIN_TEST=0;
    private boolean logsTrue = true;
    private FileAndLog sFlog = null;



    private String DIR_ROOT_TS="/0A_TS1_Portal/";

    private String DIR_RUN="00_RUN/";
    private String DIR_TS_PARAM=DIR_RUN;
    private String DIR_REPORT="05_REPORTS/";
    private String DIR_JDD="06_JDD/";
    private String DIR_LOGS="07_LOG/";
    private String DIR_DRIVER="08_DRIVERS/";
    private String GLOBAL_URL="02_LOG/";
    private String BROWSER_FROM_SYS="NO";
    private String BROWSER_FOR_ALL_TEST="";

    private String EXCEL_FILE="TS01_PORTAL_REPORT_V01.xlsx";
    private String EXCEL_FEUILLE  ="PORTAL_TESTS";
    private ExcelRead EXC = new ExcelRead();

    private List<List<Object>> TIME_RAPPORT= new ArrayList<>();
    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private String STATUS_TXT="NOT_OK";
    private String STATUS_NOTES="_";
    private List<String> LOG_TEST   =new ArrayList<>();

    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;



    //********************************************************
    //********************************************************


    public Ta_Portal_TS1_Main() throws IOException {
        String os = System.getProperty("os.name").toLowerCase();

        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        System.out.println("user home =");
        if (isOperatingSystemWindows) {

            DIR_ROOT_TS="C:"+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=dire_home+DIR_ROOT_TS;
        }
    }
    public Ta_Portal_TS1_Main(String nomTest, String fileParam) throws Exception {
        String dire_lancement = System.getProperty("user.dir");
        System.out.println("dire_lancement:" + dire_lancement);

        String os = System.getProperty("os.name").toLowerCase();
        boolean isOperatingSystemWindows = os.contains("win");
        String dire_home = System.getProperty("user.home");
        int deb = dire_lancement.lastIndexOf("0A_TS1_Portal");
        String sp=dire_lancement.substring(0,deb-1);
        if (isOperatingSystemWindows) {
            //L:\0A_TS1_Portal\00_RUN\xxxx
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        } else {
            DIR_ROOT_TS=sp+DIR_ROOT_TS;
        }
        System.out.println("Start= "+DIR_ROOT_TS);
        // id0611033960 fileparam non defined then ramdom test
        if (fileParam.length()<1){
            System.out.println("FILE PARAMETER ERROR");
            System.exit(0);
        }else{
            TEST_NAME=nomTest;
            FILE_PARAM=fileParam;
        }
        TEST_NAME=nomTest;
        FILE_PARAM=fileParam;
        sFlog = new FileAndLog();
        sFlog.setLogs(logsTrue);
        sUtil= new SeleniumUtil();

        InputStream inputRoot = new FileInputStream(DIR_ROOT_TS+"00_SYS_CONFIG.txt");

        SYS_CONFIG.load(inputRoot);
        DIR_REPORT = SYS_CONFIG.getProperty("DIR_REPORTS");
        EXCEL_FILE = SYS_CONFIG.getProperty("EXCEL_REPORT");
        DIR_JDD=SYS_CONFIG.getProperty("DIR_JDD");
        DIR_LOGS=SYS_CONFIG.getProperty("DIR_LOGS");
        DIR_DRIVER=SYS_CONFIG.getProperty("DIR_DRIVER");
        DIR_TS_PARAM=SYS_CONFIG.getProperty("DIR_TS_PORTAL");
        GLOBAL_URL=SYS_CONFIG.getProperty("SYS_CNX_LOGIN_PORTAL");
        BROWSER_FROM_SYS=SYS_CONFIG.getProperty("BROWSER_ALL");
        BROWSER_FOR_ALL_TEST=SYS_CONFIG.getProperty("SYS_CNX_BROWSER");
        DIR_RUN=DIR_ROOT_TS+DIR_RUN;

        String sF[]=FILE_PARAM.split("\\.");
        String sF2=sF[0].replace("\\","-");
        sF2=sF2.replace("/","-");
        //DIR_LOGS=DIR_ROOT_TS+DIR_LOGS+TEST_NAME+ sFlog.logDate(2)+".log";
        DIR_LOGS=DIR_ROOT_TS+DIR_LOGS+sF2+ "_"+sFlog.logDate(4)+".log";
        sHelp = new SeleniumHelp(SYS_CONFIG,LOG_TEST);
        sHelp.setDIR_ROOT_TS(DIR_ROOT_TS);
        DIR_TS_PARAM=DIR_ROOT_TS+DIR_TS_PARAM;
        EXCEL_FILE=DIR_RUN+EXCEL_FILE;


    }

    //********************************************************

    //********************************************************
    public boolean setUp(String testName, String testParam) throws Exception {
        String dire_lancement = System.getProperty("user.dir");
        sFlog.Add_String("START TEST  = " +TEST_NAME,DIR_LOGS);


        InputStream input = new FileInputStream(DIR_TS_PARAM+FILE_PARAM);

        TEST_CONFIG.load(input);
        String temp = TEST_CONFIG.getProperty("CNX_BROWSER");
        if (BROWSER_FROM_SYS.contentEquals("YES")){
            temp=BROWSER_FOR_ALL_TEST;
        }
        if (temp.length()>0)
            CONF_BROWSER=temp.trim();

        MILLI_DEP = System.currentTimeMillis();
        MAP_TIME_LIGNE.put("START",sFlog.logDate(4));
        LOG_TEST.add("START TEST  = " +TEST_NAME);
        LOG_TEST.add("Dir lancement ="+dire_lancement);
        driver = sHelp.openWebdriver(driver, CONF_BROWSER); // FIREFOX  CHROME IEXPLORER PHANTOMJS

        sFlog.Add_String("apres INIT =" + CONF_BROWSER, DIR_LOGS);
        LOG_TEST.add("INIT OK =" + CONF_URL );
        try {

            WAIT = new WebDriverWait(driver, 10);
            //Dimension dime = new Dimension(800, 600);
            //wd.manage().window().setSize(dime);
            driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            if (BROWSER_FROM_SYS.contains("YES")){
                CONF_URL =GLOBAL_URL ;
            }else
                CONF_URL =TEST_CONFIG.getProperty("CNX_LOGIN_PORTAL");
            LOG_TEST.add("TRY TO OPEN URL =" + CONF_URL );

            String s_login = TEST_CONFIG.getProperty("CNX_TA_USER").trim();
            String s_pass = TEST_CONFIG.getProperty("CNX_TA_PASSWORD").trim();
            driver.get(CONF_URL);

            PageLoginPortal loginPage = new PageLoginPortal(driver, sUtil,sHelp,WAIT, LOG_TEST);
            sUtil.tempo(2);

            LOG_TEST.add("TRY TO LOGIN WITH USER AND PSW "+"Login user = "+s_login+ " ");

            loginPage.loginPortal(s_login, s_pass);
            sUtil.tempo(3);
            //WAIT.until(ExpectedConditions.urlContains("microsoftonline.com"));
            driver = loginPage.GetDriver();

            MILLI_FIN = System.currentTimeMillis();
            MILLI_DIFF = MILLI_FIN-MILLI_DEP;

            MAP_TIME_LIGNE.put("LOGIN",MILLI_DIFF);
            MILLI_TOT=MILLI_DIFF;
        } catch (Exception e) {
            e.printStackTrace();
            LOG_TEST.add("CRASH ");
            sHelp.screenShotCrash(driver);
        }

        System.out.println(MILLI_DIFF);

        return logGetIfOK();
    }

    public void runOneTest(String testName) throws IOException {

        if (FILE_PARAM.contains("__")){
            String a[]=FILE_PARAM.split("__");
            String num=a[0];
            if (num.length()>2){
                String os = System.getProperty("os.name").toLowerCase();
                int deb=0;
                if (num.contains("\\")) {
                    deb = num.lastIndexOf("\\");
                    num=num.substring(deb+1);
                }
                if (num.contains("/")){
                    deb = num.lastIndexOf("/");
                    num=num.substring(deb+1);
                    }
            }
            NUM_LIN_TEST=Integer.parseInt(num);
        }

        System.out.println("\n--->CALL TEST = "+TEST_NAME);
        Map<String, Object>  d_upload = new HashMap<>();
        try {
            switch  (TEST_NAME) {
                case "01_AccessToHistory":
                    AccessToHistory ath= new AccessToHistory(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );

                    LOG_TEST.add("--->CALL TEST = "+TEST_NAME);
                    MILLI_DIFF = ath.TestAccesToHistory();
                    MILLI_TOT = MILLI_TOT + MILLI_DIFF;
                    break;

                case "02_SubmitBordereau":
                    AccesAndSubmitBordereau asb= new AccesAndSubmitBordereau(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );

                    d_upload=asb.TestAccesAndSubmitBordereau();
                    MILLI_DIFF=(Long)d_upload.get("MILLI_TOT");
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;

                case "03_SubmitOtherRequest":
                    AccesSubmitOtherRequest sor=new AccesSubmitOtherRequest(
                            driver, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,sHelp );

                    MILLI_DIFF= sor.TestSubmitOtherRequest();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;
                case "04_Bordereau_History_List":
                    Bordereau_History_List bhl= new Bordereau_History_List(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE );

                        MILLI_DIFF=bhl.TestConsultHistoryLine();
                        MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;
                case "05_Other_Req_History_List":
                    Other_Req_History_List orhl= new Other_Req_History_List(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );

                    MILLI_DIFF=orhl.TestConsultHistoryLine();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;

                case "06_Export_Bordereau_History_List":
                    Export_Bordereau_History_List ebhl= new Export_Bordereau_History_List(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE );

                    MILLI_DIFF=ebhl.TestExportHistoryList();
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;

                case "07_Display_Filter_Results":
                    DisplayFilterResults dfr= new DisplayFilterResults(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,sHelp ,
                            MAP_TIME_LIGNE,LOG_TEST);

                    dfr.TestDisplayFilterResults();
                    break;
                case "R2_CLAIM_REFERAL_SUBMIT":
                    R2ClaimReferralSubmit r2crs = new R2ClaimReferralSubmit(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    r2crs.TestR2ClaimReferralSubmit();
                    break;
                case "R2_CLAIM_REFERAL_FILTER_AND_HISTORY":
                    R2ClaimReferralFilterAndHistory r2crfah = new R2ClaimReferralFilterAndHistory(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    r2crfah.TestR2ClaimReferralFilterAndHistory();
                    break;
                case "08_Error_Management_Page":
                    ErrorManagements tem= new ErrorManagements(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );

                    LOG_TEST.add("--->CALL TEST = "+TEST_NAME);
                    MILLI_DIFF = tem.TestError_Managements();
                    MILLI_TOT = MILLI_TOT + MILLI_DIFF;
                    break;
                case "09_ActiveMq_Dms_Check":
                    AccesAndSubmitBordereau asba= new AccesAndSubmitBordereau(
                            driver, TEST_CONFIG, WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    d_upload=asba.TestAccesAndSubmitBordereau();
                    MILLI_DIFF=(Long)d_upload.get("MILLI_TOT");
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    String dtSend=sFlog.logDate(11);

                    List<String> files_uploaded=asba.getFILES_UPLOADED();

                    ActiveMq_Dms_Check amqd= new ActiveMq_Dms_Check(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,files_uploaded );
                    MILLI_DIFF=amqd.TestActiveMqTopic(dtSend);
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;

                    break;
                case "10_ActiveMq_Negative_Check":
                    AccesAndSubmitBordereau asbneg= new AccesAndSubmitBordereau(
                            driver, TEST_CONFIG,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
                    d_upload=asbneg.TestAccesAndSubmitBordereau();
                    MILLI_DIFF=(Long)d_upload.get("MILLI_TOT");

                    dtSend=sFlog.logDate(11);
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;

                    List<String> files_uploadedNeg=asbneg.getFILES_UPLOADED();

                    ActiveMq_Negative_Check amqdn= new ActiveMq_Negative_Check(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,files_uploadedNeg );
                    MILLI_DIFF=amqdn.TestActiveMqTopic(dtSend);
                    MILLI_TOT=MILLI_TOT+MILLI_DIFF;
                    break;
                case "11_ASSIGN_ERROR_TO_USER_or_GROUP":

                    ErrorManagements temes= new ErrorManagements(
                            driver,TEST_NAME, DIR_LOGS,TEST_CONFIG, sFlog,WAIT,sUtil,
                            sHelp,MAP_TIME_LIGNE,LOG_TEST );

                    LOG_TEST.add("--->CALL TEST = "+TEST_NAME);
                    MILLI_DIFF = temes.TestError_Managements();
                    MILLI_TOT = MILLI_TOT + MILLI_DIFF;
                    break;

                case "12_UPLOAD_AND_CONFIRMATION_EMAIL":
//                    Upload_And_Confirmation_Email uace = new Upload_And_Confirmation_Email(
//                            driver, TEST_CONFIG,WAIT,sUtil,
//                            sHelp,MAP_TIME_LIGNE,LOG_TEST );
//                    LOG_TEST.add("--->CALL TEST = "+TEST_NAME);
//                    MILLI_DIFF = uace.TestBordereauUploadOnlyAndReadEmail();
//                    MILLI_TOT = MILLI_TOT + MILLI_DIFF;
                    break;
            }
            LOG_TEST.add("FIN TEST  _-------------");
        } catch (Exception e) {
            e.printStackTrace();
            int i=LOG_TEST.size();
            String s=LOG_TEST.get(i-1);
            s="Fail test when "+s;
            LOG_TEST.set(i-1,s);
            LOG_TEST.add("CRASH");
        }
        System.out.println("---> FIN TEST "+TEST_NAME+"\n");
    }
    //***************************************************
    public void tearDown() throws IOException {

        MILLI_DEP = System.currentTimeMillis();
        PageLogoutPortal logoutPage = new PageLogoutPortal(driver,sHelp,sUtil, WAIT, DIR_LOGS);

        try {
            if (driver!=null) {
                if (logGetIfOK()){
                    logoutPage.clickClickProfile();
                    logoutPage.clickClickConfirm();
                    STATUS_TXT="OK";
                }else{
                    STATUS_NOTES=LOG_TEST.get(LOG_TEST.size()-2);
                }
            }
        } catch (Exception e) {
            if (logGetIfOK()){
                LOG_TEST.add("Test OK but log out KO");
                LOG_TEST.add("CRASH");
                STATUS_TXT="OK";
                STATUS_NOTES="Test OK but log out KO";
            }
            e.printStackTrace();
        } finally {
            System.out.println("------------------");

        }
        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN-MILLI_DEP;
        MAP_TIME_LIGNE.put("LOGOUT",MILLI_DIFF);
        MILLI_TOT=MILLI_TOT+MILLI_DIFF;
        MAP_TIME_LIGNE.put("TOTAL",MILLI_TOT);
        sFlog.writeStatistic(DIR_ROOT_TS+DIR_REPORT+"STAT_"+TEST_NAME+".csv",MAP_TIME_LIGNE);
        sFlog.Add_Lines(LOG_TEST, DIR_LOGS);
        if (NUM_LIN_TEST>4)
            setExcelfile();
        if (driver!=null) driver.quit();

    }

    //***************************************************
    public void setExcelfile() throws IOException {
        EXC.set_File_Feuille(EXCEL_FILE,EXCEL_FEUILLE);
        System.out.println("EXCEL_FILE ="+EXCEL_FILE+" EXCEL_FEUILLE="+EXCEL_FEUILLE);
        EXC.openExcelFile();
        EXC.openFeuille(EXCEL_FEUILLE);
        EXC.ecrit_Lign_StringCell(NUM_LIN_TEST,5,STATUS_TXT);
        EXC.ecrit_Lign_StringCell(NUM_LIN_TEST,6,STATUS_NOTES);
        EXC.saveXLSX_file();
    }

    public boolean logGetIfOK(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return false;
        else
            return true;
    }
    public String logGetLastAction(){
        int i=LOG_TEST.size();
        if (LOG_TEST.get(i-1).contains("CRASH"))
            return LOG_TEST.get(i-2);
        else
            return LOG_TEST.get(i-1);
    }

    /************************************************************************************
     * ********************   MAIN
     ************************************************************************************/

    public  static void main(String[] args) throws Exception{
//        "ATH": //ACCESS TO HISTORY
//        "SB":  // SUBMIT BORDEREAU
//        "SOR":  // SUBMIT OTHER REQUESTS
//        "BHL":  // BORDEREAU HISTORY LIST
//        "ORHL":  // OTHER REQUESTS HISTORY LIST
//        "EBHL":  //EXPORT BORDEREAU HISTORY LIST
//        "DFR":  // DISPLAY FILTER RESULTS
//        "EMP": // ERROR MANAGEMENTS PAGE
//        "AMQP": // ACTIVEMQ JMS CHECK

        HashMap<String,String> map_test=new HashMap<>();
        map_test.put("ATH" ,"01_AccessToHistory");
        map_test.put("SB"  ,"02_SubmitBordereau");
        map_test.put("SOR" ,"03_SubmitOtherRequest");
        map_test.put("BHL" ,"04_Bordereau_History_List");
        map_test.put("ORHL","05_Other_Req_History_List");
        map_test.put("EBHL","06_Export_Bordereau_History_List");
        map_test.put("DFR", "07_Display_Filter_Results");
        map_test.put("EMP", "08_Error_Management_Page");
        map_test.put("AMQP","09_ActiveMq_Dms_Check");
        map_test.put("AMQN","10_ActiveMq_Negative_Check");
        map_test.put("AETUOG","11_ASSIGN_ERROR_TO_USER_or_GROUP");


        String  nomTest="";
//       nomTest="01_AccessToHistory";
//        nomTest="02_SubmitBordereau";
//        nomTest="03_SubmitOtherRequest";
//        nomTest="04_Bordereau_History_List";
//        nomTest="05_Other_Req_History_List";
//        nomTest="06_Export_Bordereau_History_List";
//        nomTest="07_Display_Filter_Results";
//        nomTest="08_Error_Management_Page";
 //       nomTest="09_ActiveMq_Dms_Check";
//        nomTest="10_ActiveMq_Negative_Check";
//        nomTest="11_ASSIGN_ERROR_TO_USER_or_GROUP";
//        nomTest="12_UPLOAD_AND_CONFIRMATION_EMAIL";
//        nomTest="R2_CLAIM_REFERAL_SUBMIT";
        nomTest="R2_CLAIM_REFERAL_FILTER_AND_HISTORY";

        String fileParam="";
//        fileParam="01_AccessToHistory_PARAM.txt";
//        fileParam="01_AccessToHistory_PARAM-ScorSuper.txt";
//        fileParam="02_SubmitBordereau_PARAM.txt";
//        fileParam="02_SubmitBordereau_PARAM_Excel.txt";
//        fileParam="02_SubmitBordereau_TEST_B_PARAM.txt";
//        fileParam="03_SubmitOtherRequest_PARAM.txt";
//        fileParam="04_Bordereau_History_List_PARAM.txt";
//        fileParam="05_Other_Req_History_List_PARAM.txt";
//        fileParam="06_Export_Bordereau_History_List_PARAM.txt";
//        fileParam="07_Display_Filter_Results_PARAM.txt";
//        fileParam="08_Error_Managements_Page_PARAM.txt";
//        fileParam="09_ActiveMq_Dms_Check_PARAM.txt";
//        fileParam="10_ActiveMq_Negative_Check_PARAM.txt";
//        fileParam="11_ASSIGN_ERROR_TO_USER_or_GROUP_PARAM.txt";
//        fileParam="12_UPLOAD_AND_CONFIRMATION_EMAIL_PARAM.txt";
//        fileParam="R2_CLAIM_REFERAL_SUBMIT_PARAM.txt";
       fileParam="R2_CLAIM_REFERAL_FILTER_AND_HISTORY_PARAM.txt";

        String  nomTestK="";
        if (args.length ==2) {
            System.out.println(args[0] +"+ "+args[1] );
            nomTestK=args[0] ;
            nomTest=map_test.get(nomTestK);
            fileParam=args[1] ;
            System.out.println(nomTest+"+ "+fileParam);
            if (nomTest==null)
                System.exit(0);
            if (fileParam==null)
                System.exit(0);
        }


        /*********  Construit le test ***********************
         *
         */
        Ta_Portal_TS1_Main ta = new Ta_Portal_TS1_Main(nomTest,fileParam);
        boolean status=ta.setUp(nomTest,fileParam);

        if (status==true) ta.runOneTest(nomTest);
        ta.tearDown();
        System.exit(0);
    }

}
