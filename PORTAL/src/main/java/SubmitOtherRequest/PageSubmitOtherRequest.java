package SubmitOtherRequest;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumUtil;
import Utils.RobotUtils;

public class PageSubmitOtherRequest {

    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();

    /***********************************************************************
     *
     * @param driver
     */

    public PageSubmitOtherRequest(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.WAIT=wait;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    //*************** ******************************************
    //***************  PageOject *************

    @FindBy(css = "body > div > main > div.container.desc > div > div.col-md-4.text-left > div > button")

    private WebElement ButtonAddDocu;
    public void clickButtonAddDocu() throws Exception{
        WAIT.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.btn:nth-child(1)")));
        ButtonAddDocu.click();
        sUtil.tempo(1);
    }

    @FindBy(css = "button.btn:nth-child(1)")
    private WebElement waitAddDoc;
    @FindBy(css = "body > div > main > div.container.desc > div > div.col-md-4.text-left > div > div > a")
    private WebElement ChoiseOther;
    public void clickChoiseOther() throws Exception {
        WAIT.until(ExpectedConditions.textToBePresentInElement(waitAddDoc,"Add document"));
        ChoiseOther.click();
        WAIT.until(ExpectedConditions.titleContains("SCOR"));
        sUtil.tempo(1);
    }

    @FindBy(css = "#AddRecipient")
    private WebElement AddEmail;

    @FindBy(css = "#validateEmail")
    private WebElement ValidEmail;
    @FindBy(css = "#recipientEmailInput")
    private WebElement TypeRecip;
    public void enterTypeRecip(String typeRecipient) throws Exception {

        WAIT.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#validateEmail")));

        TypeRecip.clear();
        sUtil.tempo(2);
        TypeRecip.sendKeys(typeRecipient);
        sUtil.tempo(2);
        ValidEmail.click();
        sUtil.tempo(2);
        WAIT.until(ExpectedConditions.elementToBeClickable(AddEmail));
        AddEmail.click();
    }

    @FindBy(css = "#Subject")
    private WebElement TitleRecip;
    public void enterTitleRecip(int n) throws Exception {
        String[] titRecip= {
                "Nam . libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est",
                "omnis. dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae ",
                "Itaque. earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat",
                "Nor. again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain"
        };
        //TitleRecip.sendKeys("");
        sUtil.tempo(1);
        TitleRecip.sendKeys(titRecip[n]);
//        RobotUtils rb= new RobotUtils();
//        rb.leftClickPAram(180,20);
        sUtil.tempo(1);

    }

    @FindBy(css = "#Comment")
    private WebElement CommentRecip;
    public void enterCommentRecip(int n) throws Exception {
        String[] comm= {
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
        "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur",
        "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful"
 };
        sUtil.tempo(1);
        CommentRecip.sendKeys(comm[n]);
        sUtil.tempo(2);
    }

    @FindBy(css = "#dropLocation")
    private WebElement DropRecip;
    public void clickDropLocation(String filename) throws Exception {

        DropRecip.click();
        sUtil.memoClipboard(filename);
        Thread.sleep(1);

        RobotUtils rb= new RobotUtils();
        Thread.sleep(0);
        rb.leftClickPAram(180,20);
        rb.typeKeyPress("CTRL_V");
        Thread.sleep(0);

        rb.typeKeyPress("ENTER");
        sUtil.tempo(1);
    }

    @FindBy(css = "#dropLocation > input:nth-child(2)")
    private WebElement inputLocation;
    public void clickDropLocation2(List<String> list_Bord) throws Exception {

        String var= list_Bord.get(0);
        List<String> lst =new ArrayList<>();
        for (int i =1; i< list_Bord.size();i++){
            lst.add(list_Bord.get(i));
        }
        String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";
        //((JavascriptExecutor) driver).executeScript(js, inputLocation);

        boolean oui=true;
        int nbSend=0;

        for (int i =1; i< lst.size();i++){
            String filename=lst.get(i);
            if (var.contentEquals("VARIABLE")){
                int ivar=sUtil.getIntRandomNum(0,2,1);
                if (ivar==0){
                    oui=false;
                    if ((i==lst.size()-1)&&(nbSend<1)){
                        oui=true;
                    }
                }else oui=true;
            }
            if (oui){
                System.out.println(filename);
                ((JavascriptExecutor) driver).executeScript(js, inputLocation);
                inputLocation.sendKeys(filename);
                Thread.sleep(1);
                nbSend++;
            }
        }
        WebDriverWait wait_load = new WebDriverWait(driver, 600);
        for (int i=0;i<nbSend;i++){
            wait_load.until(ExpectedConditions.textToBePresentInElement(
                    (driver.findElement(By.cssSelector("ul.document:nth-child("+(i+1)+") > li:nth-child(6)"))),"Completed"));

        }

        System.out.println("aa");
    }

    @FindBy(css = ".swal2-confirm")
    private WebElement submitConfirm;
    @FindBy(xpath="//button[text()='Submit Other Request']")
    private WebElement submiOtherReq;
    @FindBy(css = "ul.document > li:nth-child(6)")
    private WebElement waitCompleted;

    public void clicksubmiOtherReq() throws Exception {
        WebDriverWait wait_load = new WebDriverWait(driver, 300);
        wait_load.until(ExpectedConditions.textToBePresentInElement(waitCompleted,"Completed"));
        submiOtherReq.click();
        sUtil.tempo(1);
        WAIT.until(ExpectedConditions.elementToBeClickable(submitConfirm));
        submitConfirm.click();
        sUtil.tempo(1);
    }





}
