package SubmitOtherRequest;


import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;

public class AccesSubmitOtherRequest {


    Properties CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME="SubmitBordereau";
    private String TEST_PARAM="05_SubmitOtherRequest_PARAM.txt";

    private FileAndLog sFlog = null;

    private String DIR_PARAM="";
    private String FIC_LOGS=DIR_PARAM+"";
    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;

    //****************************************************
    public AccesSubmitOtherRequest() throws IOException {
        sFlog = new FileAndLog();
    }
    public AccesSubmitOtherRequest(
            WebDriver dr, String fLogs, Properties testConf, FileAndLog logs,
            WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni) throws IOException {
        driver = dr;

        FIC_LOGS=fLogs;
        TEST_CONFIG=testConf;
        sFlog = logs;
        WAIT = wt;
        sUtil= sut;
        sHelp = sIni;
    }
    //********************************************************
    public void setUp() throws Exception {
        sFlog.Add_String("START TEST  = " +TEST_NAME,FIC_LOGS);

    }


    public long TestSubmitOtherRequest() throws Exception {

        MILLI_DEP = System.currentTimeMillis();

        PageSubmitOtherRequest pageAdd = new PageSubmitOtherRequest(driver,WAIT);
        sFlog.Add_String("clickButtonAddDocu + Choise Other Request  ",FIC_LOGS);
        pageAdd.clickButtonAddDocu();
        pageAdd.clickChoiseOther();
        sUtil.tempo(2);

        int nAle=sUtil.getIntRandomNum(0,4,2);
        sFlog.Add_String("enter email for recipient   ",FIC_LOGS);
        String recip="IWAJSBLATT@scor.com";
        pageAdd.enterTypeRecip(recip);
        //pageAdd.enterTypeRecip("DTRICOCHE-EXTERNAL@scor.com");

        sFlog.Add_String("enter title   ",FIC_LOGS);
        pageAdd.enterTitleRecip(nAle);
        sFlog.Add_String("enter comment   ",FIC_LOGS);
        pageAdd.enterCommentRecip(nAle);

        sFlog.Add_String("Send file  ",FIC_LOGS);
        List<String> list_Bord=sHelp.getFileToSendFromJddOR(TEST_CONFIG);

        pageAdd.clickDropLocation2(list_Bord);
        sFlog.Add_String("submit file   ",FIC_LOGS);
        pageAdd.clicksubmiOtherReq();
        sUtil.tempo(2);
        return MILLI_TOT;
    }

    //********************************************************

    public void tearDown() {

        driver.quit();
    }
    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        AccesSubmitOtherRequest logP = new AccesSubmitOtherRequest();
        logP.setUp();
        logP.TestSubmitOtherRequest();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }
}
