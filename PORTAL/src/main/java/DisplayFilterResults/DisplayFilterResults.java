package DisplayFilterResults;


import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;
import Utils.FileAndLog;

public class DisplayFilterResults {

    Properties CONFIG = new Properties();
    Properties TEST_CONFIG= new Properties();
    private SeleniumHelp sHelp = null;
    private SeleniumUtil sUtil = null;
    protected static WebDriver driver = null;
    private WebDriverWait WAIT = null;
    private String CONF_VAR="";
    private String CONF_BROWSER="FIREFOX";   // FIREFOX  CHROME IEXPLORER PHANTOMJS
    private String TEST_NAME="DisplayFilterResults";
    private String TEST_PARAM="02_DisplayFilterResults_PARAM.txt";
    private FileAndLog sFlog = null;
    private String DIR_PARAM="";
    private String FIC_LOGS="";

    private LinkedHashMap<String,Object> MAP_TIME_LIGNE=new LinkedHashMap<>();
    private LinkedHashMap<String,Object> STATUS_TEST=new LinkedHashMap<>();
    private List<String> LOG_TEST=null;

    private long MILLI_DEP =0;
    private long MILLI_FIN =0;
    private long MILLI_DIFF =0;
    private long MILLI_TOT =0;


    //*****************************************************************************************
    public DisplayFilterResults() throws IOException {
        sFlog = new FileAndLog();

    }

    public DisplayFilterResults(
            WebDriver dr, String testName, String fLogs, Properties testConf, FileAndLog logs, WebDriverWait wt, SeleniumUtil sut,
            SeleniumHelp sIni, LinkedHashMap<String,Object> mtl,List<String> st) throws IOException {
        driver = dr;
        TEST_NAME=testName;
        FIC_LOGS=fLogs;
        TEST_CONFIG=testConf;
        sFlog = logs;
        WAIT = wt;
        sUtil= sut;
        sHelp = sIni;
        MAP_TIME_LIGNE=mtl;
        LOG_TEST=st;
    }
    //*****************************************************************************************
    public void setUp() throws Exception {

        sFlog.Add_String("START TEST  = " +TEST_NAME,FIC_LOGS);


    }
    //********************************************************

    public Long TestDisplayFilterResults() throws Exception{
        MILLI_DEP = System.currentTimeMillis();

        PageFilterResults pageF = new PageFilterResults(driver,WAIT);
        LOG_TEST.add("Access to Historic OK  ");

        boolean bCedent=true;
        for (String s :LOG_TEST){
            if (s.contains("TaExternalPartner")) {
                bCedent=false;
            }
        }


        if (bCedent){
            String sFtype=TEST_CONFIG.getProperty("FILTER_TYPE").trim();
            String sFsending=TEST_CONFIG.getProperty("FILTER_SENDING_DATE").trim();
            String sFtoDate=TEST_CONFIG.getProperty("FILTER_FROM_TO_DATE").trim();
            String spFtoDate[]=sFtoDate.split("-");
            String allType[]=sFtype.split(";");
            for (String sf : allType){
                sf=sf.trim();
                LOG_TEST.add("Filter By Type  ");
                pageF.filterByType(sf);
            }
            LOG_TEST.add("Sort By Sending Date ");
            pageF.filterBySendingDate(sFsending);
            LOG_TEST.add("Sort By From To Date");
            pageF.filterByDateToDate(spFtoDate[0].trim(),spFtoDate[1].trim());

            LOG_TEST.add("Sort By Type  ");
            pageF.sortByType();
            LOG_TEST.add("Sort By Period  ");
            pageF.sortByPeriod();
            LOG_TEST.add("Sort By SendingDates  ");
            pageF.sortBySendingDates();
            LOG_TEST.add("Sort By SentBy  ");
            pageF.sortBySentBy();
        }else{
            LOG_TEST.add("Sort By Sending Date External Partner  ");
            pageF.sortEpBySendingDate();
            LOG_TEST.add("Sort By Nb Docu External Partner  ");
            pageF.sortEpByNbDocu();
            LOG_TEST.add("Sort By Sent by");
            pageF.sortEpBySentBy();
        }




        MILLI_FIN = System.currentTimeMillis();
        MILLI_DIFF = MILLI_FIN - MILLI_DEP;
        MILLI_TOT = MILLI_TOT + MILLI_DIFF;
        MAP_TIME_LIGNE.put("NAVIGATE PAGE", MILLI_DIFF);

        return MILLI_TOT;
    }
    //********************************************************

    public void tearDown() {
        if (driver!=null)
            driver.quit();
        System.out.println("End");
    }

    //********************************************************

    /********************************************************************************************************
     * MAIN
     * @throws Exception
     *
     * @throws IOException

     *
     ********************************************************************************************************/
    public static void main(String[] args) throws Exception  {
        // Instance
        DisplayFilterResults logP = new DisplayFilterResults();
        logP.setUp();
        logP.TestDisplayFilterResults();
        logP.tearDown();
        System.out.println("===FIN=== ");
    }
}
