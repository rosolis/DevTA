package DisplayFilterResults;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Helper.SeleniumHelp;
import Helper.SeleniumUtil;

public class PageFilterResults {


    private WebDriver driver;
    private WebDriverWait WAIT = null;
    private SeleniumUtil sUtil = new SeleniumUtil();
    private SeleniumHelp sHelp = new SeleniumHelp();

    /***********************************************************************
     *
     * @param driver
     */

    public PageFilterResults(WebDriver driver, WebDriverWait wait) throws IOException {
        this.driver = driver;
        this.WAIT=wait;
        PageFactory.initElements(this.driver, this);
    }
    public WebDriver GetDriver() {
        return this.driver;
    }
    //*************** ******************************************
    //***************  PageOject *************
    @FindBy(css = ".fa-search")
    private WebElement FilterSelectButton;

    //*[@id="formSearchDocHistory"]/div/div[3]/div[1]/span/span[1]/span/ul
    @FindBy(xpath = "//*[@id=\"formSearchDocHistory\"]/div/div[3]/div[1]/span/span[1]/span/ul")
    private WebElement FilterTypeField;
    @FindBy(css = ".select2-selection__clear")
    private WebElement FilterTypeFieldClear;
    public void filterByType(String sf) throws Exception {
        FilterTypeField.click();
        int v=1;
        if (sf.contains("Claims"))v=2;
        if (sf.contains("Premium"))v=3;
        if (sf.contains("Complementary "))v=4;
        String sv=Integer.toString(v);
        sUtil.tempo(3);
        //driver.findElement(By.cssSelector(".select2-results > ul> li:nth-of-type("+sv+")")).click();
        driver.findElement(By.xpath("//*[@id=\"select2-TypeId-results\"]/li["+sv+"]")).click();

        sUtil.tempo(2);
        FilterSelectButton.click();
        sUtil.tempo(3);

        FilterTypeFieldClear.click();
        sUtil.tempo(2);
        FilterTypeField.click();
        sUtil.tempo(1);
        FilterSelectButton.click();
        sUtil.tempo(3);
    }

    @FindBy(css = "#SendingDate")
    private WebElement FilterSendingDate;
    public void filterBySendingDate(String sf) throws Exception {
        FilterSendingDate.sendKeys(sf);
        sUtil.tempo(2);
        FilterSelectButton.click();
        sUtil.tempo(4);
        FilterSendingDate.clear();
        sUtil.tempo(4);
        FilterSelectButton.click();
        sUtil.tempo(3);
    }
    @FindBy(css = "#FromPeriod")
    private WebElement FilterFromDate;
    @FindBy(css = "#ToPeriod")
    private WebElement FilterToDate;
    public void filterByDateToDate(String from,String to) throws Exception {
        FilterFromDate.sendKeys(from);
        sUtil.tempo(1);
        FilterToDate.sendKeys(to);
        sUtil.tempo(2);
        FilterSelectButton.click();
        sUtil.tempo(4);
        FilterFromDate.clear();
        FilterToDate.clear();
        FilterSelectButton.click();
    }

    @FindBy(css = "div.ag-header-cell:nth-child(1) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabType;
    public void sortByType() throws Exception {
        TabType.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(2) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabPeriod;
    public void sortByPeriod() throws Exception {
        TabPeriod.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(4) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabSendingDates;
    public void sortBySendingDates() throws Exception {
        TabSendingDates.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(6) > div:nth-child(3) > div:nth-child(1) > span:nth-child(1)")
    private WebElement TabSentBy;
    public void sortBySentBy() throws Exception {
        TabSentBy.click();
        sUtil.tempo(2);
    }
    //*************************** External Partner  "
    @FindBy(css = "div.ag-header-cell:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
    private WebElement EP_SendingDate;
    public void sortEpBySendingDate() throws Exception {
        EP_SendingDate.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(3) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
    private WebElement EP_NbDocu;
    public void sortEpByNbDocu() throws Exception {
        EP_NbDocu.click();
        sUtil.tempo(2);
    }
    @FindBy(css = "div.ag-header-cell:nth-child(4) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
    private WebElement EP_SentBy;
    public void sortEpBySentBy() throws Exception {
        EP_SentBy.click();
        sUtil.tempo(2);
    }
}
